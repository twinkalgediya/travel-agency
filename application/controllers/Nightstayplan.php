    <?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';
/**
 * Class : Night Stay Plan (AdminController)
 * Hotel.
 * @author : bhavinpethani333@gmail.com
 * @version : 1.0
 * @since : 23.07.2018
 */
class Nightstayplan extends BaseController
{

    /**
     * This is default constructor of the class
     */
    public function __construct()
    {

        parent::__construct();
        $this->load->model('Nightstayplan_model');
        // Datas -> libraries ->BaseController / This function used load user sessions
        $this->datas();
        // isLoggedIn / Login control function /  This function used login control
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('login');
        }
        
    }
	
     /**
     * This function is used to load the user list
     */
    function nightstayplanListing()
    {   
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->Nightstayplan_model->nightstayplanListingCount($searchText);

			$returns = $this->paginationCompress("nightstayplanListing/",$count,$count);
            //echo $returns["page"];echo $returns["segment"];exit;
            $data['nightRecords'] = $this->Nightstayplan_model->nightstayplanListing($searchText, $returns["page"], $returns["segment"]);
            
            $process = 'NightStayPlan Listing';
            $processFunction = 'Admin/nightstayplanListing';
            $this->logrecord($process,$processFunction);
            
            $this->global['pageTitle'] = SITE_NAME.' : NightStayPlan List';
           
            $this->loadViews("nightstayplans", $this->global, $data, NULL);
    }

    /**
     * This function is used to load the add new form
     */
    function addNightStayPlan()
    {
            
            $this->global['pageTitle'] = SITE_NAME.' : ADD NightStayPlan';
            $data['state'] = $this->getstate();
            $data['countries'] = $this->getcountry();
            
            $this->loadViews("addNightStayPlan", $this->global,$data,NULL);
    }

    /**
     * This function is used to add new user to the system
     */
    function insertNightStayPlan()
    {
		$postdata=$this->input->post();
            $this->form_validation->set_rules('typeof_place','Select Place Type','required');
            /*if(isset($postdata['country_id'])){
                $this->form_validation->set_rules('country_id','Country','required');
                
            }
            else
            {
                $this->form_validation->set_rules('state_id','State','required');
            }*/
            
            $this->form_validation->set_rules('no_of_nights','No Off Night','trim|required');
            $this->form_validation->set_rules('night_stay','Night Stay','trim|required');
            $this->form_validation->set_rules('itinerary','Itinerary','trim|required');
            
           
            if($this->form_validation->run() == FALSE)
            {
                $this->addNightStayPlan();
            }
            else
            {
				$nightInfo = array(
                    'typeof_place' => $this->input->post('typeof_place'),
                    'no_of_nights' =>$this->input->post('no_of_nights'),
                    'night_stay' =>$this->input->post('night_stay'),
                    'itinerary' =>$this->input->post('itinerary'),
                );
                   if(isset($postdata['country_id'])){
                     $nightInfo['country_id'] =$this->input->post('country_id');
                     
                   } 
                   if(isset($postdata['state_id']))
                   {
                        $nightInfo['state_id'] =$this->input->post('state_id');
                   }                
                $last_insert_id = $this->Nightstayplan_model->insertNightStayPlan($nightInfo);
               
            
                if($last_insert_id > 0)
                {
                    $process = 'Create NightStayPlan';
                    $processFunction = 'hotel/addNightStayPlan';
                    $this->logrecord($process,$processFunction);
					$this->session->set_flashdata('success', 'Your NightStayPlan added successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Something wrong in your information');
                }
                
                redirect('nightstayplanListing');
            }
       
	}
     /**
     * This function is used load hotel edit information
     * @param number $hotelId : Optional : This is hotel id
     */
    function editNightStayPlan($nightId = NULL)
    {
       
            if($nightId == null)
            {
                redirect('nightstayplanListing');
            }
            $data['countries'] = $this->getcountry();
            $data['state'] = $this->getstate();
            $data['night'] = $this->Nightstayplan_model->getNightStayPlan($nightId);

			 if(isset($data['night']) &&  !empty($data['night'][0]->country_id) && $data['night'][0]->country_id > 0){
            $data['noofnight'] = $this->Nightstayplan_model->getnightByCountryedit($data['night'][0]->country_id);
            }
            if(isset($data['night']) &&  !empty($data['night'][0]->state_id) && $data['night'][0]->state_id > 0){
            $data['noofnight'] = $this->Nightstayplan_model->getnightByStateedit($data['night'][0]->state_id);
            }
            
            $this->global['pageTitle'] = SITE_NAME.' : Edit NightStayPlan';
           //echo "<pre>"; print_r($data);exit;
            $this->loadViews("addNightStayPlan", $this->global, $data, NULL);
    }


    /**
     * This function is used to edit the Hotel informations
     */
    function updateNightStayPlan()
    {
            $nightId = $this->input->post('id');
           $postdata=$this->input->post();
            $this->form_validation->set_rules('typeof_place','Select Place Type','required');
           /* $this->form_validation->set_rules('country_id','Country','required');
            $this->form_validation->set_rules('state_id','State','required');*/
            $this->form_validation->set_rules('no_of_nights','No Off Night','trim|required');
            $this->form_validation->set_rules('night_stay','Night Stay','trim|required');
            $this->form_validation->set_rules('itinerary','Itinerary','trim|required');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editNightStayPlan($nightId);
            }
            else
            {
				
				
                $nightInfo = array(
                    'typeof_place' => $this->input->post('typeof_place'),
                    'no_of_nights' =>$this->input->post('no_of_nights'),
                    'night_stay' =>$this->input->post('night_stay'),
                    'itinerary' =>$this->input->post('itinerary'),
                );
                
                    if(isset($postdata['country_id'])){
                     $nightInfo['country_id'] =$this->input->post('country_id');
                     
                   } 
                   if(isset($postdata['state_id']))
                   {
                        $nightInfo['state_id'] =$this->input->post('state_id');
                   }               

                $result = $this->Nightstayplan_model->updateNightStayPlan($nightInfo, $nightId);
                
                if($result == true)
                {
                    $process = 'NightStayPlan Update';
                    $processFunction = 'Admin/editNightStayPlan';
                    $this->logrecord($process,$processFunction);

                    $this->session->set_flashdata('success', 'NightStayPlan successfully updated');
                }
                else
                {
                    $this->session->set_flashdata('error', 'NightStayPlan update failed');
                }
                redirect('nightstayplanListing');
            }
    }

     /**
     * This function is used to delete the Hotel using Id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteNightStayPlan($id)
    {
           if($id == null && $id < 0)
            {
                redirect('nightstayplanListing');
            }
            $result = $this->Nightstayplan_model->deleteNightStayPlan($id);
            
			if($result > 0){
				 $this->session->set_flashdata('success', 'NightStayPlan Deleted Successfully');
					redirect('nightstayplanListing');
			} else{
				$this->session->set_flashdata('error', 'NightStayPlan deleted failed');
				redirect('nightstayplanListing');
			}
	}

   function getInternational()
   {
    $plan = $this->input->post('plan');
    $package_id = $this->input->post('package_id');
    $international = array();
    if($package_id != '' && $package_id > 0){
         $international = $this->Nightstayplan_model->getInternationalrows1($plan);
    }else{
        if($plan){
           $international = $this->Nightstayplan_model->getInternationalrows($plan);
        }
    }
       echo json_encode($international);
   }
   function getNoofNightbycountry()
   {
        $country_id = $this->input->post('country_id');
        $international = array();
        if($country_id){
           $international = $this->Nightstayplan_model->getnightbycountryrows($country_id);
        }
        echo json_encode($international);
   }
   function getNoofNightbystate()
   {
        $state_id = $this->input->post('state_id');
        $international = array();
        if($state_id){
           $international = $this->Nightstayplan_model->getnightbystaterows($state_id);
        }
        echo json_encode($international);
   }
    function getnightStayPlan()
   {
        $stayplan = array();
        $nightid = $this->input->post('nightid');
        if($nightid){
           $stayplan = $this->Nightstayplan_model->getnightStayPlanrows($nightid);
        }
        echo json_encode($stayplan);
   }
   function getIternary()
   {
       $iternary = array();
        $plan_id = $this->input->post('plan_id');
        if($plan_id){
           $iternary = $this->Nightstayplan_model->getIternaryrows($plan_id);
        }
        echo json_encode($iternary);
   }
   function getnightByCountry()
   {
	   $no_of_night = array();
        $nightID = $this->input->post('no_of_nights_id');
        if($nightID){
			
           $no_of_night = $this->Nightstayplan_model->getnightByCountry($nightID);
        }
        echo json_encode($no_of_night);
   }
    function getnightByState()
   {
	   $no_of_night = array();
        $nightID = $this->input->post('no_of_nights_id');
        if($nightID){
			
           $no_of_night = $this->Nightstayplan_model->getnightByState($nightID);
		   
        }
        echo json_encode($no_of_night);
   }
   
    	
}
