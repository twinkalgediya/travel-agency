<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';
/**
 * Class : Inquiry (AdminController)
 * Inquiry.
 * @author : bhavinpethani333@gmail.com
 * @version : 1.0
 * @since : 23.07.2018
 */
class Inquiry extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('inquiry_model');
        // Datas -> libraries ->BaseController / This function used load user sessions
        $this->datas();
        // isLoggedIn / Login control function /  This function used login control
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('login');
        }
        
    }
	
     /**
     * This function is used to load the user list
     */
    function inquiryListing()
    {   
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->inquiry_model->inquiryListingCount($searchText);

			$returns = $this->paginationCompress ( "inquiryListing/", $count, $count );
            
            $data['inquiryRecords'] = $this->inquiry_model->inquiryListing($searchText, $returns["page"], $returns["segment"]);
            
            $process = 'Inquiry Listing';
            $processFunction = 'Admin/inquiryListing';
            $this->logrecord($process,$processFunction);
            
            $this->global['pageTitle'] = SITE_NAME.' : Inquiry List';
           
            $this->loadViews("inquirys", $this->global, $data, NULL);
    }

    /**
     * This function is used to load the add new form
     */
    function addInquiry()
    {
            
            $this->global['pageTitle'] = SITE_NAME.' : ADD Inquiry';
			$data['getnbr'] = $this->inquiry_model->getnbr();
			//print_r($data['getnbr']);exit;
            $this->loadViews("addInquiry", $this->global, $data, NULL);
    }

    /**
     * This function is used to add new user to the system
     */
    function insertInquiry()
    {
            //print_r($this->input->post());die;        
            $this->form_validation->set_rules('firstName','First Name','trim|required|max_length[255]');
            $this->form_validation->set_rules('lastName','last Name','trim|max_length[255]');
            $this->form_validation->set_rules('email','Email','trim|valid_email|max_length[128]');
            $this->form_validation->set_rules('altEmail','Alternet Email','trim|valid_email|max_length[128]');
            $this->form_validation->set_rules('phoneNo','Mobile Number','required|min_length[10]');
            $this->form_validation->set_rules('altPhoneNo','Alternet Mobile Number','min_length[10]');
            $this->form_validation->set_rules('destination','Destination Name','trim|required|max_length[255]');
            $this->form_validation->set_rules('travelDate','Travel Date','required');
            $this->form_validation->set_rules('noOffAdults','Travel Date','required|numeric');
            $this->form_validation->set_rules('childWithExtraBed','Child With ExtraBed','numeric');
            $this->form_validation->set_rules('childWithoutExtraBed','Child Without ExtraBed','numeric');
                    $this->form_validation->set_rules('prospect','Prospect','required');
            $this->form_validation->set_rules('source','Source','required');
            $this->form_validation->set_rules('nextFollowupDate','Next Followup Date','required');
            $this->form_validation->set_rules('status','Status','required');
           
            if($this->form_validation->run() == FALSE)
            {
                $this->addInquiry();
            }
			else
            {
				
                $inqueryInfo = array(
                    'firstName' => $this->input->post('firstName'),
                    'lastName' => $this->input->post('lastName'),
                    'email' => $this->input->post('email'),
                    'altEmail' => $this->input->post('altEmail'),
                    'phoneNo' => $this->input->post('phoneNo'),
                    'altPhoneNo' => $this->input->post('altPhoneNo'),
                    'destination' => $this->input->post('destination'),
                    'travelDate' => $this->input->post('travelDate'),
					
                    'noOffAdults' => $this->input->post('noOffAdults'),
                    'childWithExtraBed' => $this->input->post('childWithExtraBed'),
                    'childWithoutExtraBed' => $this->input->post('childWithoutExtraBed'),
                    'prospect' => $this->input->post('prospect'),
                    'source' => $this->input->post('source'),
                    'nextFollowupDate' => $this->input->post('nextFollowupDate'),
                   // 'notes' => $this->input->post('notes'),
                    'nbr' => $this->input->post('nbr'),
                    'status' => $this->input->post('status'),
                    'addedBy' => $this->session->userdata['userId'],
                );
                                    
                $result = $this->inquiry_model->insertInquiry($inqueryInfo);
                
                if($result > 0)
                {
					$inqueryNote = array(
						'inquiryNote' => $this->input->post('notes'),
						'inquiryID' => $result,
						'addedBy' => $this->session->userdata['userId'],
					);
					$noteRes = $this->inquiry_model->insertNote($inqueryNote);
					if($noteRes > 0){
						$process = 'Note Added';
						$processFunction = 'inquiry/addInquiry';
						$this->logrecords($process,$processFunction, $noteRes);
					}
                    $process = 'Create Inquiry';
                    $processFunction = 'inquiry/addInquiry';
                    $this->logrecords($process,$processFunction, $result);

                    $this->session->set_flashdata('success', 'Your inquery added successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Something wrong in your information');
                }
                
                redirect('inquiryListing');
            }
        }

     /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editInquiry($inquiryId = NULL)
    {
       
            if($inquiryId == null)
            {
                redirect('inquiryListing');
            }
			
            $data['oditlog']=$this->inquiry_model->getOditlog($inquiryId);
			
            $data['Inquiry'] = $this->inquiry_model->getInquiry($inquiryId);
			
            $data['InquiryNote'] = $this->inquiry_model->getInquiryNote($inquiryId);
			
			$data['getnbr'] = $this->inquiry_model->getnbr();
            $this->global['pageTitle'] = SITE_NAME.' : Edit Inquiry';
            $this->loadViews("addInquiry", $this->global, $data, NULL);
    }


    /**
     * This function is used to edit the user informations
     */
    function updateInquiry()
    {
            $inquiryId = $this->input->post('id');
           
             //print_r($this->input->post());die;        
             $this->form_validation->set_rules('firstName','First Name','trim|required|max_length[255]');
             $this->form_validation->set_rules('lastName','last Name','trim|max_length[255]');
             //$this->form_validation->set_rules('customerName','Customer Name','trim|required|max_length[255]');
             $this->form_validation->set_rules('email','Email','trim|valid_email|max_length[128]');
             $this->form_validation->set_rules('phoneNo','Mobile Number','required|min_length[10]');
             $this->form_validation->set_rules('destination','Destination Name','trim|required|max_length[255]');
             $this->form_validation->set_rules('travelDate','Travel Date','required');
             $this->form_validation->set_rules('noOffAdults','Travel Date','required|numeric');
             $this->form_validation->set_rules('childWithExtraBed','Child With ExtraBed','numeric');
             $this->form_validation->set_rules('childWithoutExtraBed','Child Without ExtraBed','numeric');
           
             $this->form_validation->set_rules('prospect','Prospect','required');
             $this->form_validation->set_rules('nextFollowupDate','Next Followup Date','required');
             $this->form_validation->set_rules('status','Status','required');
			 $this->form_validation->set_rules('source','Source','required');
            
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editInquiry($inquiryId);
            }
            else
            {
				$inqueryInfo = array(
                    'firstName' => $this->input->post('firstName'),
                    'lastName' => $this->input->post('lastName'),
                    'email' => $this->input->post('email'),
					'altEmail' => $this->input->post('altEmail'),
                    'phoneNo' => $this->input->post('phoneNo'),
					'altPhoneNo' => $this->input->post('altPhoneNo'),
                    'destination' => $this->input->post('destination'),
                    'travelDate' => $this->input->post('travelDate'),
                   
                    'noOffAdults' => $this->input->post('noOffAdults'),
                    'childWithExtraBed' => $this->input->post('childWithExtraBed'),
                    'childWithoutExtraBed' => $this->input->post('childWithoutExtraBed'),
                    'prospect' => $this->input->post('prospect'),
                    'source' => $this->input->post('source'),
                    'nextFollowupDate' => $this->input->post('nextFollowupDate'),
                    'notes' => $this->input->post('notes'),
					'nbr' => $this->input->post('nbr'),
					'status' => $this->input->post('status'),
					
                );
                
                $result = $this->inquiry_model->updateInquiry($inqueryInfo, $inquiryId);
                
                if($result == true)
                {
					$inqueryNote = array(
						'inquiryNote' => $this->input->post('notes'),
						'inquiryID' => $inquiryId,
						'addedBy' => $this->session->userdata['userId'],
					);
					$noteRes = $this->inquiry_model->insertNote($inqueryNote);
					if($noteRes > 0){
						$process = 'Note Added';
						$processFunction = 'inquiry/addInquiry';
						$this->logrecords($process,$processFunction, $noteRes);
					}
					
                    $process = 'Inquiry Update';
                    $processFunction = 'Admin/editInquiry';
                    $this->logrecords($process,$processFunction, $inquiryId);

                    $this->session->set_flashdata('success', 'Inquiry successfully updated');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Inquiry update failed');
                }
                
                redirect('editInquiry/'.$inquiryId);
            }
    }

     /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteInquiry($id)
    {
           if($id == null && $id < 0)
            {
                redirect('inquiryListing');
            }
            $result = $this->inquiry_model->deleteInquiry($id);
            
			if($result > 0){
				 $process = 'Delete Inquiry';
				 $processFunction = 'Admin/deleteInquiry';
				 $this->logrecord($process,$processFunction);
				
					$this->session->set_flashdata('success', 'Inquiry Deleted Successfully');
					redirect('inquiryListing');
			} else{
				$this->session->set_flashdata('error', 'Inquiry deleted failed');
				redirect('inquiryListing');
			}
	}

     /**
     * This function used to show log history
     * @param number $userId : This is user id
     */
    function logHistory($userId = NULL)
    {
            $data['dbinfo'] = $this->user_model->gettablemb('tbl_log','cias');
            if(isset($data['dbinfo']->total_size))
            {
                if(($data['dbinfo']->total_size)>1000){
                    $this->backupLogTable();
                }
            }
            $data['userRecords'] = $this->user_model->logHistory($userId);

            $process = 'Log Görüntüleme';
            $processFunction = 'Admin/logHistory';
            $this->logrecord($process,$processFunction);

            $this->global['pageTitle'] = 'BSEU : Kullanıcı Giriş Geçmişi';
            
            $this->loadViews("logHistory", $this->global, $data, NULL);
    }

    /**
     * This function used to show specific user log history
     * @param number $userId : This is user id
     */
    function logHistorysingle($userId = NULL)
    {       
            $userId = ($userId == NULL ? $this->session->userdata("userId") : $userId);
            $data["userInfo"] = $this->user_model->getUserInfoById($userId);
            $data['userRecords'] = $this->user_model->logHistory($userId);
            
            $process = 'Tekil Log Görüntüleme';
            $processFunction = 'Admin/logHistorysingle';
            $this->logrecord($process,$processFunction);

            $this->global['pageTitle'] = 'BSEU : Kullanıcı Giriş Geçmişi';
            
            $this->loadViews("logHistorysingle", $this->global, $data, NULL);      
    }
    
    /**
     * This function used to backup and delete log table
     */
    function backupLogTable()
    {
        $this->load->dbutil();
        $prefs = array(
            'tables'=>array('tbl_log')
        );
        $backup=$this->dbutil->backup($prefs) ;

        date_default_timezone_set('Europe/Istanbul');
        $date = date('d-m-Y H-i');

        $filename = './backup/'.$date.'.sql.gz';
        $this->load->helper('file');
        write_file($filename,$backup);

        $this->user_model->clearlogtbl();

        if($backup)
        {
            $this->session->set_flashdata('success', 'Yedekleme ve Tablo temizleme işlemi başarılı');
            redirect('log-history');
        }
        else
        {
            $this->session->set_flashdata('error', 'Yedekleme ve Tablo temizleme işlemi başarısız');
            redirect('log-history');
        }
    }

    /**
     * This function used to open the logHistoryBackup page
     */
    function logHistoryBackup()
    {
            $data['dbinfo'] = $this->user_model->gettablemb('tbl_log_backup','cias');
            if(isset($data['dbinfo']->total_size))
            {
            if(($data['dbinfo']->total_size)>1000){
                $this->backupLogTable();
            }
            }
            $data['userRecords'] = $this->user_model->logHistoryBackup();

            $process = 'Yedek Log Görüntüleme';
            $processFunction = 'Admin/logHistoryBackup';
            $this->logrecord($process,$processFunction);

            $this->global['pageTitle'] = 'BSEU : Kullanıcı Yedek Giriş Geçmişi';
            
            $this->loadViews("logHistoryBackup", $this->global, $data, NULL);
    }

    /**
     * This function used to delete backup_log table
     */
    function backupLogTableDelete()
    {
        $backup=$this->user_model->clearlogBackuptbl();

        if($backup)
        {
            $this->session->set_flashdata('success', 'Tablo temizleme işlemi başarılı');
            redirect('log-history-backup');
        }
        else
        {
            $this->session->set_flashdata('error', 'Tablo temizleme işlemi başarısız');
            redirect('log-history-backup');
        }
    }

    /**
     * This function used to open the logHistoryUpload page
     */
    function logHistoryUpload()
    {       
            $this->load->helper('directory');
            $map = directory_map('./backup/', FALSE, TRUE);
        
            $data['backups']=$map;

            $process = 'Yedek Log Yükleme';
            $processFunction = 'Admin/logHistoryUpload';
            $this->logrecord($process,$processFunction);

            $this->global['pageTitle'] = 'BSEU : Kullanıcı Log Yükleme';
            
            $this->loadViews("logHistoryUpload", $this->global, $data, NULL);      
    }

    /**
     * This function used to upload backup for backup_log table
     */
    function logHistoryUploadFile()
    {
        $optioninput = $this->input->post('optionfilebackup');

        if ($optioninput == '0' && $_FILES['filebackup']['name'] != '')
        {
            $config = array(
            'upload_path' => "./uploads/",
            'allowed_types' => "gz|sql|gzip",
            'overwrite' => TRUE,
            'max_size' => "20048000", // Can be set to particular file size , here it is 20 MB(20048 Kb)
            );

            $this->load->library('upload', $config);
            $upload= $this->upload->do_upload('filebackup');
                $data = $this->upload->data();
                $filepath = $data['full_path'];
                $path_parts = pathinfo($filepath);
                $filetype = $path_parts['extension'];
                if ($filetype == 'gz')
                {
                    // Read entire gz file
                    $lines = gzfile($filepath);
                    $lines = str_replace('tbl_log','tbl_log_backup', $lines);
                }
                else
                {
                    // Read in entire file
                    $lines = file($filepath);
                    $lines = str_replace('tbl_log','tbl_log_backup', $lines);
                }
        }

        else if ($optioninput != '0' && $_FILES['filebackup']['name'] == '')
        {
            $filepath = './backup/'.$optioninput;
            $path_parts = pathinfo($filepath);
            $filetype = $path_parts['extension'];
            if ($filetype == 'gz')
            {
                // Read entire gz file
                $lines = gzfile($filepath);
                $lines = str_replace('tbl_log','tbl_log_backup', $lines);
            }
            else
            {
                // Read in entire file
                $lines = file($filepath);
                $lines = str_replace('tbl_log','tbl_log_backup', $lines);
            }
        }
                // Set line to collect lines that wrap
                $templine = '';
                
                // Loop through each line
                foreach ($lines as $line)
                {
                    // Skip it if it's a comment
                    if (substr($line, 0, 2) == '--' || $line == '')
                    continue;
                    // Add this line to the current templine we are creating
                    $templine .= $line;

                    // If it has a semicolon at the end, it's the end of the query so can process this templine
                    if (substr(trim($line), -1, 1) == ';')
                    {
                        // Perform the query
                        $this->db->query($templine);

                        // Reset temp variable to empty
                        $templine = '';
                    }
                }
            if (empty($lines) || !isset($lines))
            {
                $this->session->set_flashdata('error', 'Yedek yükleme işlemi başarısız');
                redirect('log-history-upload');
            }
            else
            {
                $this->session->set_flashdata('success', 'Yedek yükleme işlemi başarılı');
                redirect('log-history-upload');
            }
    }
	
	function logrecords($process,$processFunction,$id){
        $this->datas();
        $logInfo = array("userId"=>$this->vendorId,
        "userName"=>$this->name,
        "process"=>$process,
        "processFunction"=>$processFunction,
        "userRoleId"=>$this->role,
        "userRoleText"=>$this->roleText,
        "userIp"=>$_SERVER['REMOTE_ADDR'],
        "userAgent"=>getBrowserAgent(),
        "agentString"=>$this->agent->agent_string(),
        "platform"=>$this->agent->platform(),
        "pro_inq_id"=>$id
        );
        
        $this->load->model('login_model');
        $this->login_model->loginsert($logInfo);
    }
}