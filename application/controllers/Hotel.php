<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';
/**
 * Class : Hotel (AdminController)
 * Hotel.
 * @author : bhavinpethani333@gmail.com
 * @version : 1.0
 * @since : 23.07.2018
 */
class Hotel extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('hotel_model');
        // Datas -> libraries ->BaseController / This function used load user sessions
        $this->datas();
        // isLoggedIn / Login control function /  This function used login control
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('login');
        }
        
    }
	
     /**
     * This function is used to load the user list
     */
    function hotelListing()
    {   
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->hotel_model->hotelListingCount($searchText);

			$returns = $this->paginationCompress("hotelListing/",$count,$count);
            //echo $returns["page"];echo $returns["segment"];exit;
            $data['hotelRecords'] = $this->hotel_model->hotelListing($searchText, $returns["page"], $returns["segment"]);
            
            $process = 'Hotel Listing';
            $processFunction = 'Admin/hotelListing';
            $this->logrecord($process,$processFunction);
            
            $this->global['pageTitle'] = SITE_NAME.' : Hotel List';
           
            $this->loadViews("hotels", $this->global, $data, NULL);
    }

    /**
     * This function is used to load the add new form
     */
    function addHotel()
    {
            
            $this->global['pageTitle'] = SITE_NAME.' : ADD Hotel';
            
            $data['countries'] = $this->getcountry();
            
            $this->loadViews("addHotel", $this->global,$data,NULL);
    }

    /**
     * This function is used to add new user to the system
     */
    function insertHotel()
    {
		//print_r($this->input->post());die;
            $this->form_validation->set_rules('hotelName','hotel Name','trim|required|max_length[255]');
            $this->form_validation->set_rules('email_Id','Email','trim|required|valid_email|max_length[128]');
            $this->form_validation->set_rules('altEmail','Alternet Email','trim|valid_email|max_length[128]');
            $this->form_validation->set_rules('phoneNumber','Phone Number','required|max_length[16]');
            $this->form_validation->set_rules('altPhoneNumber','Alternet Phone Number','max_length[16]');
            
            $this->form_validation->set_rules('stareRatting','Ratting','required');
            //$this->form_validation->set_rules('hotelPhoto','Hotel Photo','required');
            //$this->form_validation->set_rules('extraPhoto','Extra Photo','required|mime:gif|png|jpg|jpeg');
            $this->form_validation->set_rules('address','address','required');
            $this->form_validation->set_rules('country','country','required');
            $this->form_validation->set_rules('state','state','required');
            $this->form_validation->set_rules('city','city','required');
           
            if($this->form_validation->run() == FALSE)
            {
                $this->addHotel();
            }
            else
            {
				//Check whether user upload hotelPhoto
				if(!empty($_FILES['hotelPhoto']['name'])){
					$config['upload_path'] = 'uploads/images';
					$config['allowed_types'] = 'jpg|jpeg|png|gif';
					$config['file_name'] = $_FILES['hotelPhoto']['name'];
					
					//Load upload library and initialize configuration
					$this->load->library('upload',$config);
					$this->upload->initialize($config);
					
					if($this->upload->do_upload('hotelPhoto')){
						$uploadData = $this->upload->data();
						$hotelPhoto = $uploadData['file_name'];
					}else{
						$hotelPhoto = '';
                       // print_r($this->upload->display_errors());exit;
					}
				}else{
					$hotelPhoto = '';
                }

                $hotelInfo = array(
                    'hotelName' => $this->input->post('hotelName'),
                    'email_Id' 	=> $this->input->post('email_Id'),
					'altEmail' 	=> $this->input->post('altEmail'),
                    'phoneNumber' => $this->input->post('phoneNumber'),
                    'altPhoneNumber' => $this->input->post('altPhoneNumber'),
                    'stareRatting' => $this->input->post('stareRatting'),
                    'address' => $this->input->post('address'),
                    'hotelPhoto' => $hotelPhoto,
					'country_id' => $this->input->post('country'),
                    'state_id' =>$this->input->post('state'),
                    'city_id' => $this->input->post('city'),
					'addedBy' => $this->session->userdata['userId'],

                );
                                    
                $last_insert_id = $this->hotel_model->insertHotel($hotelInfo);
               
             if($last_insert_id > 0)
                {
                    $data = array();
                    // If file upload form submitted
					
                if(!empty($_FILES['multiplehotelPhoto']['name']))
                {   
                    $filesCount = count($_FILES['multiplehotelPhoto']['name']);
                    for($i = 0; $i < $filesCount; $i++)
                    {
                        $_FILES['file']['name']     = $_FILES['multiplehotelPhoto']['name'][$i];
                        $_FILES['file']['type']     = $_FILES['multiplehotelPhoto']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['multiplehotelPhoto']['tmp_name'][$i];
                        $_FILES['file']['error']     = $_FILES['multiplehotelPhoto']['error'][$i];
                        $_FILES['file']['size']     = $_FILES['multiplehotelPhoto']['size'][$i];
                       
                        // File upload configuration
                        $config1['upload_path'] = 'uploads/images';
                        $config1['allowed_types'] = 'jpg|jpeg|png|gif';
                        // Load and initialize upload library
                        $this->load->library('upload', $config1);
                        $this->upload->initialize($config1);
                        
                        // Upload file to server
                        if($this->upload->do_upload('file')){
                            // Uploaded file data
                            $fileDatas = $this->upload->data();
                            $multiplehotelPhotos['file_name'] = $fileDatas['file_name'];
                            //$uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");

                            if(!empty($multiplehotelPhotos)){
                                // Insert files data into the database
                                
								$hotelPhotoInfo = array(
									'hotelID' => $last_insert_id,
									'image'   => $multiplehotelPhotos['file_name']
								);
								$result = $this->hotel_model->insertHotelPhoto($hotelPhotoInfo);
							}
                        }
                        else
                        {
                             //print_r($this->upload->display_errors());exit;
                        }
                    }
				}
                if($last_insert_id > 0)
                {
                    $process = 'Create Hotel';
                    $processFunction = 'hotel/addHotel';
                    $this->logrecord($process,$processFunction);
					$this->session->set_flashdata('success', 'Your hotel added successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Something wrong in your information');
                }
                
                redirect('hotelListing');
            }
        }
	}
     /**
     * This function is used load hotel edit information
     * @param number $hotelId : Optional : This is hotel id
     */
    function editHotel($hotelId = NULL)
    {
       
            if($hotelId == null)
            {
                redirect('hotelListing');
            }
            
            $data['Hotel'] = $this->hotel_model->getHotel($hotelId);
			
            $HotelPhotoRes = $this->hotel_model->getExtraPhoto($hotelId);
			
			$array_Photo = json_decode(json_encode($HotelPhotoRes), True);
   
			foreach ($array_Photo as $value) {
			 $HotelPhotodata[] = $value;
			}
             $data['countries'] = $this->getcountry();
             $data['state'] = $this->getstate();
             $data['cities'] = $this->getcity();
			$data['HotelPhotodata'] = $HotelPhotodata;
            $this->global['pageTitle'] = SITE_NAME.' : Edit Hotel';
            $this->loadViews("addHotel", $this->global, $data, NULL);
    }


    /**
     * This function is used to edit the Hotel informations
     */
    function updateHotel()
    {
            $hotelId = $this->input->post('id');
           
            $this->form_validation->set_rules('hotelName','hotel Name','trim|required|max_length[255]');
            $this->form_validation->set_rules('email_Id','Email','trim|required|valid_email|max_length[128]');
			$this->form_validation->set_rules('altEmail','Alternet Email','trim|valid_email|max_length[128]');
            $this->form_validation->set_rules('phoneNumber','Phone Number','required|max_length[16]');
			$this->form_validation->set_rules('altPhoneNumber','Alternet Phone Number','max_length[16]');
            
            $this->form_validation->set_rules('stareRatting','Ratting','required');
            //$this->form_validation->set_rules('hotelPhoto','Hotel Photo','required|mime:gif|png|jpg|jpeg');
            //$this->form_validation->set_rules('extraPhoto','Extra Photo','required|mime:gif|png|jpg|jpeg');
            $this->form_validation->set_rules('address','address','required');
              $this->form_validation->set_rules('country','country','required');
            $this->form_validation->set_rules('state','state','required');
            $this->form_validation->set_rules('city','city','required');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editHotel($hotelId);
            }
            else
            {
				//Check whether user upload hotelPhoto
				if(!empty($_FILES['hotelPhoto']['name'])){
					$config['upload_path'] = 'uploads/images';
					$config['allowed_types'] = 'jpg|jpeg|png|gif';
					$config['file_name'] = $_FILES['hotelPhoto']['name'];
					
					//Load upload library and initialize configuration
					$this->load->library('upload',$config);
					$this->upload->initialize($config);
					
					if($this->upload->do_upload('hotelPhoto')){
						$uploadData = $this->upload->data();
						$hotelPhoto1 = $uploadData['file_name'];
					}else{
						$hotelPhoto1 = '';
                       // print_r($this->upload->display_errors());exit;
					}
				}else{
					$hotelPhoto1 = '';
                }
				
				if(empty($hotelPhoto1) && $hotelPhoto1 == ''){
					$hotelPhoto = $this->input->post('hotel_photo');
				}else{
					// unlink images at the time of update 
					$this->deletePhoto($hotelId);
					$hotelPhoto = $hotelPhoto1;
				}
				
				//if multiple images are selected then  insert into hotel_photos
				$data = array();
                    // If file upload form submitted
					
                if(!empty($_FILES['multiplehotelPhoto']['name']))
                {   
                    $filesCount = count($_FILES['multiplehotelPhoto']['name']);
                    for($i = 0; $i < $filesCount; $i++)
                    {
                        $_FILES['file']['name']     = $_FILES['multiplehotelPhoto']['name'][$i];
                        $_FILES['file']['type']     = $_FILES['multiplehotelPhoto']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['multiplehotelPhoto']['tmp_name'][$i];
                        $_FILES['file']['error']     = $_FILES['multiplehotelPhoto']['error'][$i];
                        $_FILES['file']['size']     = $_FILES['multiplehotelPhoto']['size'][$i];
                       
                        // File upload configuration
                        $config2['upload_path'] = 'uploads/images';
                        $config2['allowed_types'] = 'jpg|jpeg|png|gif';
                        // Load and initialize upload library
                        $this->load->library('upload', $config2);
                        $this->upload->initialize($config2);
                        
                        // Upload file to server
                        if($this->upload->do_upload('file')){
                            // Uploaded file data
                            $fileDatas = $this->upload->data();
                            $multiplehotelPhotos['file_name'] = $fileDatas['file_name'];
                            //$uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");

                            if(!empty($multiplehotelPhotos)){
                                // Insert files data into the database
                                
								$hotelPhotoInfo = array(
									'hotelID' => $hotelId,
									'image'   => $multiplehotelPhotos['file_name']
								);
								$result1 = $this->hotel_model->insertHotelPhoto($hotelPhotoInfo);
							}
                        }
                    }
				}
				
                $hotelInfo = array(
                    'hotelName' => $this->input->post('hotelName'),
                    'email_Id' 	=> $this->input->post('email_Id'),
                    'altEmail' 	=> $this->input->post('altEmail'),
                    'phoneNumber' => $this->input->post('phoneNumber'),
                    'altPhoneNumber' => $this->input->post('altPhoneNumber'),
                    'city' => $this->input->post('city'),
                    'stareRatting' => $this->input->post('stareRatting'),
                    'address' => $this->input->post('address'),
                    'hotelPhoto' => $hotelPhoto,
                    'country_id' => $this->input->post('country'),
                    'state_id' =>$this->input->post('state'),
                    'city_id' => $this->input->post('city'),
                );
                
                $result = $this->hotel_model->updateHotel($hotelInfo, $hotelId);
                
                if($result == true)
                {
                    $process = 'Hotel Update';
                    $processFunction = 'Admin/editHotel';
                    $this->logrecord($process,$processFunction);

                    $this->session->set_flashdata('success', 'Hotel successfully updated');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Hotel update failed');
                }
                redirect('hotelListing');
            }
    }

     /**
     * This function is used to delete the Hotel using Id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteHotel($id)
    {
           if($id == null && $id < 0)
            {
                redirect('hotelListing');
            }
            $result = $this->hotel_model->deleteHotel($id);
            
			if($result > 0){
				 $process = 'Delete Hotel';
				 $processFunction = 'Admin/deleteHotel';
				 $this->logrecord($process,$processFunction);
				
					$this->session->set_flashdata('success', 'Hotel Deleted Successfully');
					redirect('hotelListing');
			} else{
				$this->session->set_flashdata('error', 'Hotel deleted failed');
				redirect('hotelListing');
			}
	}
	
	// delete multiple uploded photo
	function deleteExtraPhoto($hotelID, $id){
		
		if($hotelID == null || $hotelID <= 0)
			abort(404);	
		
		if($id == null || $id <= 0)
			abort(404);
		
		if(isset($id)){
			$res = $this->hotel_model->getExtraPhotoByID($id);
			if($res > 0){
				$fileNM = 'uploads/images/'.$res[0]->image;
				
				if(file_exists($fileNM)){
					unlink($fileNM);
				}
				$delRes = $this->hotel_model->deleteExtraPhoto($res[0]->id);
			}
		}
		if(isset($delRes) && $delRes > 0){
			echo json_encode(array('Del' => 'TRUE'));
		}else{
			echo json_encode(array('Del' => 'FALSE'));
		}
	}
	
	// delete single photo at update time
	function deletePhoto($id){
		if($id == null || $id <= 0)
			abort(404);
		
		if(isset($id)){
			$res = $this->hotel_model->getPhoto($id);
			
			if($res > 0){
				$fileNM = 'uploads/images/'.$res[0]->hotelPhoto;
				if(file_exists($fileNM)){
					unlink($fileNM);
				}
				//$delRes = $this->hotel_model->deletePhoto($res[0]->id);
			}
		}
		/*if(isset($delRes) && $delRes > 0){
			echo json_encode(array('Del' => 'TRUE'));
		}else{
			echo json_encode(array('Del' => 'FALSE'));
		}*/
	}

     
    /**
     * This function used to upload backup for backup_log table
     */
    function logHistoryUploadFile()
    {
        $optioninput = $this->input->post('optionfilebackup');

        if ($optioninput == '0' && $_FILES['filebackup']['name'] != '')
        {
            $config = array(
            'upload_path' => "./uploads/",
            'allowed_types' => "gz|sql|gzip",
            'overwrite' => TRUE,
            'max_size' => "20048000", // Can be set to particular file size , here it is 20 MB(20048 Kb)
            );

            $this->load->library('upload', $config);
            $upload= $this->upload->do_upload('filebackup');
                $data = $this->upload->data();
                $filepath = $data['full_path'];
                $path_parts = pathinfo($filepath);
                $filetype = $path_parts['extension'];
                if ($filetype == 'gz')
                {
                    // Read entire gz file
                    $lines = gzfile($filepath);
                    $lines = str_replace('tbl_log','tbl_log_backup', $lines);
                }
                else
                {
                    // Read in entire file
                    $lines = file($filepath);
                    $lines = str_replace('tbl_log','tbl_log_backup', $lines);
                }
        }

        else if ($optioninput != '0' && $_FILES['filebackup']['name'] == '')
        {
            $filepath = './backup/'.$optioninput;
            $path_parts = pathinfo($filepath);
            $filetype = $path_parts['extension'];
            if ($filetype == 'gz')
            {
                // Read entire gz file
                $lines = gzfile($filepath);
                $lines = str_replace('tbl_log','tbl_log_backup', $lines);
            }
            else
            {
                // Read in entire file
                $lines = file($filepath);
                $lines = str_replace('tbl_log','tbl_log_backup', $lines);
            }
        }
                // Set line to collect lines that wrap
                $templine = '';
                
                // Loop through each line
                foreach ($lines as $line)
                {
                    // Skip it if it's a comment
                    if (substr($line, 0, 2) == '--' || $line == '')
                    continue;
                    // Add this line to the current templine we are creating
                    $templine .= $line;

                    // If it has a semicolon at the end, it's the end of the query so can process this templine
                    if (substr(trim($line), -1, 1) == ';')
                    {
                        // Perform the query
                        $this->db->query($templine);

                        // Reset temp variable to empty
                        $templine = '';
                    }
                }
            if (empty($lines) || !isset($lines))
            {
                $this->session->set_flashdata('error', 'Yedek yükleme işlemi başarısız');
                redirect('log-history-upload');
            }
            else
            {
                $this->session->set_flashdata('success', 'Yedek yükleme işlemi başarılı');
                redirect('log-history-upload');
            }
    }
	
	
}
