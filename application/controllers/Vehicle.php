<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';
/**
 * Class : Hotel (AdminController)
 * Hotel.
 * @author : bhavinpethani333@gmail.com
 * @version : 1.0
 * @since : 23.07.2018
 */
class Vehicle extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Vehicle_model');
        // Datas -> libraries ->BaseController / This function used load user sessions
        $this->datas();
        // isLoggedIn / Login control function /  This function used login control
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('login');
        }
        
    }
	
     /**
     * This function is used to load the user list
     */
    function vehicleListing()
    {   
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->Vehicle_model->vehicleListingCount($searchText);

			$returns = $this->paginationCompress("hotelListing/",$count,$count);
            //echo $returns["page"];echo $returns["segment"];exit;
            $data['hotelRecords'] = $this->Vehicle_model->vehicleListing($searchText, $returns["page"], $returns["segment"]);
                 
            $this->global['pageTitle'] = SITE_NAME.' : Vehicle List';
           
            $this->loadViews("vehicle", $this->global, $data, NULL);
    }

    /**
     * This function is used to load the add new form
     */
    function addVehicle()
    {
           
            $this->global['pageTitle'] = SITE_NAME.' : ADD Vehicle';
            
            $data['countries'] = $this->getcountry();
            
            $this->loadViews("addVehicle", $this->global,$data,NULL);
    }

    /**
     * This function is used to add new user to the system
     */
    function insertVehicle()
    {
            $this->form_validation->set_rules('vehical_name','Vehicle Name','trim|required|max_length[255]');
            if($this->form_validation->run() == FALSE)
            {
                $this->addVehicle();
            }
            else
            {   
                $hotelInfo = array(
                    'vehical_name' => $this->input->post('vehical_name'),
                    );
                                    
                $last_insert_id = $this->Vehicle_model->insertVehicle($hotelInfo);
                if($last_insert_id > 0)
                {
                   $this->session->set_flashdata('success', 'Your Vehicle added successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Something wrong in your information');
                }
                redirect('vehicleListing');
            }
        }
	
     /**
     * This function is used load hotel edit information
     * @param number $hotelId : Optional : This is hotel id
     */
    function editVehicle($hotelId = NULL)
    {
        if($hotelId == null)
        {
            redirect('vehicleListing');
        }
        $data['vehicle'] = $this->Vehicle_model->getVehicle($hotelId);
		$this->global['pageTitle'] = SITE_NAME.' : Edit Vehicle';
        $this->loadViews("addVehicle", $this->global, $data, NULL);
    }


    /**
     * This function is used to edit the Hotel informations
     */
    function updateVehicle()
    {
        $hotelId = $this->input->post('id');
       $this->form_validation->set_rules('vehical_name','hotel Name','trim|required|max_length[255]');
        if($this->form_validation->run() == FALSE)
        {
            $this->editVehicle($hotelId);
        }
        else
        {
			
            $hotelInfo = array(
                'vehical_name' => $this->input->post('vehical_name'),
                );
            $result = $this->Vehicle_model->updateVehicle($hotelInfo, $hotelId);
            if($result == true)
            {
                $this->session->set_flashdata('success', 'Vehicle successfully updated');
            }
            else
            {
                $this->session->set_flashdata('error', 'Vehicle update failed');
            }
            redirect('vehicleListing');
        }
    }

     /**
     * This function is used to delete the Hotel using Id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteVehicle($id)
    {
           if($id == null && $id < 0)
            {
                redirect('vehicleListing');
            }
            $result = $this->Vehicle_model->deleteVehicle($id);
            
			if($result > 0){
		      $this->session->set_flashdata('success', 'Vehicle Deleted Successfully');
			redirect('vehicleListing');
			} else{
				$this->session->set_flashdata('error', 'Vehicle deleted failed');
				redirect('vehicleListing');
			}
	}

}
