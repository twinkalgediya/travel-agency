<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';
/**
 * Class : Package (AdminController)
 * Package.
 * @author : bhavinpethani333@gmail.com
 * @version : 1.0
 * @since : 23.07.2018
 */
class Package extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('package_model');
        $this->load->model('inquiry_model');
        // Datas -> libraries ->BaseController / This function used load user sessions
        $this->datas();
        // isLoggedIn / Login control function /  This function used login control
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('login');
        }
        
    }
	
     /**
     * This function is used to load the user list
     */
    function packageListing()
    {   
			
			$id = $this->uri->segment(2);
			
			if(!isset($id) && $id < 0){
				redirect('inquiryListing');
			}
			$data['inq_id'] = $id;
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->package_model->packageListingCount($searchText);

			$returns = $this->paginationCompress ( "packageListing/", $count, 10 );
            
            $data['packageRecords'] = $this->package_model->packageListing($searchText, $returns["page"], $returns["segment"], $id);
            
            $process = 'Package Listing';
            $processFunction = 'Admin/packageListing';
            $this->logrecord($process,$processFunction);
            
            $this->global['pageTitle'] = SITE_NAME.' : Package List';
			
            $this->loadViews("packages", $this->global, $data, NULL);
			
    }

    /**
     * This function is used to load the add new form
     */
    function addPackage()
    {
			$id = $this->uri->segment(2);
			
			if(!isset($id) && $id < 0){
				redirect('inquiryListing');
			}
			
            $this->global['pageTitle'] = SITE_NAME.' : ADD Package';
			$data['Package'] = $this->inquiry_model->getInquiry($id);
			
            $this->loadViews("addPackage", $this->global, $data, NULL);
    }

    /**
     * This function is used to add new user to the system
     */
    function insertPackage()
    {
			$inquiryId = $this->input->post('inquiry_id');
			        
            $this->form_validation->set_rules('firstName','First Name','trim|required|max_length[255]');
            $this->form_validation->set_rules('lastName','last Name','trim|required|max_length[255]');
            $this->form_validation->set_rules('travelDate','Travel Date','required');
            $this->form_validation->set_rules('noOffAdults','Travel Date','required|numeric');
            $this->form_validation->set_rules('childWithExtraBed','Child With ExtraBed','numeric');
            $this->form_validation->set_rules('childWithoutExtraBed','Child Without ExtraBed','numeric');
			$this->form_validation->set_rules('packageName','Package Name','trim|required|max_length[255]');
			$this->form_validation->set_rules('packagePrice','Package Price','trim|required|max_length[255]');
			$this->form_validation->set_rules('depatureCity','Depature City','trim|required|max_length[255]');
			$this->form_validation->set_rules('nightStayPlan','Night Stay Plan','trim|required|max_length[255]');
			$this->form_validation->set_rules('amountPerPerson','Amount Per Person','trim|max_length[255]');
			$this->form_validation->set_rules('amountPerChild','Amount Per Child','trim|max_length[255]');
			$this->form_validation->set_rules('itinanrary_data','Itinanrary','trim|required');
			$this->form_validation->set_rules('hotelDetail','Hotel Detail','trim|required');
			$this->form_validation->set_rules('fliteDetail','Flite Detail','trim|required');
			$this->form_validation->set_rules('packageInclusion','Package Inclusions','trim|required|max_length[255]');
            
           
            if($this->form_validation->run() == FALSE)
            {
				$this->session->set_flashdata('error', 'Something wrong in your information');
                redirect('inquiryListing/');
            }
			else
            {
				$inquiryInfo = array(
                    'firstName' => $this->input->post('firstName'),
                    'lastName' => $this->input->post('lastName'),
                    'travelDate' => $this->input->post('travelDate'),
                    'noOffAdults' => $this->input->post('noOffAdults'),
                    'childWithExtraBed' => $this->input->post('childWithExtraBed'),
                    'childWithoutExtraBed' => $this->input->post('childWithoutExtraBed'),
                );
				
				$packageInfo = array(
                    'packageName' => $this->input->post('packageName'),
                    'packagePrice' => $this->input->post('packagePrice'),
                    'depatureCity' => $this->input->post('depatureCity'),
                    'hotelDetail' => $this->input->post('hotelDetail'),
                    'fliteDetail' => $this->input->post('fliteDetail'),
                    'amountPerPerson' => $this->input->post('amountPerPerson'),
                    'amountPerChild' => $this->input->post('amountPerChild'),
                    'nightStayPlan' => $this->input->post('nightStayPlan'),
                    'packageInclusion' => $this->input->post('packageInclusion'),
                    'itinanrary_data' => $this->input->post('itinanrary_data'),
                    'inquiryId' => $inquiryId,
                    'addedBy' => $this->session->userdata['userId'],
                );
				$result1 = $this->inquiry_model->updateInquiry($inquiryInfo, $inquiryId);	
				$result = $this->package_model->insertPackage($packageInfo);
                
				if($result1 > 0)
                {
                    $process = 'Update Inquiry';
                    $processFunction = 'package/addPackage';
                    $this->logrecord($process,$processFunction);
				}
				
				if($result > 0){
					   $data = array();
                    // If file upload form submitted
					if(!empty($_FILES['multiplepackagePhoto']['name']))
						{   
							$filesCount = count($_FILES['multiplepackagePhoto']['name']);
							for($i = 0; $i < $filesCount; $i++)
							{
								$_FILES['file']['name']     = $_FILES['multiplepackagePhoto']['name'][$i];
								$_FILES['file']['type']     = $_FILES['multiplepackagePhoto']['type'][$i];
								$_FILES['file']['tmp_name'] = $_FILES['multiplepackagePhoto']['tmp_name'][$i];
								$_FILES['file']['error']     = $_FILES['multiplepackagePhoto']['error'][$i];
								$_FILES['file']['size']     = $_FILES['multiplepackagePhoto']['size'][$i];
							   
								// File upload configuration
								$config1['upload_path'] = 'uploads/images';
								$config1['allowed_types'] = 'jpg|jpeg|png|gif';
								// Load and initialize upload library
								$this->load->library('upload', $config1);
								$this->upload->initialize($config1);
								// Upload file to server
								if($this->upload->do_upload('file')){
									// Uploaded file data
									$fileDatas = $this->upload->data();
									$galaryhotelPhotos['file_name'] = $fileDatas['file_name'];
									//$uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");

									if(!empty($galaryhotelPhotos)){
										// Insert files data into the database
										$galaryPhotoInfo = array(
											'packageId' => $result,
											'image'   => $galaryhotelPhotos['file_name']
										);
										
										$photores = $this->package_model->insertGalaryPhoto($galaryPhotoInfo);
									}
								}
								else
								{
									 //print_r($this->upload->display_errors());exit;
								}
							}
						}
				}
				
				if($result > 0)
                {	
					$process = 'Create Package';
                    $processFunction = 'package/addPackage';
                    $this->logrecord($process,$processFunction);

                    $this->session->set_flashdata('success', 'Your Package added successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Something wrong in your information');
                }
                
                redirect('packageListing/'.$inquiryId);
            }
        }

     /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editPackage($packageId = NULL)
    {
       
            if($packageId == null)
            {
                redirect('packageListing');
            }
            
            $data['Itn'] = $this->package_model->getPackage($packageId);
			
			if(isset($data['Itn']) &&  $data['Itn'][0]->inquiryId > 0){
				$data['Package'] = $this->inquiry_model->getInquiry($data['Itn'][0]->inquiryId);
			}
			
			$GalaryPhotoRes = $this->package_model->getExtraPhoto($packageId);
			
			$array_Photo = json_decode(json_encode($GalaryPhotoRes), True);
			$GalaryPhotodata = array();
			if(isset($array_Photo) && !empty($array_Photo)){
				foreach ($array_Photo as $value) {
				 $GalaryPhotodata[] = $value;
				}
			}
			
			$data['GalaryPhotodata'] = $GalaryPhotodata;
			
			$this->global['pageTitle'] = SITE_NAME.' : Edit Package';
            $this->loadViews("addPackage", $this->global, $data, NULL);
    }


    /**
     * This function is used to edit the user informations
     */
    function updatePackage()
    {
            $packageId = $this->input->post('package_id');
            $inquiryId = $this->input->post('inqId');
           
            $this->form_validation->set_rules('firstName','First Name','trim|required|max_length[255]');
            $this->form_validation->set_rules('lastName','last Name','trim|required|max_length[255]');
            $this->form_validation->set_rules('travelDate','Travel Date','required');
            $this->form_validation->set_rules('noOffAdults','Travel Date','required|numeric');
            $this->form_validation->set_rules('childWithExtraBed','Child With ExtraBed','numeric');
            $this->form_validation->set_rules('childWithoutExtraBed','Child Without ExtraBed','numeric');
			$this->form_validation->set_rules('packageName','Package Name','trim|required|max_length[255]');
			$this->form_validation->set_rules('packagePrice','Package Price','trim|required|max_length[255]');
			$this->form_validation->set_rules('depatureCity','Depature City','trim|required|max_length[255]');
			$this->form_validation->set_rules('nightStayPlan','Night Stay Plan','trim|required|max_length[255]');
			$this->form_validation->set_rules('amountPerPerson','Amount Per Person','trim|max_length[255]');
			$this->form_validation->set_rules('amountPerChild','Amount Per Child','trim|max_length[255]');
			$this->form_validation->set_rules('itinanrary_data','Itinanrary','trim|required');
			$this->form_validation->set_rules('hotelDetail','Hotel Detail','trim|required');
			$this->form_validation->set_rules('fliteDetail','Flite Detail','trim|required');
			$this->form_validation->set_rules('packageInclusion','Package Inclusions','trim|required|max_length[255]');
            
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editPackage($packageId);
            }
            else
            {
				//if multiple images are selected then  insert into hotel_photos
				$data = array();
                    // If file upload form submitted
					
                if(!empty($_FILES['multiplepackagePhoto']['name']))
                {   
                    $filesCount = count($_FILES['multiplepackagePhoto']['name']);
                    for($i = 0; $i < $filesCount; $i++)
                    {
                        $_FILES['file']['name']     = $_FILES['multiplepackagePhoto']['name'][$i];
                        $_FILES['file']['type']     = $_FILES['multiplepackagePhoto']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['multiplepackagePhoto']['tmp_name'][$i];
                        $_FILES['file']['error']     = $_FILES['multiplepackagePhoto']['error'][$i];
                        $_FILES['file']['size']     = $_FILES['multiplepackagePhoto']['size'][$i];
                       
                        // File upload configuration
                        $config2['upload_path'] = 'uploads/images';
                        $config2['allowed_types'] = 'jpg|jpeg|png|gif';
                        // Load and initialize upload library
                        $this->load->library('upload', $config2);
                        $this->upload->initialize($config2);
                        
                        // Upload file to server
                        if($this->upload->do_upload('file')){
                            // Uploaded file data
                            $fileDatas = $this->upload->data();
                            $multiplegalaryPhotos['file_name'] = $fileDatas['file_name'];
                            //$uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");

                            if(!empty($multiplegalaryPhotos)){
                                // Insert files data into the database
                                
								$galaryPhotoInfo = array(
									'packageId' => $packageId,
									'image'   => $multiplegalaryPhotos['file_name']
								);
								$result1 = $this->package_model->insertGalaryPhoto($galaryPhotoInfo);
							}
                        }
                    }
				}
				
				$inquiryInfo = array(
                    'firstName' => $this->input->post('firstName'),
                    'lastName' => $this->input->post('lastName'),
                    'travelDate' => $this->input->post('travelDate'),
                    'noOffAdults' => $this->input->post('noOffAdults'),
                    'childWithExtraBed' => $this->input->post('childWithExtraBed'),
                    'childWithoutExtraBed' => $this->input->post('childWithoutExtraBed'),
                );
				
				$packageInfo = array(
                    'packageName' => $this->input->post('packageName'),
                    'packagePrice' => $this->input->post('packagePrice'),
                    'depatureCity' => $this->input->post('depatureCity'),
                    'hotelDetail' => $this->input->post('hotelDetail'),
                    'fliteDetail' => $this->input->post('fliteDetail'),
                    'amountPerPerson' => $this->input->post('amountPerPerson'),
                    'amountPerChild' => $this->input->post('amountPerChild'),
                    'nightStayPlan' => $this->input->post('nightStayPlan'),
                    'packageInclusion' => $this->input->post('packageInclusion'),
                    'itinanrary_data' => $this->input->post('itinanrary_data'),
                );
				
                $result1 = $this->inquiry_model->updateInquiry($inquiryInfo, $inquiryId);	
				
				$result = $this->package_model->updatePackage($packageInfo, $packageId);
                
                if($result == true)
                {
                    $process = 'Package Update';
                    $processFunction = 'Admin/editPackage';
                    $this->logrecord($process,$processFunction);

                    $this->session->set_flashdata('success', 'Package successfully updated');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Package update failed');
                }
                
                redirect('packageListing/'.$inquiryId);
            }
    }

     /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deletePackage($id)
    {
           if($id == null && $id < 0)
            {
                redirect('packageListing');
            }
            $result = $this->inquiry_model->deletePackage($id);
            
			if($result > 0){
				 $process = 'Delete Package';
				 $processFunction = 'Admin/deletePackage';
				 $this->logrecord($process,$processFunction);
				
					$this->session->set_flashdata('success', 'Package Deleted Successfully');
					redirect('packageListing');
			} else{
				$this->session->set_flashdata('error', 'Inquiry deleted failed');
				redirect('packageListing');
			}
	}
	
	// delete multiple uploded photo
	function deleteGalaryPhoto($packageID, $id){
		
		if($packageID == null || $packageID <= 0)
			abort(404);	
		
		if($id == null || $id <= 0)
			abort(404);
		
		if(isset($id)){
			$res = $this->package_model->getExtraPhotoByID($id);
			if($res > 0){
				$fileNM = 'uploads/images/'.$res[0]->image;
				
				if(file_exists($fileNM)){
					unlink($fileNM);
				}
				$delRes = $this->package_model->deleteExtraPhoto($res[0]->id);
			}
		}
		if(isset($delRes) && $delRes > 0){
			echo json_encode(array('Del' => 'TRUE'));
		}else{
			echo json_encode(array('Del' => 'FALSE'));
		}
	}

     
}