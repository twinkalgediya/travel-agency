<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';
/**
 * Class : Night Stay Plan (AdminController)
 * Hotel.
 * @author : bhavinpethani333@gmail.com
 * @version : 1.0
 * @since : 23.07.2018
 */
class Noofnight extends BaseController
{

    /**
     * This is default constructor of the class
     */
    public function __construct()
    {

        parent::__construct();
        $this->load->model('Noofnigth_model');
        $this->load->model('Nightstayplan_model');
        // Datas -> libraries ->BaseController / This function used load user sessions
        $this->datas();
        // isLoggedIn / Login control function /  This function used login control
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('login');
        }
        
    }
	
     /**
     * This function is used to load the user list
     */
    function noofnightListing()
    {   
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->Noofnigth_model->noofnightListingCount($searchText);

			$returns = $this->paginationCompress("noofnightListing/",$count,$count);
            //echo $returns["page"];echo $returns["segment"];exit;
            $data['nightRecords'] = $this->Noofnigth_model->noofnightListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = SITE_NAME.' : Noofnight List';
           
            $this->loadViews("noofnight", $this->global, $data, NULL);
    }

    /**
     * This function is used to load the add new form
     */
    function addnoofnight()
    {
            
            $this->global['pageTitle'] = SITE_NAME.' : ADD Noofnight';
            $data['state'] = $this->getstate();
            $data['countries'] = $this->getcountry();
            
            $this->loadViews("addNight", $this->global,$data,NULL);
    }

    /**
     * This function is used to add new user to the system
     */
    function insertNoofnight()
    {
        print_r($this->input->post());
		$postdata=$this->input->post();
            $this->form_validation->set_rules('no_of_nights','No Off Night','trim|required');
            $this->form_validation->set_rules('typeof_place','Type Of Place','trim|required');
          
            if($this->form_validation->run() == FALSE)
            {
                $this->addNoofnight();
            }
            else
            {
				$nightInfo = array(
                    'no_of_nights' =>$this->input->post('no_of_nights'),
                    'typeof_place' =>$this->input->post('typeof_place'),
                  
                );
                   if(isset($postdata['country_id'])){
                     $nightInfo['country_id'] =$this->input->post('country_id');
                     
                   } 
                   if(isset($postdata['state_id']))
                   {
                        $nightInfo['state_id'] =$this->input->post('state_id');
                   }                
                $last_insert_id = $this->Noofnigth_model->insertNoofnight($nightInfo);
               
            
                if($last_insert_id > 0)
                {
                    $process = 'Create NightStayPlan';
                    $processFunction = 'hotel/addNightStayPlan';
                    $this->logrecord($process,$processFunction);
					$this->session->set_flashdata('success', 'Your No of night added successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Something wrong in your information');
                }
                
                redirect('noofnightListing');
            }
       
	}
     /**
     * This function is used load hotel edit information
     * @param number $hotelId : Optional : This is hotel id
     */
    function editNoofnight($nightId = NULL)
    {
       
            if($nightId == null)
            {
                redirect('noofnightListing');
            }
            $data['countries'] = $this->getcountry();
            $data['state'] = $this->getstate();
            $data['night'] = $this->Noofnigth_model->getNoofnight($nightId);
			
            
            $this->global['pageTitle'] = SITE_NAME.' : Edit Noofnight';
            $this->loadViews("addNight", $this->global, $data, NULL);
    }


    /**
     * This function is used to edit the Hotel informations
     */
    function updateNoofnight()
    {
            $nightId = $this->input->post('id');
           $postdata=$this->input->post();
            $this->form_validation->set_rules('no_of_nights','No Off Night','trim|required');
             $this->form_validation->set_rules('typeof_place','Type Of Place','trim|required');
          
            if($this->form_validation->run() == FALSE)
            {
                $this->editNoofnight
                ($nightId);
            }
            else
            {
				
				
                $nightInfo = array(
                    'typeof_place' => $this->input->post('typeof_place'),
                    'no_of_nights' =>$this->input->post('no_of_nights'),
                    );
                
                    if(isset($postdata['country_id'])){
                     $nightInfo['country_id'] =$this->input->post('country_id');
                     
                   } 
                   if(isset($postdata['state_id']))
                   {
                        $nightInfo['state_id'] =$this->input->post('state_id');
                   }               

                $result = $this->Noofnigth_model->updateNightStayPlan($nightInfo, $nightId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Noofnight successfully updated');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Noofnight update failed');
                }
                redirect('noofnightListing');
            }
    }

     /**
     * This function is used to delete the Hotel using Id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteNoofnight($id)
    {
    	
           if($id == null && $id < 0)
            {
                redirect('noofnightListing');
            }
            $result = $this->Noofnigth_model->deletenight($id);
            
			if($result > 0){
				 $this->session->set_flashdata('success', 'Noofnight Deleted Successfully');
					redirect('noofnightListing');
			} else{
				$this->session->set_flashdata('error', 'Noofnight deleted failed');
				redirect('noofnightListing');
			}
	}

   function getInternational()
   {
    $plan = $this->input->post('plan');
    $international = array();
        if($plan){
           $international = $this->Noofnigth_model->getInternationalrows($plan);
        }
        echo json_encode($international);
   }
   function getNoofNightbycountry()
   {
        $country_id = $this->input->post('country_id');
        $international = array();
        if($country_id){
           $international = $this->Noofnigth_model->getnightbycountryrows($country_id);
        }
        echo json_encode($international);
   }
   function getNoofNightbystate()
   {
        $state_id = $this->input->post('state_id');
        $international = array();
        if($state_id){
           $international = $this->Noofnigth_model->getnightbystaterows($state_id);
        }
        echo json_encode($international);
   }
  
}
