<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';
/**
 * Class : Inquiry (EmployeeController)
 * Inquiry.
 * @author : bhavinpethani333@gmail.com
 * @version : 1.0
 * @since : 23.07.2018
 */
class Employee extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('employee_model');
        $this->load->model('login_model');
        // Datas -> libraries ->BaseController / This function used load user sessions
        //$this->datas();
        // isLoggedIn / Login control function /  This function used login control
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('login');
        }
        
    }
	
     /**
     * This function is used to load the employee list
     */
    function employeeListing()
    {   
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->employee_model->employeeListingCount($searchText);

			$returns = $this->paginationCompress ( "employeeListing/", $count, $count );
            
            $data['employeeRecords'] = $this->employee_model->employeeListing($searchText, $returns["page"], $returns["segment"]);
            
            $process = 'Employee Listing';
            $processFunction = 'Admin/employeeListing';
            $this->logrecord($process,$processFunction);
            
            $this->global['pageTitle'] = SITE_NAME.' : Employee List';
           
            $this->loadViews("employees", $this->global, $data, NULL);
    }

    /**
     * This function is used to load the add new form
     */
    function addEmployee()
    {
            
            $this->global['pageTitle'] = SITE_NAME.' : ADD Employee';
			$data['getUserRoles'] = $this->employee_model->getUserRoles();
			$this->loadViews("addEmployee", $this->global, $data);
    }

    /**
     * This function is used to add new user to the system
     */
    function insertEmployee()
    {
                   
        $this->form_validation->set_rules('email','Email','trim|required|is_unique[tbl_users.email]|valid_email|max_length[128]');
            $this->form_validation->set_rules('name','Name','trim|required|max_length[128]');
            $this->form_validation->set_rules('mobile','Phone','trim|required');
            $this->form_validation->set_rules('roleId','Role','trim|required');
			$this->form_validation->set_rules('password','Password','required|max_length[20]');
			$this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]');
            
           
            if($this->form_validation->run() == FALSE)
            {
                $this->addEmployee();
            }
			else
            {
				$employeeInfo = array(
                    'name' => $this->input->post('name'),
                    'email' => $this->input->post('email'),
                    'mobile' => $this->input->post('mobile'),
                    'roleId' => $this->input->post('roleId'),
                    'password' => getHashedPassword($this->input->post('password')),
                );
                                    
				$result = $this->employee_model->insertEmployee($employeeInfo);
            
				if($result > 0)
				{
					$process = 'Create Employee';
					$processFunction = 'employee/addEmployee';
					$this->logrecord($process,$processFunction);

					$this->session->set_flashdata('success', 'Your employee added successfully');
				}
				else
				{
					$this->session->set_flashdata('error', 'Something wrong in your information');
				}
			redirect('employeeListing');
        }
	}
     /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editEmployee($employeeId = NULL)
    {
       
            if($employeeId == null)
            {
                redirect('employeeListing');
            }
            
            $data['Employee'] = $this->employee_model->getEmployee($employeeId);
			
			$data['getUserRoles'] = $this->employee_model->getUserRoles();
			//print_r($data['getUserRoles']);exit;
            $this->global['pageTitle'] = SITE_NAME.' : Edit Employee';
            $this->loadViews("addEmployee", $this->global, $data, NULL);
    }


    /**
     * This function is used to edit the user informations
     */
    function updateEmployee()
    {
            $employeeId = $this->input->post('userId');
           
	//$this->form_validation->set_rules('email','Email','trim|required|is_unique[tbl_users.email]|[tbl_users.userId!='.$employeeId.']|valid_email|max_length[128]');
            $this->form_validation->set_rules('name','Name','trim|required|max_length[128]');
            $this->form_validation->set_rules('mobile','Phone','trim|required');
            $this->form_validation->set_rules('roleId','Role','trim|required');
			//$this->form_validation->set_rules('password','Password','required|max_length[20]');
			//$this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]');
           
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editEmployee($employeeId);
            }
            else
            {
				$employeeInfo = array(
                    'email' => $this->input->post('email'),
					'name' => $this->input->post('name'),
                    'mobile' => $this->input->post('mobile'),
				);
                
                $result = $this->employee_model->updateEmployee($employeeInfo, $employeeId);
                
                if($result == true)
                {
                    $process = 'Employee Update';
                    $processFunction = 'Admin/editEmployee';
                    $this->logrecord($process,$processFunction);

                    $this->session->set_flashdata('success', 'Employee successfully updated');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Employee update failed');
                }
                
                redirect('employeeListing');
            }
    }

     /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteEmployee($id)
    {
           if($id == null && $id < 0)
            {
                redirect('employeeListing');
            }
            $result = $this->employee_model->deleteEmployee($id);
            
			if($result > 0){
				 $process = 'Delete Employee';
				 $processFunction = 'Admin/deleteEmployee';
				 $this->logrecord($process,$processFunction);
				
					$this->session->set_flashdata('success', 'Employee Deleted Successfully');
					redirect('employeeListing');
			} else{
				$this->session->set_flashdata('error', 'Employee deleted failed');
				redirect('employeeListing');
			}
	}

}