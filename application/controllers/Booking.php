<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';
/**
 * Class : Package (AdminController)
 * Package.
 * @author : bhavinpethani333@gmail.com
 * @version : 1.0
 * @since : 23.07.2018
 */
class Booking extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('booking_model');
        $this->load->model('package_model');
        $this->load->model('inquiry_model');
        // Datas -> libraries ->BaseController / This function used load user sessions
        $this->datas();
        // isLoggedIn / Login control function /  This function used login control
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('login');
        }
        
    }
	
     /**
     * This function is used to load the user list
     */
    function bookingListing()
    {   
            
            $id = $this->uri->segment(2);
            
            if(!isset($id) && $id < 0){
                redirect('inquiryListing');
            }
            //$data['inq_id'] = $id;
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->booking_model->bookingListingCount($searchText);

            $returns = $this->paginationCompress ( "bookingListing/", $count, 10 );
            
            $data['bookingRecords'] = $this->booking_model->bookingListing($searchText, $returns["page"], $returns["segment"], $id);
            
            $process = 'Booking Listing';
            $processFunction = 'Admin/bookingListing';
            $this->logrecord($process,$processFunction);
            
            $this->global['pageTitle'] = SITE_NAME.' : Booking List';
            
            $this->loadViews("bookings", $this->global, $data, NULL);
            
    } 
   /**
     * This function is used to load the add new form
     */
    function addBooking()
    {   
			$inquery = $this->uri->segment(2);
            $package = $this->uri->segment(3);

            if(!isset($inquery) && $inquery < 0){
                redirect('packageListing');
            }
            if(!isset($package) && $package < 0){
                redirect('packageListing');
            }

			$this->global['pageTitle'] = SITE_NAME.' : Create Booking';
			$data['Booking'] = $this->inquiry_model->getInquiry($inquery);
            $data['countries']   = $this->getcountry();
            $data['Package']     = $this->package_model->getPackage($package);
           // print_r($data['Booking']);exit;
			
            $this->loadViews("addbooking", $this->global, $data, NULL);
    }


    /**
     * This function is used to add new user to the system
     */
    function insertBooking()
    {

        $inquiryId = $this->input->post('inquiry_id');
        $packageId = $this->input->post('package_id');
      // echo "<pre>"; print_r($this->input->post());exit;            
            $this->form_validation->set_rules('totalAmtReceve','Total Amount receive','required');
            /*$this->form_validation->set_rules('countryId','Country','required');
            $this->form_validation->set_rules('stateId','State','required'); 
            $this->form_validation->set_rules('cityId','City','trim|required');
            $this->form_validation->set_rules('checkInDate','CheckIn Date','trim|required');
            $this->form_validation->set_rules('checkOutDate','CheckOut Date','trim|required');
            $this->form_validation->set_rules('noOfRoom','No Of Room','trim|required');
            $this->form_validation->set_rules('roomType','Room Type','trim|required');
            $this->form_validation->set_rules('netPayToHotel','Net Pay To Hotel','trim|required');
            $this->form_validation->set_rules('paymentDueDate','Payment DueDate','trim|required');
            $this->form_validation->set_rules('partialPayAmount','partial PayAmount','trim|required');
            $this->form_validation->set_rules('partialPayDueDate','partialPay DueDate','trim|required');
*/
           
            if($this->form_validation->run() == FALSE)
            {
                $this->session->set_flashdata('error', 'Something wrong in your information');
                redirect('inquiryListing/');
            }
            else
            {
				$BookingCount = $this->booking_model->getCount();
				if($BookingCount > 0){
					$countID = $BookingCount + 1;
				}else{
					$countID = 1;
				}
                $bookingInfo = array(
                    'inquiryId' => $this->input->post('inquiry_id'),
                    'packageId' => $this->input->post('package_id'),
                    'totalAmtReceve' => $this->input->post('totalAmtReceve'),
                    'bookingId' =>'VH'.date('Y').'B'.$countID,
					'addedBy' => $this->session->userdata['userId'],
                );
                
                $insBooking = $this->booking_model->insertBooking($bookingInfo);

                if($insBooking > 0){
                    $hotelId           = $this->input->post('hotelId');
                    $countryId         = $this->input->post('countryId');
                    $stateId           = $this->input->post('stateId');
                    $cityId            = $this->input->post('cityId');
                    $hotelId           = $this->input->post('hotelId');
                    $checkInDate       = $this->input->post('checkInDate');
                    $checkOutDate      = $this->input->post('checkOutDate');
                    $noOfRoom          = $this->input->post('noOfRoom');
                    $extraBed          = $this->input->post('extraBed');
                    $mealPlan          = $this->input->post('mealPlan');
                    $roomType          = $this->input->post('roomType');
                    $extraInclusion    = $this->input->post('extraInclusion');
                    $netPayToHotel     = $this->input->post('netPayToHotel');
                    $remarks           = $this->input->post('remarks');
                    $paymentDueDate    = $this->input->post('paymentDueDate');
                    $partialPayAmount  = $this->input->post('partialPayAmount');
                    $partialPayDueDate = $this->input->post('partialPayDueDate');

                    $hotelInfo = array();
                    for($i=0; $i<count($hotelId); $i++){
                        
                        $hotelInfo = array(
                            'bookingId'         => $insBooking,
                            'countryId'         => $countryId[$i],
                            'stateId'           => $stateId[$i],
                            'cityId'            => $cityId[$i],
                            'hotelId'           => $hotelId[$i],
                            'checkInDate'       => $checkInDate[$i],
                            'checkOutDate'      => $checkOutDate[$i],
                            'noOfRoom'          => $noOfRoom[$i],
                            'extraBed'          => $extraBed[$i],
                            'mealPlan'          => $mealPlan[$i],
                            'roomType'          => $roomType[$i],
                            'extraInclusion'    => $extraInclusion[$i],
                            'netPayToHotel'     => $netPayToHotel[$i],
                            'remarks'           => $remarks[$i],
                            'paymentDueDate'    => $paymentDueDate[$i],
                            'partialPayAmount'  => $partialPayAmount[$i],
                            'partialPayDueDate' => $partialPayDueDate[$i],
                        );
                 
                        $result = $this->booking_model->insertHotelDetail($hotelInfo);
                    }   
                } 
   
                if($result > 0)
                {   
                    $process = 'Create Booking';
                    $processFunction = 'Booking/addBooking';
                    $this->logrecord($process,$processFunction);

                    $this->session->set_flashdata('success', 'Your Booking added successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Something wrong in your information');
                }
                
                redirect('bookingListing');
            }
        }

     /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
        function editBooking($bookingId = NULL)
        {
           
                if($bookingId == null)
                {
                    redirect('packageListing');
                }
                
                $data['Booking'] = $this->booking_model->getBooking($bookingId);
                
                if(isset($data['Booking']) &&  $data['Booking'][0]->bookingAutoId > 0){
                    $data['HotelDetail'] = $this->booking_model->getHotelDetail($data['Booking'][0]->bookingAutoId);
                }
                
                 $data['countries'] = $this->getcountry();
                 $data['state'] = $this->getstate();
                 $data['cities'] = $this->getcity();
                 $data['Hotel'] = $this->getHotelList();
                //echo "<pre>";
                //print_r($data['HotelDetail']);exit;
                $this->global['pageTitle'] = SITE_NAME.' : Edit Booking';
                $this->loadViews("addBooking", $this->global, $data, NULL);
        }

        /**
     * This function is used to update booking to the system
     */
    function updateBooking()
    {

        $inquiryId = $this->input->post('inquiry_id');
        $packageId = $this->input->post('package_id');
        $bookingId = $this->input->post('id');
      //echo "<pre>"; print_r($this->input->post());exit;            
            $this->form_validation->set_rules('totalAmtReceve','Total Amount receive','required');
            /*$this->form_validation->set_rules('countryId','Country','required');
            $this->form_validation->set_rules('stateId','State','required'); 
            $this->form_validation->set_rules('cityId','City','trim|required');
            $this->form_validation->set_rules('checkInDate','CheckIn Date','trim|required');
            $this->form_validation->set_rules('checkOutDate','CheckOut Date','trim|required');
            $this->form_validation->set_rules('noOfRoom','No Of Room','trim|required');
            $this->form_validation->set_rules('roomType','Room Type','trim|required');
            $this->form_validation->set_rules('netPayToHotel','Net Pay To Hotel','trim|required');
            $this->form_validation->set_rules('paymentDueDate','Payment DueDate','trim|required');
            $this->form_validation->set_rules('partialPayAmount','partial PayAmount','trim|required');
            $this->form_validation->set_rules('partialPayDueDate','partialPay DueDate','trim|required');
*/
           
            if($this->form_validation->run() == FALSE)
            {
                $this->session->set_flashdata('error', 'Something wrong in your information');
                redirect('editBooking/'.$bookingId);
            }
            else
            {
                $bookingInfo = array(
                    'inquiryId' => $this->input->post('inquiry_id'),
                    'packageId' => $this->input->post('package_id'),
                    'totalAmtReceve' => $this->input->post('totalAmtReceve'),
                );
                
                $insBooking = $this->booking_model->updateBooking($bookingInfo, $bookingId);
                if($insBooking > 0){
                    $deleted = $this->booking_model->deleteBookingDetail($bookingId);
                }
               
                    $countryId         = $this->input->post('countryId');
                    $stateId           = $this->input->post('stateId');
                    $cityId            = $this->input->post('cityId');
                    $hotelId           = $this->input->post('hotelId');
                    $checkInDate       = $this->input->post('checkInDate');
                    $checkOutDate      = $this->input->post('checkOutDate');
                    $noOfRoom          = $this->input->post('noOfRoom');
                    $extraBed          = $this->input->post('extraBed');
                    $mealPlan          = $this->input->post('mealPlan');
                    $roomType          = $this->input->post('roomType');
                    $extraInclusion    = $this->input->post('extraInclusion');
                    $netPayToHotel     = $this->input->post('netPayToHotel');
                    $remarks           = $this->input->post('remarks');
                    $paymentDueDate    = $this->input->post('paymentDueDate');
                    $partialPayAmount  = $this->input->post('partialPayAmount');
                    $partialPayDueDate = $this->input->post('partialPayDueDate');

                    $hotelInfo = array();
                    for($i=0; $i<count($hotelId); $i++){
                        
                        $hotelInfo = array(
                            'bookingId'         => $bookingId,
                            'countryId'         => $countryId[$i],
                            'stateId'           => $stateId[$i],
                            'cityId'            => $cityId[$i],
                            'hotelId'           => $hotelId[$i],
                            'checkInDate'       => $checkInDate[$i],
                            'checkOutDate'      => $checkOutDate[$i],
                            'noOfRoom'          => $noOfRoom[$i],
                            'extraBed'          => $extraBed[$i],
                            'mealPlan'          => $mealPlan[$i],
                            'roomType'          => $roomType[$i],
                            'extraInclusion'    => $extraInclusion[$i],
                            'netPayToHotel'     => $netPayToHotel[$i],
                            'remarks'           => $remarks[$i],
                            'paymentDueDate'    => $paymentDueDate[$i],
                            'partialPayAmount'  => $partialPayAmount[$i],
                            'partialPayDueDate' => $partialPayDueDate[$i],
                        );
                 
                        $result = $this->booking_model->insertHotelDetail($hotelInfo);
                    }   
               
   
                if($result > 0)
                {   
                    $process = 'Update Booking';
                    $processFunction = 'Booking/updateBooking';
                    $this->logrecord($process,$processFunction);

                    $this->session->set_flashdata('success', 'Your Booking updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Something wrong in your information');
                }
                
                redirect('bookingListing');
            }
        }

    /* User For Dropdown Value Get */
    public function getStates(){
        $states = array();
        $country_id = $this->input->post('country_id');
        if($country_id){
            $states = $this->package_model->getStateRows($country_id);
        }
        echo json_encode($states);
    }

     public function getCities(){
        $cities = array();
        $state_id = $this->input->post('state_id');
        if($state_id){
           $cities = $this->package_model->getCityRows($state_id);
        }
        echo json_encode($cities);
    }
     public function getHotel(){
        $hotel = array();
        $city_id = $this->input->post('city_id');
        $state_id = $this->input->post('state_id');
		
        if($city_id){
           $hotel = $this->booking_model->getHotelRows($state_id,$city_id);
        }
		
        echo json_encode($hotel);
    }

     function getHotelList()
    {
        $this->db->select('id, hotelName');
        $this->db->from('tbl_hotel');
        $query = $this->db->get();
        return $query->result();
    }

}