<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';
/**
 * Class : Create PDF (PDFContrroler)
 * Package.
 * @author : bhavinpethani333@gmail.com
 * @version : 1.0
 * @since : 23.07.2018
 */
 
class CreatePdf extends BaseController
{

    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('package_model');
        $this->load->model('inquiry_model');
        // Datas -> libraries ->BaseController / This function used load user sessions
        $this->datas();
        // isLoggedIn / Login control function /  This function used login control
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('login');
        }
        
    }
	


    function ganratePdf()
    {
		$id = $this->uri->segment(2);
			
		if(!isset($id) && $id < 0){
			redirect('packageListing');
		}
		ini_set('memory_limit', '256M');
        
        
		//$data = [];
		
		// retrieve data from model
        $data['package'] = $this->package_model->getPackageWithGalary($id);
        $data['galary'] = $this->package_model->getPackageGalary($id);
		//echo $data['package'][0]->paymentLink;exit;
		$this->global['pageTitle'] = SITE_NAME.' : '.$data['package'][0]->packageName;
			
		
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4']);
		$mpdf->SetDisplayMode('fullpage');
		$html = $this->load->view('createpdf', $data, TRUE);
		
		$mpdf->WriteHTML($html);
		
        //$mpdf->Output(); // opens in browser
        $mpdf->Output($data['package'][0]->packageName.'.pdf','D'); // it downloads the file into the user system, with give name
		
    }
}
/* End of file dashboard.php */
/* Location: ./system/application/modules/matchbox/controllers/dashboard.php */