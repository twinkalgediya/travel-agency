<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"> 
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
 <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li>
                                <h4 class="page-title">Dashboard</h4>
                            </li>
							<li>
                                <a href="<?php echo base_url(); ?>hotelListing">Vehicle</a>
                            </li>
                            
                            <li class="active">
                               <?php echo isset($vehicle[0]->id) ? 'Edit' : 'Add' ?>
                            </li>
                        </ol>
                    </div>
                </div>

				
                <div class="row">
                    <div class="col-sm-12">
					<label class="col-md-2 control-label"></div>
									
					<form action="<?php echo base_url() ?><?php echo isset($vehicle[0]->id) ? 'Vehicle/updateVehicle' : 'Vehicle/insertVehicle' ?>" name="insertVehicle" id="insertVehicle" method="post" enctype="multipart/form-data" class="form-horizontal" data-parsley-validate novalidate>
                        <div class="row">
                            <div class="col-sm-12">
							
                                <div class="card-box">
                                    
										<h5 class="text-muted text-uppercase m-t-0 m-b-20"><b>General</b></h5>
										
											<div class="row">
												<div class="col-md-12">
													<?php //echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
												</div>
											</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Vehicle Name :<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="vehical_name" name="vehical_name" class="form-control <?php if( form_error('vehical_name') != '') { ?> parsley-error <?php } ?>"
											value="<?php echo isset($vehicle[0]->vehical_name) ? set_value("vehical_name", $vehicle[0]->vehical_name) : set_value("vehical_name"); ?>"  placeholder="Enter Vehicle Name" parsley-trigger="change" required>
											<?php if(isset($vehicle[0]->id)) { ?>
												<input type="hidden" name="id" id="id" value="<?php echo $vehicle[0]->id; ?>" >
										    <?php } ?>
											
										</div>
									</div>
								
									<hr />
									<div class="form-group p-20">
										<label class="col-md-1 control-label"></label>
										
										<div class="col-md-11">
										 <button type="submit" class="btn w-sm btn-default waves-effect waves-light">Save</button>
										 <button type="button" class="btn w-sm btn-white waves-effect"><a href="<?php echo base_url(); ?>VehicleListing">Cancel</a></button>
										</div> 
									</div>
                                </div>
							</div>
						</div>
					</form>
                </div>
            </div>

<?php $this->load->view('includes/footer', ''); ?>
<script type="text/javascript">

jQuery(document).ready(function($) {
		$('.counter').counterUp({
			delay: 100,
			time: 1200
		});
		$(".knob").knob();
		
		$('form').parsley();

    });
  
</script>