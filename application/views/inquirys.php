
<!-- DataTables -->
<link href="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">

<!-- Page-Title -->
<div class="row">
	<div class="col-sm-12">
		<ol class="breadcrumb">
			<li>
				<a href="<?php echo base_url(); ?>"><h4 class="page-title">Dashboard</h4></a>
			</li>
			<li>
				<a href="<?php echo base_url(); ?>inquiryListing">Inquiry</a>
			</li>
			<li class="active">
				List
			</li>
		</ol>
	</div>
</div>
<div class="row">
<div class="col-sm-12">
	<div class="card-box table-responsive">
		<div class="btn-group pull-right m-t-15">
			<a class="btn btn-default dropdown-toggle waves-effect waves-light" href="<?php echo base_url(); ?>addInquiry"><span class="m-l-5"><i class="md md-add-circle"></i></span> Add New </a>
			
		</div>
		<h4 class="m-t-0 m-b-15 header-title"><b>Inquiry List</b></h4>
		<div class="col-md-12">
			<div class="col-md-2 m-b-15">
				<div class="input-group date" id="fl_st_date1">
					<input type="text" name="fl_st_date" id="fl_st_date" class="form-control" placeholder="Select End Date">
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
			<div class="col-md-2 m-b-15">
				<div class="input-group date" id="fl_end_date1">
					<input type="text" name="fl_end_date" id="fl_end_date" class="form-control" placeholder="Select End Date">
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div> 
			<div class="col-md-2 m-b-15">
				<div class="col-md-12">
					<select name="prospect" id="prospect" class="form-control">
						<option value="">Select Prospect</option>
						<option value="Cold" >Cold</option>
						<option value="Hot" >Hot</option>
						<option value="Warm">Warm</option>
					</select>
				</div>	
			</div>
			<div class="col-md-2 m-b-15">
				<div class="col-md-12">
					<select name="source" id="source" class="form-control">
						<option value="">Select Source</option>
						<option value="HelloTravel">HelloTravel</option>
						<option value="Web">Web</option>
						<option value="Reference">Reference</option>
						<option value="HolidayIq">Holiday IQ</option>
						<option value="Phone">Phone</option>
						<option value="JustDial">JustDial</option>
						<option value="Data Diary">Data Diary</option>
						<option value="Sulekha">Sulekha</option>
					</select>
				</div>	
			</div>
			
			<div class="col-md-2 m-b-15">
			  <input type="reset" name="search" id="search" value="Search" class="btn btn-info" />
			</div>
		</div>	
		<div class="col-md-12">	
		<?php
			$error = $this->session->flashdata('error');
			if($error){ ?>
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<?php echo $this->session->flashdata('error'); ?>
			</div>
			<?php } ?>
			<?php  
			$success = $this->session->flashdata('success');
			if($success)
			{ ?>
			<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<?php echo $this->session->flashdata('success'); ?>
			</div>
		<?php } ?>
		</div>
			 
			  
    		
		<table id="datatable-responsive"
			   class="table table-striped table-bordered dt-responsive nowrap inquiry_table" cellspacing="0"
			   width="100%">
			<thead>
				<tr>
					<th>Inquiry Id</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>phone</th>
					<th>prospect</th>
					<th>source</th>
					<th>travelDate</th>
					
					<th>FollowupDate</th>
					<th>Status</th>
					<th>action</th>
				</tr>
			</thead>
			<tbody>
				<?php if(!empty($inquiryRecords)){
					foreach($inquiryRecords as  $value) { ?>		
						<tr>
							<td>
								<?php echo $value->inquiryID ?>
							</td>
							<td>
								<?php echo $value->firstName ?>
							</td>
							<td>
								<?php echo $value->lastName ?>
							</td>
							<td>
								<?php echo $value->phoneNo ?>
							</td>
							
							<td>
								<?php echo $value->prospect ?>
							</td>
							<td>
								<?php echo $value->source ?>
							</td>
							<td>
								<?php echo $value->travelDate ?>
							</td>
							<td>
								<?php echo $value->nextFollowupDate ?>
							</td><td>
								<?php echo $value->status ?>
							</td>
							
							<td class="text-center">
								 
								<?php if($value->status != 'Closed') { ?>
									<a class="btn btn-default dropdown-toggle waves-effect waves-light" href="<?php echo base_url().'addPackage/'.$value->id; ?>"><span class="m-l-5"><i class="md md-add-circle"></i></span> Add Package </a>
									<a class="btn btn-default dropdown-toggle waves-effect waves-light" href="#"><span class="m-l-5"><i class="md md-add-circle"></i></span> Booking </a>
								<?php } ?>
								<a class="btn btn-sm btn-success" href="<?php echo base_url().'editInquiry/'.$value->id; ?>" title="Edit">
									<i class="fa fa-pencil"></i>
								</a>
							</td>
						</tr>
                    <?php }
                } ?>
			</tbody>
		</table>
	</div>
</div>
</div>
<?php $this->load->view('includes/footer', ''); ?>
<script type="text/javascript">

jQuery(document).ready(function($) {
		$('.counter').counterUp({
			delay: 100,
			time: 1200
		});
		$(".knob").knob();
		
		$('form').parsley();
		
		$('.inquiry_table').DataTable({
			"lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]]
		});
		 var table = $('.inquiry_table').DataTable();

		$('#fl_st_date').datepicker({orientation: "bottom auto"});
		$('#fl_end_date1').datepicker({orientation: "bottom auto"});
		
		 $('#search').click( function() {
			table.draw();
		} );
		
	});
	
	$.fn.dataTable.ext.search.push(
		function( settings, data, dataIndex ) {
			var start_dt = new Date($('#fl_st_date').val()).getTime();
			var end_dt = new Date($('#fl_end_date').val()).getTime();
			var pros = $('#prospect').val();
			var sour = $('#source').val();
			var age = new Date(data[7]).getTime() || 0; // use data for the age column
			
			var prospect = data[4] || 0; // use data for the age column
			var source = data[5] || 0; // use data for the age column
			/*if(prospect != '' && prospect == pros){
				return true;
			}*/
			
			if( (pros != '' && pros == prospect) && (sour != '' && sour == source) && ( ( isNaN( start_dt ) && age <= end_dt ) || ( start_dt <= age   && isNaN( end_dt ) ) || ( start_dt <= age   && age <= end_dt )) ){
				return true;
			}else if( (pros != '' && pros == prospect) && (sour == '') && ( ( isNaN( start_dt ) && age <= end_dt ) || ( start_dt <= age   && isNaN( end_dt ) ) || ( start_dt <= age   && age <= end_dt )) ){
				return true;
			}else if( (sour != '' && sour == source) && (pros == '') && ( ( isNaN( start_dt ) && age <= end_dt ) || ( start_dt <= age   && isNaN( end_dt ) ) || ( start_dt <= age   && age <= end_dt )) ){
				return true;
			}else if( (sour != '' && sour == source) && (pros != '' && pros == prospect) && (isNaN( start_dt ) && isNaN( end_dt )) ){
				return true;
			}else if((pros != '' && pros == prospect) && ( isNaN( start_dt ) && isNaN( end_dt )) && (sour == '') ){
				return true;
			}else if((sour != '' && sour == source) && ( isNaN( start_dt ) && isNaN( end_dt )) && (pros == '') ){
				return true;
			}else if( (pros == '') && (( isNaN( start_dt ) && age <= end_dt ) || ( start_dt <= age   && isNaN( end_dt ) ) || ( start_dt <= age   && age <= end_dt ))){
				return true;
			}else if( (pros == '') && (( isNaN( start_dt ) && age <= end_dt ) || ( start_dt <= age   && isNaN( end_dt ) ) || ( start_dt <= age   && age <= end_dt ))){
				return true;
			}else if( (pros == '') && (sour == '')&& (( isNaN( start_dt ) && isNaN( end_dt ) ) || ( isNaN( start_dt ) && age <= end_dt ) || ( start_dt <= age   && isNaN( end_dt ) ) || ( start_dt <= age   && age <= end_dt ) ))
			{ return true;	
			}else if( (sour == '') && ( pros == '') ){
				return true;
			}
			
			return false;
		}
	);	
	
		
		
</script>