<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"> 
<link href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.css" rel="stylesheet"> 

 <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li>
                                <h4 class="page-title">Dashboard</h4>
                            </li>
							<li>
                                <a href="<?php echo base_url(); ?>inquiryListing">Inquiry</a>
                            </li>
                            
                            <li class="active">
                               <?php echo isset($Inquiry[0]->id) ? 'Edit' : 'Add' ?>
                            </li>
                        </ol>
                    </div>
                </div>

				
                <div class="row">
				<?php if(isset($Inquiry[0]->id) && $Inquiry[0]->id > 0){ ?>
					
					<ul class="nav nav-tabs tabs">
						<li class="tab active">
							<a href="#messages-2" data-toggle="tab" aria-expanded="true" class="active">
								<span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
								<span class="hidden-xs">Update Inquiry</span>
							</a>
						</li>
						<!--<li class="tab">
							<a href="#settings-2" data-toggle="tab" aria-expanded="false">
								<span class="visible-xs"><i class="fa fa-cog"></i></span>
								<span class="hidden-xs">Odit Log</span>
							</a>
						</li>-->
                    </ul>
					<div class="tab-content">
                        <div class="tab-pane active" id="messages-2">
						
							<form action="<?php echo base_url() ?><?php echo isset($Inquiry[0]->id) ? 'Inquiry/updateInquiry' : 'Inquiry/insertInquiry' ?>" name="insertInquiry" id="insertInquiry" method="post" class="form-horizontal" data-parsley-validate novalidate autocomplete="off">
								<div class="row  card-box">
									
											<div class="col-md-8">
												<h5 class="text-muted text-uppercase m-t-0 m-b-20"><b>General</b></h5>
													<div class="row">
														<div class="col-md-12">
															<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
														</div>
													</div>
											<div class="form-group m-b-20">
												<label class="col-md-2 control-label">First Name :<span class="text-danger">*</span></label>
												<div class="col-md-8"> 
													<input type="text" id="firstName" name="firstName" class="form-control" 
													value="<?php echo isset($Inquiry[0]->firstName) ? set_value("firstName", $Inquiry[0]->firstName) : set_value("firstName"); ?>"  placeholder="Enter First Name" required>
													<?php if(isset($Inquiry[0]->id)) { ?>
														<input type="hidden" name="id" id="id" value="<?php echo $Inquiry[0]->id; ?>" >
													<?php } ?>
												</div>
											</div>
											
											<div class="form-group m-b-20">
												<label class="col-md-2 control-label">Last Name :</label>
												<div class="col-md-8"> 
													<input type="text" id="lastName" name="lastName" class="form-control" 
													value="<?php echo isset($Inquiry[0]->lastName) ? set_value("lastName", $Inquiry[0]->lastName) : set_value("lastName"); ?>"  placeholder="Enter Last Name" parsley-trigger="change">
												</div>
											</div>
											
											<div class="form-group m-b-20">
												<label class="col-md-2 control-label">Phone Number :<span class="text-danger">*</span></label>
												<div class="col-md-8"> 
													<input type="text" id="phoneNo" name="phoneNo" maxlength="15" class="form-control" 
													value="<?php echo isset($Inquiry[0]->phoneNo) ? set_value("phoneNo", $Inquiry[0]->phoneNo) : set_value("phoneNo"); ?>"  placeholder="Enter Phone Number" parsley-trigger="change" required>
												</div>
											</div>
											
											<div class="form-group m-b-20">
												<label class="col-md-2 control-label">Alternet Phone Number :</label>
												<div class="col-md-8"> 
													<input type="text" id="altPhoneNo" name="altPhoneNo" maxlength="15" class="form-control" 
													value="<?php echo isset($Inquiry[0]->altPhoneNo) ? set_value("altPhoneNo", $Inquiry[0]->altPhoneNo) : set_value("altPhoneNo"); ?>"  placeholder="Enter Alternet Phone Number" parsley-trigger="change">
												</div>
											</div>
												
											<div class="form-group m-b-20">
												 <label class="col-md-2 control-label">Email Id :</label>
												<div class="col-md-8">
													<input type="email" id="email" name="email" maxlength="128" class="form-control" 
													value="<?php echo isset($Inquiry[0]->email) ? set_value("email", $Inquiry[0]->email) : set_value("email"); ?>"  placeholder="Enter Your Email" parsley-trigger="change">
												</div>	
											</div>
											<div class="form-group m-b-20">
												 <label class="col-md-2 control-label">Destination :<span class="text-danger">*</span></label>
												<div class="col-md-8">
													<input type="text" name="destination" id="destination" class="form-control"  value="<?php echo isset($Inquiry[0]->destination) ? set_value("destination", $Inquiry[0]->destination) : set_value("destination"); ?>" required/>
												</div>	
											</div>
											
											
											<div class="form-group m-b-20">
												 <label class="col-md-2 control-label">Alternet Email Id :</label>
												<div class="col-md-8">
													<input type="altEmail" id="altEmail" name="altEmail" maxlength="128" class="form-control" 
													value="<?php echo isset($Inquiry[0]->altEmail) ? set_value("altEmail", $Inquiry[0]->altEmail) : set_value("altEmail"); ?>"  placeholder="Enter Alternet Email" parsley-trigger="change" >
												</div>	
											</div>
											
																				
											<div class="form-group m-b-20">
												 <label class="col-md-2 control-label">No Off Adults :<span class="text-danger">*</span></label>
												<div class="col-md-8">
													<input type="numer" id="noOffAdults" name="noOffAdults" maxlength="3" class="form-control" 
													value="<?php echo isset($Inquiry[0]->noOffAdults) ? set_value("noOffAdults", $Inquiry[0]->noOffAdults) : set_value("noOffAdults"); ?>"  placeholder="Enter No Off Adults" parsley-trigger="change" required>
												</div>	
											</div>
											<div class="form-group m-b-20">
												 <label class="col-md-2 control-label">Child With Extra Bed :</label>
												<div class="col-md-8">
													<input type="numer" id="childWithExtraBed" name="childWithExtraBed" maxlength="3" class="form-control" 
													value="<?php echo isset($Inquiry[0]->childWithExtraBed) ? set_value("childWithExtraBed", $Inquiry[0]->childWithExtraBed) : set_value("childWithExtraBed"); ?>"  placeholder="Enter No Off Child With Extra bed" parsley-trigger="change" >
												</div>	
											</div>
											
											<div class="form-group m-b-20">
												 <label class="col-md-2 control-label">Child Without Extra Bed :</label>
												<div class="col-md-8">
													<input type="numer" id="childWithoutExtraBed" name="childWithoutExtraBed" maxlength="3" class="form-control" 
													value="<?php echo isset($Inquiry[0]->childWithoutExtraBed) ? set_value("childWithoutExtraBed", $Inquiry[0]->childWithoutExtraBed) : set_value("childWithoutExtraBed"); ?>"  placeholder="Enter No Off Child Without Extra bed" parsley-trigger="change">
												</div>	
											</div>
											<div class="form-group m-b-20">
												 <label class="col-md-2 control-label">Prospect : <span class="text-danger">*</span></label>
												<div class="col-md-8">
													<select name="prospect" id="prospect" class="form-control" required>
														<option value="">Select Prospect</option>
														<option value="Cold" <?php if (isset($Inquiry[0]->prospect) && $Inquiry[0]->prospect == 'Cold') echo 'selected' ; ?>>Cold</option>
														<option value="Hot" <?php if (isset($Inquiry[0]->prospect) && $Inquiry[0]->prospect == 'Hot') echo 'selected' ; ?>>Hot</option>
														<option value="Warm" <?php if (isset($Inquiry[0]->prospect) && $Inquiry[0]->prospect == 'Warm') echo 'selected' ; ?>>Warm</option>
													</select>
												</div>	
											</div>
											
																			
											<div class="form-group m-b-20">
												 <label class="col-md-2 control-label">Source : <span class="text-danger">*</span></label>
												<div class="col-md-8">
													<select name="source" id="source" class="form-control" required>
														<option value="">Select Source</option>
														<option value="HelloTravel" <?php if (isset($Inquiry[0]->source) && $Inquiry[0]->source == 'HelloTravel') echo 'selected' ; ?>>HelloTravel</option>
														<option value="Web" <?php if (isset($Inquiry[0]->source) && $Inquiry[0]->source == 'Web') echo 'selected' ; ?>>Web</option>
														<option value="Reference" <?php if (isset($Inquiry[0]->source) && $Inquiry[0]->source == 'Reference') echo 'selected' ; ?>>Reference</option>
														<option value="HolidayIq" <?php if (isset($Inquiry[0]->source) && $Inquiry[0]->source == 'HolidayIq') echo 'selected' ; ?>>Holiday IQ</option>
														<option value="Phone" <?php if (isset($Inquiry[0]->source) && $Inquiry[0]->source == 'Phone') echo 'selected' ; ?>>Phone</option>
														<option value="JustDial" <?php if (isset($Inquiry[0]->source) && $Inquiry[0]->source == 'JustDial') echo 'selected' ; ?>>JustDial</option>
														<option value="Data Diary" <?php if (isset($Inquiry[0]->source) && $Inquiry[0]->source == 'Data Diary') echo 'selected' ; ?>>Data Diary</option>
														<option value="Sulekha" <?php if (isset($Inquiry[0]->source) && $Inquiry[0]->source == 'Sulekha') echo 'selected' ; ?>>Sulekha</option>
													</select>
												</div>	
											</div>
											<div class="form-group m-b-20">
												 <label class="col-md-2 control-label">Travel Date : <span class="text-danger">*</span></label>
												<div class="col-md-8">
													<div class='input-group date' id='datetimepicker1'>
														<input type='text' name="travelDate" id="travelDate" class="form-control" value="<?php if(isset($Inquiry[0]->travelDate)){
															echo $Inquiry[0]->travelDate;
														} ?>" placeholder="Select Travel Date"/>
															<span class="input-group-addon">
																<span class="glyphicon glyphicon-calendar"></span>
															</span>
													</div>
												</div>	
											</div>

											<div class="form-group m-b-20">
												 <label class="col-md-2 control-label">Next Followup Date : <span class="text-danger">*</span></label>
												<div class="col-md-8">
													<div class='input-group date' id='datetimepicker2'>
														<input type='text' name="nextFollowupDate" id="nextFollowupDate" class="form-control" value="<?php if(isset($Inquiry[0]->nextFollowupDate)){ echo $Inquiry[0]->nextFollowupDate; }  ?>" placeholder="Select Next Followup Date"/>
															<span class="input-group-addon">
																<span class="glyphicon glyphicon-calendar"></span>
															</span>
													</div>
												</div>	
											</div>
											<div class="form-group m-b-20">
												 <label class="col-md-2 control-label">Status : <span class="text-danger">*</span></label>
												<div class="col-md-8">
													<select name="status" value="status" id="status" class="form-control" required >
														<option value="">Select Status</option>
														<option value="Untouched" <?php if (isset($Inquiry[0]->status) && $Inquiry[0]->status == 'Untouched') echo 'selected' ; ?> >Untouched</option>
														<option value="Open" <?php if (isset($Inquiry[0]->status) && $Inquiry[0]->status == 'Open') echo 'selected' ; ?>>Open</option>
														<option value="Closed" <?php if (isset($Inquiry[0]->status) && $Inquiry[0]->status == 'Closed') echo 'selected' ; ?>>Closed</option>
														<option value="Booked" <?php if (isset($Inquiry[0]->status) && $Inquiry[0]->status == 'Booked') echo 'selected' ; ?>>Booked</option>
													</select>
												</div>	
											</div>
											
											<div class="form-group m-b-20" id="not_buy_reson" <?php if (isset($Inquiry[0]->status) && $Inquiry[0]->status == 'Closed') { ?> style="display:block" <?php } else { ?> style="display:none;" <?php } ?>>
												 <label class="col-md-2 control-label">Not Buying Reason : <span class="text-danger">*</span></label>
												<div class="col-md-8">
												<select name="nbr" id="nbr" class="form-control">
													<option value="">Select NBR</option>
													<?php if(isset($getnbr)){ ?>
														<?php foreach($getnbr as $nbr){ ?>
															<option value="<?php echo $nbr->id; ?>" 
															<?php if(isset($Inquiry[0]->nbr) && $Inquiry[0]->nbr == $nbr->id){ ?> selected <?php } ?>> <?php echo $nbr->not_buy_resion; ?></option>
														 <?php } ?>
													<?php } ?>
													</select>
												</div>	
											</div>
											
											
											

											<div class="form-group m-b-20">
												 <label class="col-md-2 control-label">Notes :</label>
												<div class="col-md-8">
													<textarea name="notes" id="notes" cols="76" rows="5"></textarea>
												</div>	
											</div>
											
															
												<hr />
											<div class="form-group p-20">
												<label class="col-md-1 control-label"></label>
												
												<div class="col-md-11">
												 <button type="submit" class="btn w-sm btn-default waves-effect waves-light">Save</button>
												 <button type="button" class="btn w-sm btn-white waves-effect"><a href="<?php echo base_url(); ?>inquiryListing">Cancel</a></button>
												</div> 
											</div>
											</div>
											
											<div class="col-md-4">
											<?php if(isset($InquiryNote) && !empty($InquiryNote)){ ?>
											<h5 class="text-muted text-uppercase m-t-0 m-b-20"><b>Odit Log</b></h5>
												
											<div class="form-group m-b-20">
												<div class="col-md-12">
												 <ul class="list-group">
													<?php	
													for($i = 0; $i < count($InquiryNote); $i++){
													if(!empty($InquiryNote[$i]->inquiryNote)){?>
													<li class="list-group-item">
														<?php if($this->session->userdata['roleText'] == 'Admin'){
															echo "<strong>Added By :</strong>" .$InquiryNote[$i]->name."<br />"; 
														}
														?>
														<?php
														echo "<strong>Note :</strong>" . $InquiryNote[$i]->inquiryNote . "<br /><strong>At:</strong>".$InquiryNote[$i]->createdAt ?>
														</li>
												  	<?php } } ?>	
												</ul>	
													
												</div>
											</div>
											<?php } ?>
											</div>	
									
								</div>
							</form>
						</div>
					
						<!--ODIT LOG-->
						<div class="tab-pane" id="settings-2">
							<div class="row">
								<div class="col-sm-12">
									<div class="card-box">
										<table id="datatable-responsive"
											   class="table table-striped table-bordered dt-responsive nowrap inquiry_table" cellspacing="0"
											   width="100%">
											<thead>
												<tr>
													<th>Action person Name</th>
													<th>Activity</th>
													<th>Time</th>
													
												</tr>
											</thead>
											<tbody>
												<?php
												if(!empty($oditlog)){
												foreach ($oditlog as $key => $values) {
													echo "<tr>";
													echo "<td>".$values->userName."</td>";
													echo "<td>".$values->process."</td>";
													echo "<td>".$values->createdDtm."</td>";
													echo "</tr>";
												}
											}
												?>
											</tbody>
										</table>
										
									</div>   
								</div>
							</div>            
						</div>
					
					</div>	
					
				<?php } else{ ?>
					
					<form action="<?php echo base_url() ?><?php echo isset($Inquiry[0]->id) ? 'Inquiry/updateInquiry' : 'Inquiry/insertInquiry' ?>" name="insertInquiry" id="insertInquiry" method="post" class="form-horizontal" data-parsley-validate novalidate autocomplete="off">
                        <div class="row">
                            <div class="col-sm-12">
							
                                <div class="card-box">
                                    
										<h5 class="text-muted text-uppercase m-t-0 m-b-20"><b>General</b></h5>
										
											<div class="row">
												<div class="col-md-12">
													<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
												</div>
											</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">First Name :<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="firstName" name="firstName" class="form-control" 
											value="<?php echo isset($Inquiry[0]->firstName) ? set_value("firstName", $Inquiry[0]->firstName) : set_value("firstName"); ?>"  placeholder="Enter First Name" required>
											<?php if(isset($Inquiry[0]->id)) { ?>
												<input type="hidden" name="id" id="id" value="<?php echo $Inquiry[0]->id; ?>" >
											<?php } ?>
										</div>
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Last Name :</label>
										<div class="col-md-6"> 
											<input type="text" id="lastName" name="lastName" class="form-control" 
											value="<?php echo isset($Inquiry[0]->lastName) ? set_value("lastName", $Inquiry[0]->lastName) : set_value("lastName"); ?>"  placeholder="Enter Last Name" parsley-trigger="change">
										</div>
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Phone Number :<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="phoneNo" name="phoneNo" maxlength="15" class="form-control" 
											value="<?php echo isset($Inquiry[0]->phoneNo) ? set_value("phoneNo", $Inquiry[0]->phoneNo) : set_value("phoneNo"); ?>"  placeholder="Enter Phone Number" parsley-trigger="change" required>
										</div>
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Alternet Phone Number :</label>
										<div class="col-md-6"> 
											<input type="text" id="altPhoneNo" name="altPhoneNo" maxlength="15" class="form-control" 
											value="<?php echo isset($Inquiry[0]->altPhoneNo) ? set_value("altPhoneNo", $Inquiry[0]->altPhoneNo) : set_value("altPhoneNo"); ?>"  placeholder="Enter Alternet Phone Number" parsley-trigger="change">
										</div>
									</div>
										
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Email Id :</label>
										<div class="col-md-6">
											<input type="email" id="email" name="email" maxlength="128" class="form-control" 
											value="<?php echo isset($Inquiry[0]->email) ? set_value("email", $Inquiry[0]->email) : set_value("email"); ?>"  placeholder="Enter Your Email" parsley-trigger="change">
										</div>	
									</div>
									
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Alternet Email Id :</label>
										<div class="col-md-6">
											<input type="altEmail" id="altEmail" name="altEmail" maxlength="128" class="form-control" 
											value="<?php echo isset($Inquiry[0]->altEmail) ? set_value("altEmail", $Inquiry[0]->altEmail) : set_value("altEmail"); ?>"  placeholder="Enter Alternet Email" parsley-trigger="change" >
										</div>	
									</div>
									
																		
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">No Off Adults :<span class="text-danger">*</span></label>
										<div class="col-md-6">
											<input type="numer" id="noOffAdults" name="noOffAdults" maxlength="3" class="form-control" 
											value="<?php echo isset($Inquiry[0]->noOffAdults) ? set_value("noOffAdults", $Inquiry[0]->noOffAdults) : set_value("noOffAdults"); ?>"  placeholder="Enter No Off Adults" parsley-trigger="change" required>
										</div>	
									</div>
									
								
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Child With Extra Bed :<span class="text-danger">*</span></label>
										<div class="col-md-6">
											<input type="numer" id="childWithExtraBed" name="childWithExtraBed" maxlength="3" class="form-control" 
											value="<?php echo isset($Inquiry[0]->childWithExtraBed) ? set_value("childWithExtraBed", $Inquiry[0]->childWithExtraBed) : set_value("childWithExtraBed"); ?>"  placeholder="Enter No Off Child With Extra bed" parsley-trigger="change" required>
										</div>	
									</div>
									
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Child Without Extra Bed :<span class="text-danger">*</span></label>
										<div class="col-md-6">
											<input type="numer" id="childWithoutExtraBed" name="childWithoutExtraBed" maxlength="3" class="form-control" 
											value="<?php echo isset($Inquiry[0]->childWithoutExtraBed) ? set_value("childWithoutExtraBed", $Inquiry[0]->childWithoutExtraBed) : set_value("childWithoutExtraBed"); ?>"  placeholder="Enter No Off Child Without Extra bed" parsley-trigger="change" required>
										</div>	
									</div>
									
									
									
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Prospect : <span class="text-danger">*</span></label>
										<div class="col-md-6">
											<select name="prospect" value="prospect" class="form-control" required>
												<option value="">Select Prospect</option>
												<option value="Cold" <?php if (isset($Inquiry[0]->prospect) && $Inquiry[0]->prospect == 'Cold') echo 'selected' ; ?>>Cold</option>
												<option value="Hot" <?php if (isset($Inquiry[0]->prospect) && $Inquiry[0]->prospect == 'Hot') echo 'selected' ; ?>>Hot</option>
												<option value="Warm" <?php if (isset($Inquiry[0]->prospect) && $Inquiry[0]->prospect == 'Warm') echo 'selected' ; ?>>Warm</option>
                                            </select>
										</div>	
									</div>
									
																	
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Source : <span class="text-danger">*</span></label>
										<div class="col-md-6">
											<select name="source" value="source" class="form-control" required>
												<option value="">Select Source</option>
												<option value="HelloTravel" <?php if (isset($Inquiry[0]->source) && $Inquiry[0]->source == 'HelloTravel') echo 'selected' ; ?>>HelloTravel</option>
												<option value="Web" <?php if (isset($Inquiry[0]->source) && $Inquiry[0]->source == 'Web') echo 'selected' ; ?>>Web</option>
												<option value="Reference" <?php if (isset($Inquiry[0]->source) && $Inquiry[0]->source == 'Reference') echo 'selected' ; ?>>Reference</option>
												<option value="HolidayIq" <?php if (isset($Inquiry[0]->source) && $Inquiry[0]->source == 'HolidayIq') echo 'selected' ; ?>>Holiday IQ</option>
												<option value="Phone" <?php if (isset($Inquiry[0]->source) && $Inquiry[0]->source == 'Phone') echo 'selected' ; ?>>Phone</option>
												<option value="JustDial" <?php if (isset($Inquiry[0]->source) && $Inquiry[0]->source == 'JustDial') echo 'selected' ; ?>>JustDial</option>
												<option value="Data Diary" <?php if (isset($Inquiry[0]->source) && $Inquiry[0]->source == 'Data Diary') echo 'selected' ; ?>>Data Diary</option>
												<option value="Sulekha" <?php if (isset($Inquiry[0]->source) && $Inquiry[0]->source == 'Sulekha') echo 'selected' ; ?>>Sulekha</option>
                                            </select>
										</div>	
									</div>
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Travel Date : <span class="text-danger">*</span></label>
										<div class="col-md-6">
											<div class='input-group date' id='datetimepicker1'>
												<input type='text' name="travelDate" id="travelDate" class="form-control" value="<?php if(isset($Inquiry[0]->travelDate)){
													echo $Inquiry[0]->travelDate;
												} ?>" placeholder="Select Travel Date"/>
													<span class="input-group-addon">
														<span class="glyphicon glyphicon-calendar"></span>
													</span>
											</div>
										</div>	
									</div>

									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Next Followup Date : <span class="text-danger">*</span></label>
										<div class="col-md-6">
											<div class='input-group date' id='datetimepicker2'>
												<input type='text' name="nextFollowupDate" id="nextFollowupDate" class="form-control" value="<?php if(isset($Inquiry[0]->nextFollowupDate)){ echo $Inquiry[0]->nextFollowupDate; }  ?>" placeholder="Select Next Followup Date"/>
													<span class="input-group-addon">
														<span class="glyphicon glyphicon-calendar"></span>
													</span>
											</div>
										</div>	
									</div>
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Status : <span class="text-danger">*</span></label>
										<div class="col-md-6">
											<select name="status" value="status" id="status" class="form-control" required >
												<option value="">Select Status</option>
												<option value="Untouched" <?php if (isset($Inquiry[0]->status) && $Inquiry[0]->status == 'Untouched') echo 'selected' ; ?> >Untouched</option>
												<option value="Open" <?php if (isset($Inquiry[0]->status) && $Inquiry[0]->status == 'Open') echo 'selected' ; ?>>Open</option>
												<option value="Closed" <?php if (isset($Inquiry[0]->status) && $Inquiry[0]->status == 'Closed') echo 'selected' ; ?>>Closed</option>
												<option value="Booked" <?php if (isset($Inquiry[0]->status) && $Inquiry[0]->status == 'Booked') echo 'selected' ; ?>>Booked</option>
                                            </select>
										</div>	
									</div>
									
									
									<div class="form-group m-b-20" id="not_buy_reson" <?php if (isset($Inquiry[0]->status) && $Inquiry[0]->status == 'Closed') { ?> style="display:block" <?php } else { ?> style="display:none;" <?php } ?>>
										 <label class="col-md-2 control-label">Not Buying Reason : <span class="text-danger">*</span></label>
										<div class="col-md-6">
										<select name="nbr" id="nbr" class="form-control">
											<option value="">Select NBR</option>
											<?php if(isset($getnbr)){ ?>
												<?php foreach($getnbr as $nbr){ ?>
													<option value="<?php echo $nbr->id; ?>" 
													<?php if(isset($Inquiry[0]->nbr) && $Inquiry[0]->nbr == $nbr->id){ ?> selected <?php } ?>> <?php echo $nbr->not_buy_resion; ?></option>
												 <?php } ?>
											<?php } ?>
											</select>
										</div>	
									</div>
									
									
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Destination :<span class="text-danger">*</span></label>
										<div class="col-md-6">
											<input type="text" name="destination" id="destination" class="form-control"  value="<?php echo isset($Inquiry[0]->destination) ? set_value("destination", $Inquiry[0]->destination) : set_value("destination"); ?>" required/>
										</div>	
									</div>
									

									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Notes :</label>
										<div class="col-md-6">
											<textarea name="notes" id="notes" cols="74" rows="5"><?php echo isset($Inquiry[0]->notes) ? set_value("notes", $Inquiry[0]->notes) : set_value("notes"); ?></textarea>
										</div>	
									</div>

										<hr />
									<div class="form-group p-20">
										<label class="col-md-1 control-label"></label>
										
										<div class="col-md-11">
										 <button type="submit" class="btn w-sm btn-default waves-effect waves-light">Save</button>
										 <button type="button" class="btn w-sm btn-white waves-effect"><a href="<?php echo base_url(); ?>inquiryListing">Cancel</a></button>
										</div> 
									</div>
                                </div>
							</div>
						</div>
					</form>
					
				<?php } ?>
                    
                </div>
            </div>

 <?php $this->load->view('includes/footer', ''); ?>
<script type="text/javascript">

jQuery(document).ready(function($) {
	$('.counter').counterUp({
		delay: 100,
		time: 1200
	});
	$(".knob").knob();
	$('form').parsley();
	
	$('#datetimepicker1').datepicker();
	$('#datetimepicker2').datepicker();
		
});
	
			
/* onchange evento for inquiry status */
$("#status").change(function () {
	var selectedText = $(this).find("option:selected").text();
	var selectedValue = $(this).val();
	if(selectedText != '' && selectedValue == 'Closed'){
		if( confirm('Are you sure to Closed this Inquiry') == true )
		{
			document.getElementById('not_buy_reson').style.display = "block";
		}else{
			return false;
		}
	}else if(selectedText != '' && selectedValue != 'Closed'){
		document.getElementById('not_buy_reson').style.display = "none";
	}
});
</script>				