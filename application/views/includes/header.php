<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon_1.ico">

        <title><?php echo $pageTitle; ?></title>

        <!--Morris Chart CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/morris/morris.css">

        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?php echo base_url(); ?>assets/js/modernizr.min.js"></script>

    </head>


    <body>


        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container">

                    <!-- Logo container-->
                    <div class="logo">
                        <a href="<?php echo base_url(); ?>" class="logo"><span><?php echo SITE_NAME; ?></span></a>
                    </div>
                    <!-- End Logo container-->


                    <div class="menu-extras">
						
                        <ul class="nav navbar-nav navbar-right pull-right">
                            <li class="dropdown navbar-c-items">
                                <a href="" class="dropdown-toggle waves-effect waves-light profile" data-toggle="dropdown" aria-expanded="true"><img src="<?php echo base_url(); ?>assets/images/users/avatar-1.jpg" alt="user-img" class="img-circle"> </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo base_url(); ?>userEdit"><i class="ti-settings text-custom m-r-10"></i> Account Settings</a></li>
                                    <li class="divider"></li>
									<?php  if($this->session->userdata('roleText') != 'null' && $this->session->userdata('roleText') == 'Admin'){ ?>
										<li><a href="<?php echo base_url(); ?>employeeListing"><i class="md md-account-child m-r-10"></i> Manage Employee</a></li>
										  <li class="divider"></li>
									<?php } ?>
									
                                  
                                    <li><a href="<?php echo base_url(); ?>logout"><i class="ti-power-off text-danger m-r-10"></i> Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                        <div class="menu-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </div>
                    </div>

                </div>
            </div>

            <div class="navbar-custom">
                <div class="container">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">
                            <li class="has-submenu">
                                <a href="<?php echo base_url(); ?>dashboard"><i class="fa fa-tachometer"></i>Dashboard</a>
                                
                            </li>
                            <li class="has-submenu">
                                <a href="<?php echo base_url(); ?>inquiryListing"><i class="fa fa-info-circle"></i>Inquiry</a>
                            </li>
							<?php  if($this->session->userdata('roleText') != 'null' && $this->session->userdata('roleText') != 'Seller'){ ?>
								<li class="has-submenu">
									<a href="<?php echo base_url(); ?>hotelListing"><i class="md md-local-hotel"></i>Hotel</a>
								</li>
                                <li class="has-submenu">
                                    <a href="<?php echo base_url(); ?>nightstayplanListing"><i class="md md-local-hotel"></i>Night Stay Plan</a>
                                </li>
                                <li class="has-submenu">
                                    <a href="<?php echo base_url(); ?>noofnightListing"><i class="md md-local-hotel"></i>Add Night</a>
                                </li>
							<?php } ?>
							<li class="has-submenu">
                                <a href="<?php echo base_url(); ?>packageListing"><i class="md md-layers "></i>Package</a>
                            </li>
							<li class="has-submenu">
                                <a href="<?php echo base_url(); ?>bookingListing"><i class="md md-class"></i>Booking</a>
                            </li>
                            <li class="has-submenu">
                                <a href="<?php echo base_url(); ?>vehicleListing"><i class="md md-class"></i>Add Vehicle</a>
                            </li>

                        </ul>
                        <!-- End navigation menu        -->
                    </div>
                </div> <!-- end container -->
            </div> <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->
		 <div class="wrapper">
            <div class="container">