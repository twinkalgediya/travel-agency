
 <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li>
                                <h4 class="page-title">Dashboard</h4>
                            </li>
							<li>
                                <a href="<?php echo base_url(); ?>nightstayplanListing">Night Stay Plan</a>
                            </li>
                            
                            <li class="active">
                               <?php echo isset($night[0]->id) ? 'Edit' : 'Add' ?>

                            </li>
                        </ol>
                    </div>
                </div>

				
                <div class="row">
                    <div class="col-sm-12">
					<label class="col-md-2 control-label"></div>
									
					<form action="<?php echo base_url() ?><?php echo isset($night[0]->id) ? 'Nightstayplan/updateNightStayPlan' : 'Nightstayplan/insertNightStayPlan' ?>" name="insertNightStayPlan" id="insertNightStayPlan" method="post" enctype="multipart/form-data" class="form-horizontal" data-parsley-validate novalidate>
                        <div class="row">
                            <div class="col-sm-12">
							
                                <div class="card-box">
                                    
										<h5 class="text-muted text-uppercase m-t-0 m-b-20"><b>General</b></h5>
																															
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Type Of Place:<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="radio" class="dom" name="typeof_place" value="domestic" <?php  if(isset($night[0]->typeof_place) && $night[0]->typeof_place == "domestic") echo "checked"?>> Domestic</input>
											<input type="radio" class="inter" name="typeof_place" value="international" <?php if(isset($night[0]->typeof_place) && $night[0]->typeof_place == "international") echo "checked"?> > International</input>
											<?php if(isset($night[0]->id)) { ?>
												<input type="hidden" name="id" id="id" value="<?php echo $night[0]->id; ?>" >
											<?php } ?>
											<?php if( form_error('typeof_place') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('typeof_place'); ?></li></ul>
											<?php } ?>
										</div>
									</div>
								<div class="domestic"  style='display:none'>
									<div class="form-group m-b-20">
									<label class="col-md-2 control-label">Countries :<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
									<select name="country_id" class="form-control" id="country_id">
								      <option value="">Select Country</option>
								       <?php foreach($countries as $count): ?>
								        <option value="<?php echo $count->id; ?>" <?php if (isset($night[0]->country_id) && $night[0]->country_id == $count->id) echo 'selected' ; ?>><?php echo $count->name; ?></option>
								         <?php endforeach; ?> 
								      </select>
								      <?php if( form_error('country_id') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('country_id'); ?></li></ul>
											<?php } ?>
								      	</div>
									</div>	
								</div>

								<div class="international"  style='display:none'>
									<div class="form-group m-b-20">
									<label class="col-md-2 control-label">State :<span class="text-danger">*</span></label>
										<div class="col-md-6">
									<select id="state_id" name="state_id" class="form-control">
									    <option value="">Select country first</option>
									   <?php if(isset($state) && !empty($state)) { ?>
									    	 <?php foreach($state as $count): ?>
								        <option value="<?php echo $count->id; ?>" <?php if (isset($night[0]->state_id) && $night[0]->state_id == $count->id) echo 'selected' ; ?>><?php echo $count->name; ?></option>
								         <?php endforeach; ?> 
								          <?php } ?>
									    
									</select>
									  <?php if( form_error('state_id') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('state_id'); ?></li></ul>
											<?php } ?>
										</div>
									</div>
								</div>
								
								<div class="form-group m-b-20">
									<label class="col-md-2 control-label">No Of Night :<span class="text-danger">*</span></label>
									<div class="col-md-6"> 
										<select name="no_of_nights" class="form-control" id="no_of_nights">
									      <option value="">Select No Of Night </option>
									      
									       <?php foreach($noofnight as $count): ?>
									    		<option value="<?php echo $count->id; ?>" <?php if (isset($night[0]->no_of_nights) && $night[0]->no_of_nights == $count->id) echo 'selected' ; ?>><?php echo $count->no_of_nights; ?></option>
									        <?php endforeach; ?> 
								      	</select>

								    </div>
								</div>	
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Night Stay Plan:</label>
										<div class="col-md-6">
											<input type="text" id="night_stay" name="night_stay" maxlength="128" class="form-control <?php if( form_error('night_stay') != '') { ?> parsley-error" <?php } ?>" 
											value="<?php echo isset($night[0]->night_stay) ? set_value("night_stay", $night[0]->night_stay) : set_value("night_stay"); ?>"  placeholder="Enter Night Stay Plan" parsley-trigger="change">
											
											<?php if( form_error('night_stay') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('night_stay'); ?></li></ul>
											<?php } ?>
										</div>	
									</div>
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Itineraryitinerary:<span class="text-danger">*</span></label>
										<div class="col-md-6">
<textarea name="itinerary" id="itinerary" required parsley-trigger="change" cols="88" rows="5"><?php echo isset($night[0]->itinerary)?set_value("itinerary", $night[0]->itinerary):set_value("itinerary"); ?>
											</textarea>
											<?php if( form_error('itinerary') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('itinerary'); ?></li></ul>
											<?php } ?>
										</div>	
									</div>
									
										<hr />
									<div class="form-group p-20">
										<label class="col-md-1 control-label"></label>
										
										<div class="col-md-11">
										 <button type="submit" class="btn w-sm btn-default waves-effect waves-light">Save</button>
										 <button type="button" class="btn w-sm btn-white waves-effect"><a href="<?php echo base_url(); ?>hotelListing">Cancel</a></button>
										</div> 
									</div>
                                </div>
							</div>
						</div>
					</form>
                </div>
            </div>

<?php $this->load->view('includes/footer', ''); ?>
<script type="text/javascript">

jQuery(document).ready(function($) {
		$('.counter').counterUp({
			delay: 100,
			time: 1200
		});
		$(".knob").knob();
		
		$('form').parsley();

$(document).ready(function() {
    $('input[type="radio"]').change(function() {
    	
       if($(this).attr('class') == 'dom') {
       		$('.domestic').hide(); 
            $('.international').show();   
                     
       }
       else if($(this).attr('class') == 'inter') {
            $('.domestic').show(); 
            $('.international').hide();   
                     
       }
       else {
            $('.domestic').hide();
            $('.international').hide();      
       }
   }).filter(function(checkbox) {
            return $(this).prop("checked");
        })
        .trigger("change");    // or .change();
});

		$('#country_id').on('change',function(){
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('booking/getStates'); ?>',
                data:'country_id='+countryID,
                success:function(data){
                    $('#state_id').html('<option value="">Select State</option>'); 
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.id).text(this.name);           
                            $('#state_id').append(option);
                        });
                    }else{
                        $('#state_id').html('<option value="">State not available</option>');
                    }
                }
            }); 
        }else{
            $('#state_id').html('<option value="">Select country first</option>');
            
        }
    });
	
	$('#country_id').on('change',function(){
        var no_of_nightsID = $(this).val();
        if(no_of_nightsID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('Nightstayplan/getnightByCountry'); ?>',
                data:'no_of_nights_id='+no_of_nightsID,
                success:function(data){
                    $('#no_of_nights').html('<option value="">Select No Of Nignt</option>'); 
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
							//alert(this.no_of_nights);
                            var option = $('<option />');
                            option.attr('value', this.id).text(this.no_of_nights);           
                            $('#no_of_nights').append(option);
                        });
                    }else{
                        $('#no_of_nights').html('<option value="">Nignt not available</option>');
                    }
                }
            }); 
        }else{
            $('#no_of_nights').html('<option value="">Select country Or State first</option>');
            
        }
    });
	$('#state_id').on('change',function(){
        var no_of_nightsID = $(this).val();
        if(no_of_nightsID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('Nightstayplan/getnightByState'); ?>',
                data:'no_of_nights_id='+no_of_nightsID,
                success:function(data){
                    $('#no_of_nights').html('<option value="">Select No Of Nignt</option>'); 
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.id).text(this.no_of_nights);           
                            $('#no_of_nights').append(option);
                        });
                    }else{
                        $('#no_of_nights').html('<option value="">Nignt not available</option>');
                    }
                }
            }); 
        }else{
            $('#no_of_nights').html('<option value="">Select country Or State first</option>');
            
        }
    });
    
	
});
	
</script>