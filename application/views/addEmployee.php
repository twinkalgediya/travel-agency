<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"> 
<link href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.css" rel="stylesheet"> 

 <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li>
                                <h4 class="page-title">Dashboard</h4>
                            </li>
							<li>
                                <a href="<?php echo base_url(); ?>employeeListing">Employee</a>
                            </li>
                            
                            <li class="active">
                               <?php echo isset($Employee[0]->userId) ? 'Edit' : 'Add' ?>
                            </li>
                        </ol>
                    </div>
                </div>

				
                <div class="row">
                    <div class="col-sm-12">
					<label class="col-md-2 control-label"></div>
									
					<form action="<?php echo base_url() ?><?php echo isset($Employee[0]->userId) ? 'Employee/updateEmployee' : 'Employee/insertEmployee' ?>" name="insertInquiry" id="insertEmployee" method="post" class="form-horizontal" data-parsley-validate novalidate>
                        <div class="row">
                            <div class="col-sm-12">
							
                                <div class="card-box">
                                    
										<h5 class="text-muted text-uppercase m-t-0 m-b-20"><b>General</b></h5>
										
											<div class="row">
												<div class="col-md-12">
													<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
												</div>
											</div>
									
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">User Role : <span class="text-danger">*</span></label>
										<div class="col-md-6">
										<select name="roleId" value="roleId" class="form-control" required <?php if(isset($Employee[0]->userId) && $Employee[0]->userId > 0) { ?> 
											readonly
											<?php } ?>>
											<option value="">Select Role</option>
											<?php if(isset($getUserRoles)){ ?>
												<?php foreach($getUserRoles as $role){ ?>
													<option value="<?php echo $role->roleId; ?>" 
													<?php if(isset($Employee[0]->roleId) && $role->roleId == $Employee[0]->roleId){ ?> selected <?php } ?>> <?php echo $role->role; ?></option>
												 <?php } ?>
											<?php } ?>
											</select>
										</div>	
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Name :<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="name" name="name" maxlength="128" class="form-control" 
											value="<?php echo isset($Employee[0]->name) ? set_value("name", $Employee[0]->name) : set_value("name"); ?>"  placeholder="Enter Name" parsley-trigger="change" required>
											<?php if(isset($Employee[0]->userId)) { ?>
												<input type="hidden" name="userId" id="userId" value="<?php echo $Employee[0]->userId; ?>" >
											<?php } ?>
										</div>
									</div>
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Phone Number :<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="mobile" name="mobile" maxlength="15" class="form-control" 
											value="<?php echo isset($Employee[0]->mobile) ? set_value("mobile", $Employee[0]->mobile) : set_value("mobile"); ?>"  placeholder="Enter Phone Number" parsley-trigger="change" required>
										</div>
									</div>
									
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Email Id :<span class="text-danger">*</span></label>
										<div class="col-md-6">
											<input type="email" id="email" name="email" maxlength="128" class="form-control" 
											value="<?php echo isset($Employee[0]->email) ? set_value("email", $Employee[0]->email) : set_value("email"); ?>"  placeholder="Enter Your Email" parsley-trigger="change" required>
										</div>	
									</div>
								<?php if(!isset($Employee[0]->userId)) { ?>	
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Password :<span class="text-danger">*</span></label>
										<div class="col-md-6">
											<input type="password" id="password" name="password" maxlength="128" class="form-control" 
											value="<?php echo isset($Employee[0]->password) ? set_value("password", $Employee[0]->password) : set_value("password"); ?>"  placeholder="Enter User Password" parsley-trigger="change" required>
										</div>	
									</div>
									
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Confirm Password :<span class="text-danger">*</span></label>
										<div class="col-md-6">
											<input type="password" id="cpassword" name="cpassword" maxlength="128" class="form-control" 
											 placeholder="Enter User Confirm Password" parsley-trigger="change" required>
										</div>	
									</div>
								<?php } ?>	

										<hr />
									<div class="form-group p-20">
										<label class="col-md-1 control-label"></label>
										
										<div class="col-md-11">
										 <button type="submit" class="btn w-sm btn-default waves-effect waves-light">Save</button>
										 <button type="button" class="btn w-sm btn-white waves-effect"><a href="<?php echo base_url(); ?>employeeListing">Cancel</a></button>
										</div> 
									</div>
                                </div>
							</div>
						</div>
					</form>
                </div>
            </div>

<?php $this->load->view('includes/footer', ''); ?>           