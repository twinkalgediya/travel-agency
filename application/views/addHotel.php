<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"> 
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
 <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li>
                                <h4 class="page-title">Dashboard</h4>
                            </li>
							<li>
                                <a href="<?php echo base_url(); ?>hotelListing">Hotel</a>
                            </li>
                            
                            <li class="active">
                               <?php echo isset($Hotel[0]->id) ? 'Edit' : 'Add' ?>
                            </li>
                        </ol>
                    </div>
                </div>

				
                <div class="row">
                    <div class="col-sm-12">
					<label class="col-md-2 control-label"></div>
									
					<form action="<?php echo base_url() ?><?php echo isset($Hotel[0]->id) ? 'Hotel/updateHotel' : 'Hotel/insertHotel' ?>" name="insertHotel" id="insertHotel" method="post" enctype="multipart/form-data" class="form-horizontal" data-parsley-validate novalidate>
                        <div class="row">
                            <div class="col-sm-12">
							
                                <div class="card-box">
                                    
										<h5 class="text-muted text-uppercase m-t-0 m-b-20"><b>General</b></h5>
										
											<div class="row">
												<div class="col-md-12">
													<?php //echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
												</div>
											</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Hotel Name :<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="hotelName" name="hotelName" class="form-control <?php if( form_error('hotelName') != '') { ?> parsley-error <?php } ?>"
											value="<?php echo isset($Hotel[0]->hotelName) ? set_value("hotelName", $Hotel[0]->hotelName) : set_value("hotelName"); ?>"  placeholder="Enter Hotel Name" parsley-trigger="change" required>
											<?php if(isset($Hotel[0]->id)) { ?>
												<input type="hidden" name="id" id="id" value="<?php echo $Hotel[0]->id; ?>" >
												<input type="hidden" name="hotel_photo" id="hotel_photo" value="<?php echo $Hotel[0]->hotelPhoto; ?>" >
											<?php } ?>
											<?php if( form_error('hotelName') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('hotelName'); ?></li></ul>
											<?php } ?>
										</div>
									</div>
								<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Phone Number :<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="phoneNumber" name="phoneNumber" maxlength="15" class="form-control <?php if( form_error('phoneNumber') != '') { ?> parsley-error <?php } ?>" 
											value="<?php echo isset($Hotel[0]->phoneNumber) ? set_value("phoneNumber", $Hotel[0]->phoneNumber) : set_value("phoneNumber"); ?>"  placeholder="Enter Phone Number" parsley-trigger="change" required>
											
											<?php if( form_error('phoneNumber') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('phoneNumber'); ?></li></ul>
											<?php } ?>
										</div>
									</div>
									
								<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Alternet Phone Number :</label>
										<div class="col-md-6"> 
											<input type="text" id="altPhoneNumber" name="altPhoneNumber" maxlength="15" class="form-control <?php if( form_error('altPhoneNumber') != '') { ?> parsley-error <?php } ?>" 
											value="<?php echo isset($Hotel[0]->altPhoneNumber) ? set_value("altPhoneNumber", $Hotel[0]->altPhoneNumber) : set_value("altPhoneNumber"); ?>"  placeholder="Enter Alternet Phone Number" parsley-trigger="change" >
											
											<?php if( form_error('altPhoneNumber') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('altPhoneNumber'); ?></li></ul>
											<?php } ?>
										</div>
									</div>	
									


									<div class="form-group m-b-20">
									<label class="col-md-2 control-label">Countries :<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
									<select name="country" class="form-control" id="country">
								      <option value="">Select Country</option>
								       <?php foreach($countries as $count): ?>
								        <option value="<?php echo $count->id; ?>" <?php if (isset($Hotel[0]->country_id) && $Hotel[0]->country_id == $count->id) echo 'selected' ; ?>><?php echo $count->name; ?></option>
								         <?php endforeach; ?> 
								      </select>
								      <?php if( form_error('country') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('country'); ?></li></ul>
											<?php } ?>
								      	</div>
									</div>	


									<!-- State dropdown -->
									<div class="form-group m-b-20">
									<label class="col-md-2 control-label">State :<span class="text-danger">*</span></label>
										<div class="col-md-6">
									<select id="state" name="state" class="form-control">
									    <option value="">Select country first</option>
									   <?php if(isset($state) && !empty($state)) { ?>
									    	 <?php foreach($state as $count): ?>
								        <option value="<?php echo $count->id; ?>" <?php if (isset($Hotel[0]->state_id) && $Hotel[0]->state_id == $count->id) echo 'selected' ; ?>><?php echo $count->name; ?></option>
								         <?php endforeach; ?> 
								          <?php } ?>
									    
									</select>
									  <?php if( form_error('state') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('state'); ?></li></ul>
											<?php } ?>
										</div>
									</div>
									<!-- City dropdown -->
									<div class="form-group m-b-20">
									<label class="col-md-2 control-label">Cities
									 :<span class="text-danger">*</span></label>
										<div class="col-md-6">
									<select id="city" name="city" class="form-control">
									    <option value="">Select state first</option>

									     <?php if(isset($state) && !empty($state)) { ?>
									    	 <?php foreach($cities as $count): ?>
								        <option value="<?php echo $count->id; ?>" <?php if (isset($Hotel[0]->city_id) && $Hotel[0]->city_id == $count->id) echo 'selected' ; ?>><?php echo $count->name; ?></option>
								         <?php endforeach; ?> 
								          <?php } ?>
									</select>
									<?php if( form_error('city') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('city'); ?></li></ul>
											<?php }	 ?>
								    </div>
							        </div>
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Email Id :<span class="text-danger">*</span></label>
										<div class="col-md-6">
											<input type="email" id="email_Id" name="email_Id" maxlength="128" class="form-control <?php if( form_error('email_Id') != '') { ?> parsley-error" <?php } ?>" 
											value="<?php echo isset($Hotel[0]->email_Id) ? set_value("email_Id", $Hotel[0]->email_Id) : set_value("email_Id"); ?>"  placeholder="Enter Your Email" parsley-trigger="change" required>
											
											<?php if( form_error('email_Id') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('email_Id'); ?></li></ul>
											<?php } ?>
										</div>	
									</div>
									
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Alternet Email Id :</label>
										<div class="col-md-6">
											<input type="email" id="altEmail" name="altEmail" maxlength="128" class="form-control <?php if( form_error('altEmail') != '') { ?> parsley-error" <?php } ?>" 
											value="<?php echo isset($Hotel[0]->altEmail) ? set_value("altEmail", $Hotel[0]->altEmail) : set_value("altEmail"); ?>"  placeholder="Enter Your Email" parsley-trigger="change">
											
											<?php if( form_error('altEmail') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('altEmail'); ?></li></ul>
											<?php } ?>
										</div>	
									</div>
									
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">stareRatting : <span class="text-danger">*</span></label>
										<div class="col-md-6">
											<select name="stareRatting" value="stareRatting" class="form-control <?php if( form_error('stareRatting') != '') { ?> parsley-error" <?php } ?>" required>
												<option value="">Select Ratting</option>
												<option value="1" <?php if (isset($Hotel[0]->stareRatting) && $Hotel[0]->stareRatting == '1') echo 'selected' ; ?>>One Stare</option>
												<option value="2" <?php if (isset($Hotel[0]->stareRatting) && $Hotel[0]->stareRatting == '2') echo 'selected' ; ?>>Two Stare</option>
												<option value="3" <?php if (isset($Hotel[0]->stareRatting) && $Hotel[0]->stareRatting == '3') echo 'selected' ; ?>>Three Stare</option>
												<option value="4" <?php if (isset($Hotel[0]->stareRatting) && $Hotel[0]->stareRatting == '4') echo 'selected' ; ?>>Four Stare</option>
												<option value="5" <?php if (isset($Hotel[0]->stareRatting) && $Hotel[0]->stareRatting == '5') echo 'selected' ; ?>>Five Stare</option>
                                            </select>
											<?php if( form_error('stareRatting') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('stareRatting'); ?></li></ul>
											<?php } ?>
										</div>	
									</div>
									
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Address :<span class="text-danger">*</span></label>
										<div class="col-md-6">
											<textarea name="address" id="address" required parsley-trigger="change" cols="88" rows="5"><?php echo isset($Hotel[0]->address) ? set_value("address", $Hotel[0]->address) : set_value("address"); ?>
											</textarea>
											<?php if( form_error('address') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('address'); ?></li></ul>
											<?php } ?>
										</div>	
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Hotel Photo: <span class="text-danger">*</span></label>
										<div class="col-md-6">
										
											<input type="file" name="hotelPhoto" class="filestyle <?php if( form_error('hotelPhoto') != '') { ?> parsley-error" <?php } ?>  data-iconname="fa fa-cloud-upload" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);" <?php echo isset($Hotel[0]->hotelPhoto) ? set_value("hotelPhoto", $Hotel[0]->hotelPhoto) : set_value("hotelPhoto"); ?> <?php if(!isset($Hotel[0]->id)){ ?> required <?php } ?>>
											<?php if( form_error('hotelPhoto') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('hotelPhoto'); ?></li></ul>
											<?php } ?>
										</div>
									</div>
									
									<?php if(isset($Hotel[0]->hotelPhoto)){ ?>
											<div class="col-md-12" id="image_list" style="margin-bottom:20px">
											<label class="col-md-2 control-label"></label>
											<div class="col-md-6">
												<div class="col-md-4 m-b-20"><img src="<?php echo base_url() ?>uploads/images/<?php echo $Hotel[0]->hotelPhoto; ?>" height="120px" width="180px"/></div>
											</div> 
											</div>
										<?php } ?>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Hotel Extra Photo: <span class="text-danger">*</span></label>
										<div class="col-md-6">
											<input type="file" name="multiplehotelPhoto[]" class="filestyle <?php if( form_error('multiplehotelPhoto') != '') { ?> parsley-error" <?php } ?>  data-iconname="fa fa-cloud-upload" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);" <?php echo isset($Hotel[0]->multiplehotelPhoto) ? set_value("multiplehotelPhoto", $Hotel[0]->multiplehotelPhoto) : set_value("multiplehotelPhoto"); ?> multiple <?php if(!isset($Hotel[0]->id)){ ?> required <?php } ?>>
											<?php if( form_error('multiplehotelPhoto') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('multiplehotelPhoto'); ?></li></ul>
											<?php } ?>
										</div>
									</div>
									<?php if(isset($HotelPhotodata)){ ?>
											<div class="col-md-12" id="image_list" style="margin-bottom:20px">
											<label class="col-md-2 control-label"></label>
											<div class="col-md-6">
											<?php	
											 for($i = 0; $i < count($HotelPhotodata); $i++){ ?>
												<div class="col-md-4 m-b-20" id="img_div_<?php echo $HotelPhotodata[$i]['id']?>"><img src="<?php echo base_url() ?>uploads/images/<?php echo  $HotelPhotodata[$i]['image']; ?>" height="120px" width="180px"/><span onclick="javasctipt: removethis(<?php echo  $HotelPhotodata[$i]['hotelID']; ?>,<?php echo $HotelPhotodata[$i]['id']; ?>)"><i class="fa fa-trash trash-icon pull-right" title="Remove" ></i></span></div>
											 <?php } ?>
											</div> 
											</div>
										<?php } ?>
										
									
									
									
										<hr />
									<div class="form-group p-20">
										<label class="col-md-1 control-label"></label>
										
										<div class="col-md-11">
										 <button type="submit" class="btn w-sm btn-default waves-effect waves-light">Save</button>
										 <button type="button" class="btn w-sm btn-white waves-effect"><a href="<?php echo base_url(); ?>hotelListing">Cancel</a></button>
										</div> 
									</div>
                                </div>
							</div>
						</div>
					</form>
                </div>
            </div>

<?php $this->load->view('includes/footer', ''); ?>
<script type="text/javascript">

jQuery(document).ready(function($) {
		$('.counter').counterUp({
			delay: 100,
			time: 1200
		});
		$(".knob").knob();
		
		$('form').parsley();



		  $('#country').on('change',function(){
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('booking/getStates'); ?>',
                data:'country_id='+countryID,
                success:function(data){
                    $('#state').html('<option value="">Select State</option>'); 
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.id).text(this.name);           
                            $('#state').append(option);
                        });
                    }else{
                        $('#state').html('<option value="">State not available</option>');
                    }
                }
            }); 
        }else{
            $('#state').html('<option value="">Select country first</option>');
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
    
    /* Populate data to city dropdown */
    $('#state').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('booking/getCities'); ?>',
                data:'state_id='+stateID,
                success:function(data){
                    $('#city').html('<option value="">Select City</option>'); 
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.id).text(this.name);           
                            $('#city').append(option);
                        });
                    }else{
                        $('#city').html('<option value="">City not available</option>');
                    }
                }
            }); 
        }else{
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
  
			
});

	/* remove image */
	function removethis(hotelID, id)
	{
		if( confirm('Are you sure to delete this file') == true )
		{
			if ( hotelID != '' )
			{
				$.ajax({
					url:'<?php echo base_url();?>deleteHotelPhoto/'+hotelID+'/'+id,
					method: 'post',
					data: {hotelID: hotelID, id:id},
					dataType: 'json',
					success: function(response){
						
						if(response.Del == 'TRUE'){
							$('#img_div_'+id).html('');
						}
					}
				});
			}
		}
	}	
</script>