<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $package[0]->packageName; ?></title>
	<style type="text/css">
		.gr1{
			background: #a1eaff;  /* fallback for old browsers */
			background: -webkit-linear-gradient(to top, #e3f8ff, #a1eaff);  /* Chrome 10-25, Safari 5.1-6 */
			background: linear-gradient(to top, #e3f8ff, #a1eaff); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
			border-radius: 15px;
			float: right;
			margin-bottom:20px;
			width: 256px;
			border: 2px solid #57b4ce;
		}
		.gr2{
			background: #e4edff;  /* fallback for old browsers */
			background: -webkit-linear-gradient(to top, #e4edff, #a4c5ff);  /* Chrome 10-25, Safari 5.1-6 */
			background: linear-gradient(to top, #e4edff, #a4c5ff); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
			border: 2px solid #57b4ce;
			margin:0px -60px 0px -60px;
			padding:5px 20px 5px 20px;
			clear:both;
		}
		.gr4{
			background: #a1eaff;  /* fallback for old browsers */
			background: -webkit-linear-gradient(to top, #e3f8ff, #a1eaff);  /* Chrome 10-25, Safari 5.1-6 */
			background: linear-gradient(to top, #e3f8ff, #a1eaff); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
			border-radius: 15px;
			float: right;
			font-size:20px;
			padding:10px 20px 10px 20px;
			margin:20px 20px 20px 20px;
			border: 2px solid #57b4ce;
		}
		.gr3{
			margin:80px -70px 0px -80px;;
			padding:5px 20px 5px 20px;
			clear:both;
		}
		
		.package-title{
			background-image:url(<?php echo base_url().'uploads/logo/package-title.png'; ?>);
			background-size: contain;
			text-align:center;
			background-repeat: no-repeat;
			color:#fff;
			margin-top:50px;
		}
		.review
		{ 
			background-repeat: no-repeat;
			height:50px;
			
		}
		.package-highlight-title{
			background-image:url(<?php echo base_url().'uploads/logo/package-highlight-title.png'; ?>);
			background-size: contain;
			margin-top:50px;
			margin-bottom:30px;
			text-align:center;
			background-repeat: no-repeat;
			color:#000;
		}
		.curve{
			background-image:url(<?php echo base_url().'uploads/logo/small-curve.png'; ?>);
			background-size: contain;
			text-align:center;
			width:260px;
			background-repeat: no-repeat;
			color:#234060;
			float:right;
			position:absolute;
			
		}
		.shape{
			margin:50px -60px 0px -57px;
		}
		.highlight{
			background: #a1eaff;  /* fallback for old browsers */
			background: -webkit-linear-gradient(to top, #e3f8ff, #a1eaff);  /* Chrome 10-25, Safari 5.1-6 */
			background: linear-gradient(to top, #e3f8ff, #a1eaff); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
			border-radius: 35px 35px 0px 0px;
			width: 500px;
			border: 2px solid #57b4ce;
			margin:120px 0px 0px -45px;
		}
		.title-arrow{
			background-image:url(<?php echo base_url().'uploads/logo/title-arrow.png'; ?>);
			background-size: contain;
			+text-align:left;
			margin:10px 0px 20px -60px;
			padding-left:40px;
			height:50px;
			width:500px;
			background-repeat: no-repeat;
			color:#000;
		}
		.hotel-detail{
			background-image:url(<?php echo base_url().'uploads/logo/hotel.png'; ?>);
			background-repeat: no-repeat;
			height:200px;
			color:#fff;
			margin-top:50px;
			margin-bottom:100px;
		}
		.hotel-detail table tr{
			border:1px solid #000;
		}
		.hotel-detail table tr td{
			border-left:1px solid #000;
			padding:10px;
			text-align:center;
		}
		.first_tr{border-bottom:1px solid #000;}
		.purple-gradient{
			background: #efe9f8;  /* fallback for old browsers */
			background: -webkit-linear-gradient(to top, #efe9f8, #cab6e8);  /* Chrome 10-25, Safari 5.1-6 */
			background: linear-gradient(to top, #efe9f8, #cab6e8); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
			border-radius: 15px;
			border: 2px solid #8770aa;
			text-align:center;
			margin-bottom:30px;
			padding:10px;
		}
		.single_img{
			border: 7px solid #fff;
			height:200px;
			width:200px;
			
		}
		.gallery_images{margin-top:50px;}
		.price_detail{
				margin:0px -70px 0px 0px;;
			
			clear:both;
		}
		

	</style>
</head>
<body style="background-image:url(<?php echo base_url().'uploads/logo/bg-img.jpg'; ?>); height:1400px; background-repeat:no-repeat; padding:0px;">
<?php if(isset($package[0])){ ?>
	<div class="gr1">
		<img class="logo_img" height="100px" width="256px" src="<?php echo base_url().'uploads/logo/logo.png'; ?>"/>
	</div>
	<div class="clear:both;"></div>
	<div class="gr2">
		<h2>Dear <?php echo $package[0]->firstName; ?>,</h2>
		<p>We thank you for giving us the opportunity to let us customize your upcoming holidays. Our single motto is to provide a great travel experience to all our travellers, as we know that holidays are dreams and we are the ‘Dream Makers’</p>
	</div>
	<div class="package-title">
		<h1><?php echo $package[0]->packageName; ?><sub>(<?php echo $package[0]->nightStayPlan; ?>)</sub></h1>
		<h5><?php echo $package[0]->packageName.' N '; ?>(<?php echo $package[0]->nightStayPlan; ?>)</h5>
	</div>
	<div class="shape">
		<div class="curve">
			<h4>Departure: <?php echo ucfirst($package[0]->depatureCity);?></h4>
		</div>
		<div class="curve" style="float:left">
			<h4>Travel Date: <?php echo date("d-M-Y", strtotime($package[0]->travelDate));?></h4>
		</div>
	</div>
	<div class="highlight">
		<div class="package-highlight-title">
			<h2>Package Highlights</h2>
		</div>
		<div style="float:left; width:250px;">
			<ul  style=" list-style-type: none;">
				<li style="margin-bottom:3px;"><span>1.</span> <a href="">Package Inclusions</a></li>
				
				<li style="margin-bottom:3px;"><span>2.</span> <a href=""> Package Itinerary</a></li>
				<li style="margin-bottom:3px;"><span>3.</span> <a href=""> Hotel Detail</a></li>
				<li style="margin-bottom:3px;"><span>4.</span> <a href="">Hotel Gallery</a></li>
				<li style="margin-bottom:3px;"><span>5.</span> <a href=""> Flight Details</a></li>
				<li style="margin-bottom:3px;"><span>5.</span> <a href=""> Pricing Details</a></li>
				<li style="margin-bottom:3px;"><span>7.</span> <a href=""> Boooking Procedure</a></li>
				<li style="margin-bottom:3px;"><span>15.</span> <a href=""> Boooking Procedure</a></li>
			</ul>
		</div>
		<div>
			<ul  style=" list-style-type: none;">
				<li style="margin-bottom:3px;"><span>8.</span><a href=""> Customization In Package	</a></li>
				<li style="margin-bottom:3px;"><span>9.</span><a href="">  Key Features</a></li>
				<li style="margin-bottom:3px;"><span>10.</span><a href=""> Customer speaks</a></li>
				<li style="margin-bottom:3px;"><span>11.</span><a href=""> Customer Experience</a></li>
				<li style="margin-bottom:3px;"><span>12.</span><a href=""> Terms & Conditions</a></li>
				<li style="margin-bottom:3px;"><span>13.</span><a href=""> Booking Policy</a></li>
				<li style="margin-bottom:3px;"><span>14.</span> <a href=""> Cancellation Policy</a></li>
			</ul>
		</div>
	</div>
	<div class="title-arrow">
		<h2 style="height:35px; padding-top:10px;">1. Package Inclusions </h2>
	</div>
	<div class="purple-gradient">
		<p style="width:800px; height:auto;"><?php echo $package[0]->packageInclusion; ?></p>
	</div>
	<div class="title-arrow">
		<h2 style="height:35px; padding-top:-3px;">2. Package Itinerary  </h2>
	</div>
	<div class="itinerary-description" style="background-color:#000;  color:#ffffff; padding:10px;">
		
		<p style="float:left; "><?php echo $package[0]->itinanrary_data; ?></p>
	</div>
	<!--<div class="title-arrow">
		<h2 style="height:35px; padding-top:-3px;">3. Hotel Details</h2>
	</div>
	
	<div class="hotel-detail">
		<table autosize="1" style="border:1px solid #000; height:200px; margin-top:50px;">
		   <tr>
				<td class="first_tr" width="20%">City</td>
				<td class="first_tr" width="20%">Hotel Name</td>
				<td class="first_tr" width="20%">Check In Date</td>
				<td class="first_tr" width="20%">Check Out Date</td>
				<td class="first_tr" width="20%">Meal Plan</td>
		   </tr>

		   <tr>
				<td width="20%">Manali</td>
				<td width="20%">Jupiter</td>
				<td width="20%">14-Jul-2018</td>
				<td width="20%">17-Jul-2018</td>
				<td width="20%">Dinner</td>
		   </tr>
		</table>
	</div>-->
	<pagebreak/>
	<div class="title-arrow">
		<h2 style="height:35px; padding-top:10px;">4. Hotel Gallery</h2>
		</div>
		<div class="gallery_images" style="margin-top:-30px;">
		<?php if(isset($galary) && !empty($galary)) { ?>
		
			<?php for($i = 0; $i < count($galary); $i++){ ?>
				<div class="single_img" style="float:left; margin-right:40px;height:200px; width:200px; ">
					<img src="<?php echo base_url() ?>uploads/images/<?php echo  $galary[$i]->image; ?>" style="height:200px; width:200px;"/>
				</div>
			<?php } ?>
		<?php } ?>
			
		</div>
		
	<!--<div class="title-arrow">
		<h2 style="height:35px; padding-top:-3px;">5. Flight Details</h2>
	</div>-->	
	<pagebreak/>
	<div class="title-arrow">
		<h2 style="height:35px; padding-top:10px;"> 6. Pricing Details</h2>
	</div>
	<div class="price_detail" style="float:right; margin-right:-10px; padding-top:-40px; padding-bottom:90px;">
			<img src="<?php echo base_url().'uploads/logo/package_cost.png'; ?>"  style=" margin-top:20px;"/>
			<p style="float:left; color:#fff; margin-top:-100px; font-weight:bold; margin-left:30px; font-size:20px;">Total Package Cost: INR 24,000 /- </p><br />
			<p style="float:left; color:#fff; margin-top:-24px; margin-left:60px;">(5% GST extra on total Package)
 </p>
	</div>
		
	<div class="title-arrow">
		<h2 style="height:35px; padding-top:-3px;">7. Pay Initial amount now to Book </h2>
	</div>
	<div class="initial_pay" style="float:left; margin-left:-50px;">
	<?php if($package[0]->paymentLink != ''){
		$link =  $package[0]->paymentLink;
	}else {
		$link = '#';
		 }
		 ?>
			<img src="<?php echo base_url().'uploads/logo/squer-thumb.png'; ?>" style=" margin-top:20px";/>
			<p style="float:left; color:#fff; margin-top:-100px; font-weight:bold; margin-left:60px; font-size:20px;">1. Online Payment : Credit Card/ Debit Card </p><br />
			<p style="float:left; color:#fff; margin-top:-30px; font-weight:bold; margin-left:60px;">Pay Online: 
			<a href="<?php echo $link; ?>" target="_blank"><?php echo $link; ?></a> </p>
	</div>
		
	
	<div style="float:left;  margin-left:-50px;">
		<img src="<?php echo base_url().'uploads/logo/bank-details.png'; ?>"  style=" margin-top:70px;"/>
	</div>
	<pagebreak/>
	<div class="title-arrow">
		<h2 style="height:35px; padding-top:10px;">8. Customization In Package</h2>
	</div>
		<div class="gr4" style="float:right; margin-left:-10px; margin-bottom:100px; text-align:center;">
			In case you require any changes in given package like hotel, flight or any kind  of customization in package you can let us know, as we are not stick to any  particular hotel, this is customized package and you are free to do any changes 
		</div>
	
	<div class="title-arrow">
		<h2 style="height:35px; padding-top:-3px;">9. Key Features </h2>
	</div>
		<div class="gr4" style="float:right; margin-left:-10px; text-align:center;">
			24*7 Customer Support 
			<br>Highest Rated Travel Agency 
			<br>Experienced Travel Expert Guidance
			<br>Beat the Price Policy (We will beat any price of market offering the same services)  
		</div>
	
		<pagebreak/>
	<div class="title-arrow">
		<h2 style="height:35px; padding-top:10px;"> 10. Customer Speaks</h2>
	</div>
		<br>
		<div class="ratting">
			<img src="<?php echo base_url().'uploads/logo/review.png'; ?>"  style=" height:800px;"/>
		</div>
		
		<pagebreak/>
	<div class="title-arrow">
		<h2 style="height:35px; padding-top:10px;"> 11. Customer Experience</h2></div>	
		<div class="photos">
			<img src="<?php echo base_url().'uploads/logo/photos.png'; ?>"  style="margin-right:50px; margin-top:100px;height:1500px; width:1500px;"/>
		</div>
		
			<pagebreak/>
		<div class="title-arrow">
			<h2 style="padding-left:10px; padding-top:5px;">  12. Terms & Conditions</h2>
		</div>
		<div class="itinerary-description" style="padding-top:-30px;">
		<p style="background-color:#000; color:#fff; padding:2px; 0px; 2px; 5px; width:900px;">
		Transportation provided will be as per itinerary only and not at disposal. The package does not include guide services. Ac Will be switched off in hilly areas.</p>
		<p style="background-color:#000; color:#fff; padding:1px; 0px; 1px; 5px; width:900px;">All above rates are subject to change without prior notice as per availability & final date of travel. Any meals and services are not Included unless mentioned in Inclusions.</p>
		<p style="background-color:#000; color:#fff; padding:1px; 0px; 1px; 5px; width:900px;">Refund on cancellation strictly depends upon concerned hotel, car or flight cancellation policy. Day-wise Itinerary plan and sightseeing can be changed internally, or cancelled if any unavoidable circumstances in the particular place.</p>
		<p style="background-color:#000; color:#fff; padding:1px; 0px; 1px; 5px; width:900px;">In case of unavailability in the mentioned hotels, alternate accommodation will be arranged in a similar or upgraded category hotel.</p>
		<p style="background-color:#000; color:#fff; padding:1px; 0px; 1px; 5px; width:900px;">Vibrant Holidays reserves the right to change/modify or terminate the offer any time at its own discretion and without any prior notice.</p>
		
		<!--<p style="background-color:#000; color:#fff; padding:10px;">
		Transportation provided will be as per itinerary only and not at disposal. 
		</br>The package does not include guide services. Ac Will be switched off in hilly areas.
		<br>All above rates are subject to change without prior notice as per availability & final date of travel.
		<br> Any meals and services are not Included unless mentioned in Inclusions.
		<br> Refund on cancellation strictly depends upon concerned hotel, car or flight cancellation policy.
		<br> Day-wise Itinerary plan and sightseeing can be changed internally,
		<br> or cancelled if any unavoidable circumstances in the particular place.
		<br> In case of unavailability in the mentioned hotels,
		<br> alternate accommodation will be arranged in a similar or upgraded category hotel.
		<br> Vibrant Holidays reserves the right to change/modify or terminate the offer
		<br> any time at its own discretion and without any prior notice. 
		</p>-->
		</div>
		
		<div class="title-arrow">
			<h2 style="height:10px;padding-left:30px;">13. Booking Policy </h2>
		</div>
		<div class="itinerary-description" style="padding-top:-30px;">
		<p style="background-color:#000; color:#fff; padding:2px; 0px; 2px; 5px; width:570px;">
		Prior 30 days or more, Airfare + 30% of Land package will be the initial payment.</p>
		<p style="background-color:#000; color:#fff; padding:2px; 0px; 2px; 5px; width:550px;">Prior 29 to 11 days, Airfare + 50% of Land package will be the initial payment.</p>
		<p style="background-color:#000; color:#fff; padding:2px; 0px; 2px; 5px; width:470px;"> Prior 10 Days or lesser, 100% of the package cost will be required.</p> 
		</div> 
		
		<div class="title-arrow">
		<h2 style="padding-left:30px;  margin-top:5px;">14. Cancellation Policy </h2>
		</div>
		<div class="itinerary-description" style="padding-top:-30px;">
		<p style="background-color:#000; color:#fff; padding:2px; 0px; 2px; 5px; width:500px;">
		Prior 30 days or more Airfare + 30% of Land package – Non Refundable</p>
		<p style="background-color:#000; color:#fff; padding:2px; 0px; 2px; 5px; width:420px;">
		Prior 29 to 11 days, Airfare + 50% of Land – Non Refundable</p>
		<p style="background-color:#000; color:#fff; padding:2px; 0px; 2px; 5px; width:400px;">
		Prior 10 Days or lesser 100% payment – Non Refundable</p>
		</div>
		
		<div class="title-arrow">
		<h2 style="padding-left:30px; margin-top:5px;">
		15. Contact Us </h2>
		</div>
		
		
		<div class="ratting">
			<img src="<?php echo base_url().'uploads/logo/sign.png'; ?>" style="height:165px;"/>
		<div>
		
<?php } ?> 
</body>
</html>