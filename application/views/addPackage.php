<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"> 
<link href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.css" rel="stylesheet"> 

 <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li>
                                <h4 class="page-title">Dashboard</h4>
                            </li>
							<li>
                                <a href="<?php echo base_url(); ?>packageListing">Package</a>
                            </li>
                            
                            <li class="active">
                               <?php echo isset($Inquiry[0]->id) ? 'Edit' : 'Add' ?>
                            </li>
                        </ol>
                    </div>
                </div>

				
                <div class="row">
                    <div class="col-sm-12">
					<label class="col-md-2 control-label"></div>
									
					<form action="<?php echo base_url() ?><?php echo isset($Itn[0]->id) ? 'Package/updatePackage' : 'Package/insertPackage' ?>" name="insertPackage" id="insertPackage" enctype="multipart/form-data" method="post" class="form-horizontal" data-parsley-validate novalidate autocomplete="off">
					
                        <div class="row">
                            <div class="col-sm-12">
							
                                <div class="card-box">
                                    
										<h5 class="text-muted text-uppercase m-t-0 m-b-20"><b>General</b></h5>
										
											<div class="row">
												<div class="col-md-12">
													<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
												</div>
											</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">First Name :<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="firstName" name="firstName" class="form-control" 
											value="<?php echo $Package[0]->firstName; ?>" required>
											<?php if(isset($Package[0]->id)) { ?>
												<input type="hidden" name="inquiry_id" id="inquiry_id" value="<?php echo $Package[0]->id; ?>" >
											<?php } ?>
											<?php if(isset($Itn[0]->id)) { ?>
												<input type="hidden" name="package_id" id="package_id" value="<?php echo $Itn[0]->id; ?>" >
											<?php } ?>
											<?php if(isset($Itn[0]->inquiryId)) { ?>
												<input type="hidden" name="inqId" id="inqId" value="<?php echo $Itn[0]->inquiryId; ?>" >
											<?php } ?>
										</div>
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Last Name :<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="lastName" name="lastName" class="form-control" 
											value="<?php echo $Package[0]->lastName; ?>" required>
										</div>
									</div>
																		
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">No Off Adults :<span class="text-danger">*</span></label>
										<div class="col-md-6">
											<input type="numer" id="noOffAdults" name="noOffAdults" maxlength="3" class="form-control" 
											value="<?php echo isset($Package[0]->noOffAdults) ? set_value("noOffAdults", $Package[0]->noOffAdults) : set_value("noOffAdults"); ?>"  placeholder="Enter No Off Adults" parsley-trigger="change" required>
										</div>	
									</div>
									
								
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Child With Extra Bed :</label>
										<div class="col-md-6">
											<input type="numer" id="childWithExtraBed" name="childWithExtraBed" maxlength="3" class="form-control" 
											value="<?php echo isset($Package[0]->childWithExtraBed) ? set_value("childWithExtraBed", $Package[0]->childWithExtraBed) : set_value("childWithExtraBed"); ?>"  placeholder="Enter No Off Child With Extra bed" parsley-trigger="change" >
										</div>	
									</div>
									
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Child Without Extra Bed :</label>
										<div class="col-md-6">
											<input type="numer" id="childWithoutExtraBed" name="childWithoutExtraBed" maxlength="3" class="form-control" 
											value="<?php echo isset($Package[0]->childWithoutExtraBed) ? set_value("childWithoutExtraBed", $Package[0]->childWithoutExtraBed) : set_value("childWithoutExtraBed"); ?>"  placeholder="Enter No Off Child Without Extra bed" parsley-trigger="change">
										</div>	
									</div>
									
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Travel Date : <span class="text-danger">*</span></label>
										<div class="col-md-6">
											<div class='input-group date' id='datetimepicker1'>
												<input type='text' name="travelDate" id="travelDate" class="form-control" value="<?php if(isset($Package[0]->travelDate)){
													echo $Package[0]->travelDate;
												} ?>" placeholder="Select Travel Date" required/>
													<span class="input-group-addon">
														<span class="glyphicon glyphicon-calendar"></span>
													</span>
											</div>
										</div>	
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Package Name :<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="packageName" name="packageName" class="form-control" 
											value="<?php echo isset($Itn[0]->packageName) ? set_value("packageName", $Itn[0]->packageName) : set_value("packageName"); ?>" required>
										</div>
									</div>
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Type Of Place:<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="radio" class="dom" name="typeof_place" value="domestic" <?php  if(isset($Itn[0]->typeof_place) && $Itn[0]->typeof_place == "domestic") echo "checked"?>>Domestic</input>
											<input type="radio" class="inter" name="typeof_place" value="international" <?php if(isset($Itn[0]->typeof_place) && $Itn[0]->typeof_place == "international") echo "checked"?> >International</input>
											
											<?php if( form_error('typeof_place') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('typeof_place'); ?></li></ul>
											<?php } ?>
										</div>
									</div>
								<div class="domestic"  style='display:none'>
									<div class="form-group m-b-20">
									<label class="col-md-2 control-label">Countries :<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
									<select id="country_id" name="country_id" class="form-control" id="country_id">
								      <option value="">Select Country</option>

								       <?php foreach($countries as $count): ?>
								        <option value="<?php echo $count->country_id; ?>" <?php if (isset($Itn[0]->country_id) && $Itn[0]->country_id == $count->country_id) echo 'selected' ; ?>><?php echo $count->name; ?></option>
								         <?php endforeach; ?> 
								      </select>
								      <?php if( form_error('country_id') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('country_id'); ?></li></ul>
											<?php } ?>
								      	</div>
									</div>	
								</div>

									<div class="international"  style='display:none'>
									<div class="form-group m-b-20">
									<label class="col-md-2 control-label">State :<span class="text-danger">*</span></label>
										<div class="col-md-6">
									<select id="state_id" name="state_id" class="form-control">
									    <option value="">Select country first</option>
									   <?php if(isset($state) && !empty($state)) { ?>
									    	 <?php foreach($state as $count): ?>
								        <option value="<?php echo $count->state_id; ?>" <?php if (isset($Itn[0]->state_id) && $Itn[0]->state_id == $count->state_id) echo 'selected' ; ?>><?php echo $count->name; ?></option>
								         <?php endforeach; ?> 
								          <?php } ?>
									    
									</select>
									  <?php if( form_error('state_id') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('state_id'); ?></li></ul>
											<?php } ?>
										</div>
									</div>
								</div>
								
								<div class="form-group m-b-20">
									<label class="col-md-2 control-label">No Of Night :<span class="text-danger">*</span></label>
										<div class="col-md-6">
									<select id="no_of_nights" name="no_of_nights" class="form-control">
									    <option value="">Select No Of Night</option>
									   <?php if(isset($noofnight) && !empty($noofnight)) { ?>
									    	 <?php foreach($noofnight as $count): ?>
								        <option value="<?php echo $count->id; ?>" <?php if (isset($Itn[0]->no_of_night) && $Itn[0]->no_of_night == $count->id) echo 'selected' ; ?>><?php echo $count->no_of_nights; ?></option>
								         <?php endforeach; ?> 
								          <?php } ?>
									    
									</select>
									 <?php if( form_error('no_of_nights') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('no_of_nights'); ?></li></ul>
											<?php } ?>
										</div>
									</div>
									<div class="form-group m-b-20">
									<label class="col-md-2 control-label">Night Stay Plan:<span class="text-danger">*</span></label>
										<div class="col-md-6">
									<select id="nightStayPlan" name="nightStayPlan" class="form-control">
									    <option value="">Night Stay Plan</option>
									   <?php if(isset($nightplan) && !empty($nightplan)) { ?>
									    	 <?php foreach($nightplan as $count): ?>
								        <option value="<?php echo $count->id; ?>" <?php if (isset($Itn[0]->nightStayPlan) && $Itn[0]->nightStayPlan == $count->id) echo 'selected' ; ?>><?php echo $count->night_stay; ?></option>
								         <?php endforeach; ?> 
								          <?php } ?>
									    
									</select>
									 <?php if( form_error('nightStayPlan') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('nightStayPlan'); ?></li></ul>
											<?php } ?>
										</div>
									</div>
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label" for="itinanrary_data">Package Itinarary:<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<textarea name="itinanrary_data" id="itinanrary_data" class="form-control" required><?php echo isset($Itn[0]->itinanrary_data) ? set_value("itinanrary_data", $Itn[0]->itinanrary_data) : set_value("itinanrary_data"); ?></textarea>
										</div>
									</div>
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Payment Link:<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="paymentLink" name="paymentLink" class="form-control" 
											value="<?php echo isset($Itn[0]->paymentLink) ? set_value("paymentLink", $Itn[0]->paymentLink) : set_value("paymentLink"); ?>" required>
										</div>
									</div>
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label" for="hotelDetail">Hotel Detail:</label>
										<div class="col-md-6"> 
											<textarea name="hotelDetail" id="hotelDetail" class="form-control" ><?php echo isset($Itn[0]->hotelDetail) ? set_value("hotelDetail", $Itn[0]->hotelDetail) : set_value("hotelDetail"); ?></textarea>
										</div>
									</div>
									
								<div class="slider_data">
									<?php if(isset($flightPhotodata) && !empty($flightPhotodata)){ ?>
										<div class="col-md-12" id="flightimage_list" style="margin-bottom:20px">
											<label class="col-md-2 control-label">Flight Detail</label>
											<div class="col-md-6">
											<?php	
											 for($i = 0; $i < count($flightPhotodata); $i++){ ?>
											 	<div id="flightimg_div_<?php echo $flightPhotodata[$i]['id']; ?>">
												<input type="text" id="price" name="price[]" class="flight form-control" placeholder="Enter Flight Price" value="<?php if(isset($flightPhotodata[$i]['price']))echo  $flightPhotodata[$i]['price']; ?>" required>
												<div class="col-md-4 m-b-20" ><img src="<?php echo base_url() ?>uploads/images/<?php echo  $flightPhotodata[$i]['flight_image']; ?>" height="120px" width="180px"/></div>
												</div>
												<span onclick="javasctipt: removeflight(<?php echo  $flightPhotodata[$i]['package_id']; ?>,<?php echo $flightPhotodata[$i]['id']; ?>)"><i class="fa fa-trash trash-icon pull-right" title="Remove" style="font-size:24px;margin-left: 20px;"></i></span> 
											 <?php } ?>
											</div>

										</div>
									<?php } ?>
								
									<div class="inner_slider_data">
										<?php if(!isset($Package[0]->id) && empty($Package[0]->id)) {	?>
										<div class="form-group m-b-20">
											<label class="col-md-2 control-label">Flight Image 1:</label>
											<div class="col-md-6">
												<input type="file" id="flight_image1" name="flight_image[]" class="filestyle"  data-iconname="fa fa-cloud-upload" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);" >
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-2 control-label" for="itnTitle">Flight Price 1:<span class="text-danger"></span></label>
											<div class="col-md-6">
												<input type="text" id="price" name="price[]" class="flight form-control" placeholder="Enter Flight Price" required>
											</div>
										</div>

										<?php }	?>
									</div>

								</div>
								<div class="form-group m-b-20">
									<div class="col-md-8">
										<div class="btn btn-primary add-new-btn pull-right" onclick="addrow();" > Add New Hotel</div>
										
									</div>
								</div>
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Dipature City:<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="depatureCity" name="depatureCity" class="form-control" 
											value="<?php echo isset($Itn[0]->depatureCity) ? set_value("depatureCity", $Itn[0]->depatureCity) : set_value("depatureCity"); ?>" required>
										</div>
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Package Price:<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="packagePrice" name="packagePrice" class="form-control" 
											value="<?php echo isset($Itn[0]->packagePrice) ? set_value("packagePrice", $Itn[0]->packagePrice) : set_value("packagePrice"); ?>" required>
										</div>
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Amount Per Person:</label>
										<div class="col-md-6"> 
											<input type="text" id="amountPerPerson" name="amountPerPerson" class="form-control" 
											value="<?php echo isset($Itn[0]->amountPerPerson) ? set_value("amountPerPerson", $Itn[0]->amountPerPerson) : set_value("amountPerPerson"); ?>">
										</div>
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Amount Per Child:</label>
										<div class="col-md-6"> 
											<input type="text" id="amountPerChild" name="amountPerChild" class="form-control" 
											value="<?php echo isset($Itn[0]->amountPerChild) ? set_value("amountPerChild", $Itn[0]->amountPerChild) : set_value("amountPerChild"); ?>">
										</div>
									</div>
									
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Package Inclusions :<span class="text-danger">*</span></label>
										<div class="col-md-6">
											<textarea name="packageInclusion" id="packageInclusion" required parsley-trigger="change" cols="88" rows="5"><?php echo isset($Itn[0]->packageInclusion) ? set_value("packageInclusion", $Itn[0]->packageInclusion) : set_value("packageInclusion"); ?>
											</textarea>
											<?php if( form_error('packageInclusion') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('packageInclusion'); ?></li></ul>
											<?php } ?>
										</div>	
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Photo Galary:</label>
										<div class="col-md-6">
											<input type="file" name="multiplepackagePhoto[]" multiple class="filestyle <?php if( form_error('multiplepackagePhoto') != '') { ?> parsley-error" <?php } ?>  data-iconname="fa fa-cloud-upload" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);" >
											<?php if( form_error('multiplepackagePhoto') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('multiplehotelPhoto'); ?></li></ul>
											<?php } ?>
										</div>
									</div>
									<?php if(isset($GalaryPhotodata) && !empty($GalaryPhotodata)){ ?>
											<div class="col-md-12" id="image_list" style="margin-bottom:20px">
											<label class="col-md-2 control-label"></label>
											<div class="col-md-6">
											<?php	
											 for($i = 0; $i < count($GalaryPhotodata); $i++){ ?>
												<div class="col-md-4 m-b-20" id="img_div_<?php echo $GalaryPhotodata[$i]['id']?>"><img src="<?php echo base_url() ?>uploads/images/<?php echo  $GalaryPhotodata[$i]['image']; ?>" height="120px" width="180px"/><span onclick="javasctipt: removethis(<?php echo  $GalaryPhotodata[$i]['packageId']; ?>,<?php echo $GalaryPhotodata[$i]['id']; ?>)"><i class="fa fa-trash trash-icon pull-right" title="Remove" ></i></span></div>
											 <?php } ?>
											</div> 
											</div>
										<?php } ?>
									
										<hr />
									<div class="form-group p-20">
										<label class="col-md-1 control-label"></label>
										
										<div class="col-md-11">
										 <button type="submit" class="btn w-sm btn-default waves-effect waves-light">Save</button>

										 <button type="button" class="btn w-sm btn-white waves-effect"><a href="<?php echo base_url(); ?>packageListing">Cancel</a></button>
									<?php if(!isset($Itn[0]->id)) { ?>
										<input type="hidden" id="flag" name="flag" value="" />
										<button type="button" onClick="confSubmit(this.form);" class="btn w-sm btn-default waves-effect waves-light">Save And Continue to Booking</button>
									<?php } ?>	
										</div> 
									</div>
                                </div>
							</div>
						</div>
					</form>
                </div>
            </div>

<?php $this->load->view('includes/footer', ''); ?>
<script type="text/javascript">

jQuery(document).ready(function($) {
		$('.counter').counterUp({
			delay: 100,
			time: 1200
		});
		$(".knob").knob();
		
		$('form').parsley();
		$(".slider_data").on('click','.remCF',function(){
				//alert($(this).parent().parent().attr('class'));
				$(this).parent().parent().remove();
			});
		//$('#FollowupDate').datepicker();
		//$('#travelingdate').datepicker();
		$('#datetimepicker1').datepicker();
		$('#datetimepicker2').datepicker();
		
    $('input[type="radio"]').change(function() {
    	
       if($(this).attr('class') == 'dom') {
       		$('.domestic').hide(); 
            $('.international').show(); 
            <?php if(isset($Itn[0]->state_id) && $Itn[0]->state_id > 0){ ?>
            	var state = <?php echo $Itn[0]->state_id ?>;
           <?php } ?>
           		
           //var state = <?php echo (isset($Itn[0]->state_id) && $Itn[0]->state_id > 0)?$Itn[0]->state_id:'' ?>;
	        var plan = $(this).val();
	        var package_id = '';
	        <?php if(isset($Itn[0]->id) && $Itn[0]->id > 0) { ?>
	        	var package_id = <?php echo $Itn[0]->id; ?>;
	       <?php } ?>
	        
	        if(plan){
	            $.ajax({
	                type:'POST',
	                url:'<?php echo base_url('Nightstayplan/getInternational'); ?>',
	                data:'plan='+plan+'&package_id='+package_id,
	                success:function(data){
	                	$('#state_id').html('<option value="">Select State</option>'); 
	                    var dataObj = jQuery.parseJSON(data);
	                    if(dataObj){
	                    	 
	                        $(dataObj).each(function(){
	                            var option = $('<option />');
	                            option.attr('value', this.state_id).text(this.name);
	                            <?php
	                            if(isset($Itn[0]->state_id) && $Itn[0]->state_id > 0){ ?>
	                            	 if (state == this.state_id) {
							            option.attr("selected", "selected");
							        }       
	                        <?php    }
	                           	?>
	                                
	                            $('#state_id').append(option);
	                        });
	                    }else{
	                        $('#state_id').html('<option value="">State not available</option>');
	                    }
	                }
	            }); 
	        }else{
	            $('#state_id').html('<option value="">Select country first</option>');
	            
	        }
	          
       }
       else if($(this).attr('class') == 'inter') {
            $('.domestic').show(); 
            $('.international').hide();   
        	
        	<?php if(isset($Itn[0]->country_id) && $Itn[0]->country_id > 0){ ?>
            	var country = <?php echo $Itn[0]->country_id ?>;
           <?php } ?>
           	
           	var package_id = '';
	        <?php if(isset($Itn[0]->id) && $Itn[0]->id > 0) { ?>
	        	var package_id = <?php echo $Itn[0]->id; ?>;
	       <?php } ?>

        	var plan = $(this).val();
	        if(plan){
	            $.ajax({
	                type:'POST',
	                url:'<?php echo base_url('Nightstayplan/getInternational'); ?>',
	                data:'plan='+plan+'&package_id='+package_id,
	                success:function(data){
	                    $('#country_id').html('<option value="">Select Country</option>'); 
	                    var dataObj = jQuery.parseJSON(data);
	                    if(dataObj){
	                        $(dataObj).each(function(){
	                        	var option = $('<option />');
	                            option.attr('value', this.country_id).text(this.name);
	                             <?php if(isset($Itn[0]->country_id) && $Itn[0]->country_id > 0){ ?>
	                              if (country == this.country_id) {
							            option.attr("selected", "selected");
							        }   
							        <?php } ?>        
	                            $('#country_id').append(option);
	                        });
	                    }else{
	                      $('#country_id').html('<option value="">country not available</option>');
	                    }
	                }
	            }); 
	        }else{
	            $('#country_id').html('<option value="">Select country</option>');
	         }
	                     
       }
       else {
            $('.domestic').hide();
            $('.international').hide();      
       }
   }).filter(function(checkbox) {
            return $(this).prop("checked");
        })
        .trigger("change");    // or .change();


		$('#country_id').on('change',function(){
        var countryID = $(this).val();

        //var noofNight = <?php echo (isset($Itn[0]->no_of_nights))?$Itn[0]->no_of_nights:''; ?>;
        if(countryID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('Nightstayplan/getNoofNightbycountry'); ?>',
                data:'country_id='+countryID,
                success:function(data){
                    $('#no_of_nights').html('<option value="">Select No of night</option>'); 
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.id).text(this.no_of_nights); 
                                 
                            $('#no_of_nights').append(option);
                        });
                    }else{
                        $('#no_of_nights').html('<option value="">No of night Not available</option>');
                    }
                }
            }); 
        }else{
            $('#no_of_nights').html('<option value="">Select First country</option>');
            
        }
    });
    
	$('#state_id').on('change',function(){
        var state_id = $(this).val();
        if(state_id){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('Nightstayplan/getNoofNightbystate'); ?>',
                data:'state_id='+state_id,
                success:function(data){
                    $('#no_of_nights').html('<option value="">Select No of Night</option>'); 
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.id).text(this.no_of_nights);           
                            $('#no_of_nights').append(option);
                        });
                    }else{
                        $('#no_of_nights').html('<option value="">No of night is not available</option>');
                    }
                }
            }); 
        }else{
            $('#no_of_nights').html('<option value="">Select state first</option>');
            
        }
    });
    $('#no_of_nights').on('change',function(){
        var night_id = $(this).val();
        if(night_id){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('Nightstayplan/getnightStayPlan'); ?>',
                data:'nightid='+night_id,
                success:function(data){
                    $('#nightStayPlan').html('<option value="">Select No of Night</option>'); 
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.id).text(this.night_stay);           
                            $('#nightStayPlan').append(option);
                        });
                    }else{
                        $('#nightStayPlan').html('<option value="">No of night is not available</option>');
                    }
                }
            }); 
        }else{
            $('#nightStayPlan').html('<option value="">Select state first</option>');
            
        }
    });
        $('#nightStayPlan').on('change',function(){
        var plan_id = $(this).val();
        if(plan_id){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('Nightstayplan/getIternary'); ?>',
                data:'plan_id='+plan_id,
                success:function(data){
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                        	CKEDITOR.instances['itinanrary_data'].setData(this.itinerary)	
                        });
                    }else{
                        $('#itinanrary_data').html('');
                    }
                }
            }); 
        }else{
            $('#nightStayPlan').html('');
            
        }
    });
    		

});
	/* remove image */
	function removethis(packageId, id)
	{
		if( confirm('Are you sure to delete this file') == true )
		{
			if ( packageId != '' )
			{
				$.ajax({
					url:'<?php echo base_url();?>deleteGalaryPhoto/'+packageId+'/'+id,
					method: 'post',
					data: {packageId: packageId, id:id},
					dataType: 'json',
					success: function(response){
						
						if(response.Del == 'TRUE'){
							$('#img_div_'+id).html('');
						}
					}
				});
			}
		}
	}
	function removeflight(packageId, id)
	{
		if( confirm('Are you sure to delete this flight detail') == true )
		{
			if ( packageId != '' )
			{
				$.ajax({
					url:'<?php echo base_url();?>deleteflight/'+packageId+'/'+id,
					method: 'post',
					data: {packageId: packageId, id:id},
					dataType: 'json',
					success: function(response){
						
						if(response.Del == 'TRUE'){
							$('#flightimg_div_'+id).html('');
						}
					}
				});
			}
		}
	}	
	CKEDITOR.replace( 'itinanrary_data', {
		height: 260
	} );
	CKEDITOR.replace( 'hotelDetail', {
		height: 260
	} );
		


function confSubmit(form) {
	var names = $('#firstName').val();
	var lastName = $('#lastName').val();
	var noOffAdults = $('#noOffAdults').val();
	var travelDate = $('#travelDate').val();
	var packageName = $('#packageName').val();
	var nightStayPlan = $('#nightStayPlan').val();
	//var itinanrary_data = $('#itinanrary_data').val();
	var depatureCity = $('#depatureCity').val();
	var packagePrice = $('#packagePrice').val();
	var packageInclusion = $('#packageInclusion').val();
	
	if(names == ''){
		alert("Please Enter Valid First Name");
		$('#firstName').focus();
		return false;
	}if(lastName == ''){
		alert("Please Enter Valid Last Name");
		$('#lastName').focus();
		return false;
	}if(noOffAdults == ''){
		alert("Please Enter Valid noOffAdults");
		$('#noOffAdults').focus();
		return false;
	}if(travelDate == ''){
		alert("Please Enter Valid travelDate");
		$('#travelDate').focus();
		return false;
	}if(packageName == ''){
		alert("Please Enter Valid packageName");
		$('#packageName').focus();
		return false;
	}if(nightStayPlan == ''){
		alert("Please Enter Valid nightStayPlan");
		$('#nightStayPlan').focus();
		return false;
	}if(depatureCity == ''){
		alert("Please Enter Valid depatureCity");
		$('#depatureCity').focus();
		return false;
	}if(packagePrice == ''){
		alert("Please Enter Valid packagePrice");
		$('#packagePrice').focus();
		return false;
	}if(packageInclusion == ''){
		alert("Please Enter Valid packageInclusion");
		$('#packageInclusion').focus();
		return false;
	}
if (confirm("Are you sure you want to submit the form?")) {
	document.getElementById("flag").value = "1";
form.submit();
}

else {
alert("You decided to not Continue to booking!");
}
}

	function addrow()
			{
				var num1 = eval($('.flight').length)+1;
				
				var html = '';
					
					html = html + '<div class="inner_slider_data">';
					html = html + '<div class="form-group m-b-20">';
				   
					html = html + '	<div class="form-group">';
					html = html + '	<label class="col-md-2 control-label" for="flight_image">NFlight Image'+num1+'</label>';
					html = html + '	<div class="col-md-6">';
					html = html + '	<input type="file" id="flight_image'+num1+'" name="flight_image[]" class="filestyle" >';
					html = html + '	</div>';
					html = html + '	</div>';
					html = html + '	<div class="form-group">';
					html = html + '	<label class="col-md-2 control-label" for="price">Flight Price'+num1+'</label>';
					html = html + '	<div class="col-md-6">';
					html = html + '	<input type="text" id="price'+num1+'" name="price[]" class="flight form-control" placeholder="Flight Price">';
					html = html + '	</div>';
					

					html = html + '<a class="remCF remove" title="Remove" ><i class="fa fa-trash" style="font-size:36px;"></i></a>';
					html = html + '	</div>';
					html = html + '	</div>';

					html = html + '	</div>';

					
					
				$(".slider_data").append(html);	
				
			}
 </script>