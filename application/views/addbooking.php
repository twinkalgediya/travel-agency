 <link href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"> 
<link href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.css" rel="stylesheet"> 

 <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li>
                                <h4 class="page-title">Dashboard</h4>
                            </li>
							<li>
                                <a href="<?php echo base_url(); ?>bookingListing">Booking</a>
                            </li>
                            
                            <li class="active">
                               <?php echo isset($booking[0]->id) ? 'Edit' : 'Add' ?>
                            </li>
                        </ol>
                    </div>
                </div>

				
                <div class="row">
                    <div class="col-sm-12">
					<label class="col-md-2 control-label"></div>

					<form action="<?php echo base_url() ?><?php echo isset($Booking[0]->bookingAutoId) ? 'Booking/updateBooking' : 'Booking/insertBooking' ?>" name="insertBooking" id="insertBooking" method="post" class="form-horizontal" data-parsley-validate novalidate autocomplete="off">
					
                        <div class="row">
                            <div class="col-sm-12">
							
                                <div class="card-box">
                                    
							<h5 class="text-muted text-uppercase m-t-0 m-b-20"><b>General</b></h5>
										
											<div class="row">
												<div class="col-md-12">
													<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
												</div>
											</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Customer Name :<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="firstName" name="firstName" class="form-control" 
											value="<?php echo $Booking[0]->firstName.' '.$Booking[0]->lastName; ?>" required />

				<?php if(isset($Booking[0]->bookingAutoId) && $Booking[0]->bookingAutoId > 0) {?>
								<?php if(isset($Booking[0]->bookingAutoId)) { ?>
									<input type="hidden" name="inquiry_id" id="inquiry_id" value="<?php echo $Booking[0]->inqAutoId; ?>">
								<?php } ?>
								<?php if(isset($Booking[0]->bookingAutoId)) { ?>
									<input type="hidden" name="package_id" id="package_id" value="<?php echo $Booking[0]->pacAutoId; ?>" >
								<?php } ?>
								<?php if(isset($Booking[0]->bookingAutoId)) { ?>
									<input type="hidden" name="id" id="id" value="<?php echo $Booking[0]->bookingAutoId; ?>" >
								<?php } ?>
				<?php } else { ?>
								<?php if(isset($Booking[0]->id)) { ?>
									<input type="hidden" name="inquiry_id" id="inquiry_id" value="<?php echo $Booking[0]->id; ?>">
								<?php } ?>
								<?php if(isset($Package[0]->id)) { ?>
									<input type="hidden" name="package_id" id="package_id" value="<?php echo $Package[0]->id; ?>" >
								<?php } ?>
				<?php } ?>	
										</div>
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Phone No:<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="phoneNo" name="phoneNo" class="form-control" 
											value="<?php echo $Booking[0]->phoneNo; ?>" required>
										</div>
									</div>

									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Destination:<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="destination" name="destination" class="form-control" 
											value="<?php echo $Booking[0]->destination; ?>" required>
										</div>
									</div>


									
									
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Travel Date : <span class="text-danger">*</span></label>
										<div class="col-md-6">
											<div class='input-group date' id='datetimepicker1'>
												<input type='text' name="travelDate" id="travelDate" class="form-control" value="<?php if(isset($Booking[0]->travelDate)){
													echo $Booking[0]->travelDate;
												} ?>" placeholder="Select Travel Date" required/>
													<span class="input-group-addon">
														<span class="glyphicon glyphicon-calendar"></span>
													</span>
											</div>
										</div>	
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Inquery Id :<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="inquiryID" name="inquiryID" class="form-control" disabled="disabled"
											value="<?php echo isset($Booking[0]->inquiryID) ? set_value("inquiryID", $Booking[0]->inquiryID) : set_value("inquiryID"); ?>" required>
										</div>
									</div>
					<?php if(isset($Booking[0]->bookingAutoId) && $Booking[0]->bookingAutoId > 0) {?>				
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Package Cost :<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="packagePrice" name="packagePrice" class="form-control" 
											value="<?php echo isset($Booking[0]->packagePrice) ? set_value("packagePrice", $Booking[0]->packagePrice) : set_value("packagePrice"); ?>" required>
										</div>
									</div>
					<?php } else { ?>
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Package Cost :<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="packagePrice" name="packagePrice" class="form-control" 
											value="<?php echo isset($Package[0]->packagePrice) ? set_value("packagePrice", $Package[0]->packagePrice) : set_value("packagePrice"); ?>" required>
										</div>
									</div>
					<?php } ?>				
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Total Amount Received :<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="number" id="totalAmtReceve" name="totalAmtReceve" number class="form-control" 
											value="<?php echo isset($Booking[0]->totalAmtReceve) ? set_value("totalAmtReceve", $Booking[0]->totalAmtReceve) : set_value("totalAmtReceve"); ?>" required>
										</div>
									</div>
				<div class="slider_data">
								
					<?php if(isset($HotelDetail)){ 
						$k = 1;	
					?>
					<?php for($i=0; $i<count($HotelDetail); $i++) { ?>
						<div class="inner_slider_data">
								<div class="form-group m-b-20">
									<label class="col-md-2 control-label">Countries<?php echo $k; ?> :<span class="text-danger">*</span></label>
									<div class="col-md-6"> 
										<select class="country form-control" name="countryId[]" id="country<?php echo $k; ?>">
								      		<option value="">Select Country</option>
								        	<?php foreach($countries as $count): ?>
								        	<option value="<?php echo $count->id; ?>" <?php if (isset($HotelDetail[$i]->countryId) && $HotelDetail[$i]->countryId == $count->id) echo 'selected' ; ?>><?php echo $count->name; ?></option>
								         <?php endforeach; ?> 
								      	</select>
								      	<?php if( form_error('countryId') != '') { ?>
											<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('countryId'); ?></li></ul>
										<?php } ?>
								    </div>
								</div>

								<div class="form-group m-b-20">
									<label class="col-md-2 control-label">State<?php echo $k; ?> :<span class="text-danger">*</span></label>
									<div class="col-md-6"> 
										<select class="state form-control" name="stateId[]" id="state<?php echo $k; ?>">
								      		<option value="">Select State</option>
								        	<?php foreach($state as $count): ?>
								        	<option value="<?php echo $count->id; ?>" <?php if (isset($HotelDetail[$i]->stateId) && $HotelDetail[$i]->stateId == $count->id) echo 'selected' ; ?>><?php echo $count->name; ?></option>
								         <?php endforeach; ?> 
								      	</select>
								      	<?php if( form_error('stateId') != '') { ?>
											<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('stateId'); ?></li></ul>
										<?php } ?>
								    </div>
								</div>

								<div class="form-group m-b-20">
									<label class="col-md-2 control-label">City<?php echo $k; ?> :<span class="text-danger">*</span></label>
									<div class="col-md-6"> 
										<select class="state form-control" name="cityId[]" id="city<?php echo $k; ?>">
								      		<option value="">Select City</option>
								        	<?php foreach($cities as $count): ?>
								        	<option value="<?php echo $count->id; ?>" <?php if (isset($HotelDetail[$i]->cityId) && $HotelDetail[$i]->cityId == $count->id) echo 'selected' ; ?>><?php echo $count->name; ?></option>
								         <?php endforeach; ?> 
								      	</select>
								      	<?php if( form_error('cityId') != '') { ?>
											<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('cityId'); ?></li></ul>
										<?php } ?>
								    </div>
								</div>

								<div class="form-group m-b-20">
									<label class="col-md-2 control-label">Hotel<?php echo $k; ?> :<span class="text-danger">*</span></label>
									<div class="col-md-6"> 
										<select class="hotel form-control" name="hotelId[]" id="hotel<?php echo $k; ?>">
								      		<option value="">Select Hotel</option>
								        	<?php foreach($Hotel as $count): ?>
								        	<option value="<?php echo $count->id; ?>" <?php if (isset($HotelDetail[$i]->hotelId) && $HotelDetail[$i]->hotelId == $count->id) echo 'selected' ; ?>><?php echo $count->hotelName; ?></option>
								         <?php endforeach; ?> 
								      	</select>
								      	<?php if( form_error('hotelId') != '') { ?>
											<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('hotelId'); ?></li></ul>
										<?php } ?>
								    </div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label" for="checkindate">Check in Due Date<?php echo $k; ?></label>
									<div class="col-md-6">
										<div class="input-group" id="checkindate<?php echo $k; ?>">
											<input type="text" id="checkindates<?php echo $k; ?>" name="checkInDate[]" value="<?php echo $HotelDetail[$i]->checkInDate ?>" class="dates form-control" placeholder="Partial Payment Due Date">
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label" for="checkoutdate">Check Out Date<?php echo $k; ?></label>
									<div class="col-md-6">
										<div class="input-group" id="checkoutdate<?php echo $k; ?>">
											<input type="text" id="checkoutdates<?php echo $k; ?>" name="checkOutDate[]" value="<?php echo $HotelDetail[$i]->checkOutDate ?>" class="dates form-control" placeholder="Partial Payment Due Date">
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label" for="extraroom">No Of Extra Room<?php echo $k; ?></label>		
									<div class="col-md-6">
										<input type="text" id="extraroom<?php echo $k; ?>" name="noOfRoom[]" class="form-control" value="<?php echo isset($HotelDetail[$i]->noOfRoom) ? set_value("noOfRoom", $HotelDetail[$i]->noOfRoom) : set_value("noOfRoom"); ?>" placeholder="No Of Extra Room">
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label" for="extrbed">No Of Extra Bed<?php echo $k; ?></label>
									<div class="col-md-6">
										<input type="text" id="extrbed<?php echo $k; ?>" name="extraBed[]" class="form-control" value="<?php echo isset($HotelDetail[$i]->extraBed) ? set_value("extraBed", $HotelDetail[$i]->extraBed) : set_value("extraBed"); ?>" placeholder="No Of Extra Bed">
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label" for="roomType">Room Type<?php echo $k; ?></label>
									<div class="col-md-6">
										<input type="text" id="roomtype<?php echo $k; ?>" name="roomType[]" class="form-control" value="<?php echo isset($HotelDetail[$i]->roomType) ? set_value("roomType", $HotelDetail[$i]->roomType) : set_value("roomType"); ?>" placeholder="Room Type">
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label" for="mealPlan">Meal Plan<?php echo $k; ?></label>
									<div class="col-md-6">
										<input type="text" id="mealplan<?php echo $k; ?>" name="mealPlan[]" class="form-control" value="<?php echo isset($HotelDetail[$i]->mealPlan) ? set_value("mealPlan", $HotelDetail[$i]->mealPlan) : set_value("mealPlan"); ?>" placeholder="Meal Plan">
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label" for="extrainclusion">Extra Inclusion<?php echo $k; ?></label>
									<div class="col-md-6">
										<textarea type="text" id="extrainclusion<?php echo $k; ?>" name="extraInclusion[]" class="form-control smart_editor"placeholder="Extra Inclusion"><?php echo isset($HotelDetail[$i]->extraInclusion) ? set_value("extraInclusion", $HotelDetail[$i]->extraInclusion) : set_value("extraInclusion"); ?></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label" for="netpayable">Net Payable To Hotel<?php echo $k; ?></label>
									<div class="col-md-6">
										<input type="text" id="netpayable<?php echo $k; ?>" name="netPayToHotel[]" class="form-control" value="<?php echo isset($HotelDetail[$i]->netPayToHotel) ? set_value("netPayToHotel", $HotelDetail[$i]->netPayToHotel) : set_value("netPayToHotel"); ?>" placeholder="Net Payable To this Hotel Amount">
									</div>
								</div>
					

								<div class="form-group">
							 		<label class="col-md-2 control-label" for="reamraks">Remarks<?php echo $k; ?></label>
									<div class="col-md-6">
										<textarea type="text" id="reamraks<?php echo $k; ?>" name="remarks[]" class="form-control smart_editor" placeholder="Remarks"><?php echo isset($HotelDetail[$i]->remarks) ? set_value("remarks", $HotelDetail[$i]->remarks) : set_value("remarks"); ?></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label" for="paymentduedate">Payment Due Date<?php echo $k; ?></label>
									<div class="col-md-6">
										<div class="input-group" id="paymentduedate<?php echo $k; ?>">
											<input type="text" id="paymentduedates<?php echo $k; ?>" name="paymentDueDate[]" value="<?php echo $HotelDetail[$i]->paymentDueDate; ?>" class="dates form-control" placeholder="Payment Due Date" />
											<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>
									</div>
								</div>


								<div class="form-group">
									<label class="col-md-2 control-label" for="partialpaymentamount">Partial Payment Amount<?php echo $k; ?></label>
									<div class="col-md-6">
										<input type="text" id="partialpaymentamount<?php echo $k; ?>" name="partialPayAmount[]" value="<?php echo $HotelDetail[$i]->partialPayAmount; ?>" class="form-control" placeholder="Partial Payment Amount">
									</div>
								</div>
					
								<div class="form-group">
									<label class="col-md-2 control-label" for="partialpaymentduedate">Partial Payment Due Date<?php echo $k; ?></label>
									<div class="col-md-6">
										<div class="input-group" id="partialpaymentduedate<?php echo $k; ?>">
											<input type="text" id="partialpaymentduedates<?php echo $k; ?>" name="partialPayDueDate[]" value="<?php echo $HotelDetail[$i]->partialPayDueDate; ?>" class="dates form-control" placeholder="Partial Payment Due Date">
											<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>
									</div>
									<?php if($i >0 ) { ?>
										<a class="remCF remove" title="Remove" ><i class="fa fa-trash" style="font-size:36px;"></i></a>
									<?php } ?>	
								</div>
						</div>
						<?php $k ++; } ?>
						<?php } else { ?>
							<div class="inner_slider_data">	
								<div class="form-group m-b-20">
									<label class="col-md-2 control-label">Countries :<span class="text-danger">*</span></label>
									<div class="col-md-6"> 
										<select class="country form-control" name="countryId[]" id="country1">
											<option value="">Select Country</option>
											<?php foreach($countries as $count): ?>
											<option value="<?php echo $count->id; ?>"><?php echo $count->name; ?></option>
											<?php endforeach; ?> 
										</select>
								    </div>
								</div>	


								<!-- State dropdown -->
								<div class="form-group m-b-20">
									<label class="col-md-2 control-label">State 1:<span class="text-danger">*</span></label>
									<div class="col-md-6">
										<select id="state1" class="state form-control" name="stateId[]" >
											<option value="">Select country first</option>
										</select>
									</div>
								</div>
									<!-- City dropdown -->
								<div class="form-group m-b-20">
									<label class="col-md-2 control-label">Cities 1:<span class="text-danger">*</span></label>
										<div class="col-md-6">
											<select id="city1" class="city form-control" name="cityId[]">
												<option value="">Select state first</option>
											</select>
										</div>
								</div>

								<div class="form-group m-b-20">
									<label class="col-md-2 control-label">Hotel:<span class="text-danger">*</span></label>
										<div class="col-md-6">
											<select id="hotel1" class="hotel form-control" name="hotelId[]">
												<option value="">Select hotel</option>
											</select>
										</div>
								</div>
				
								<div class="form-group">
								<label class="col-md-2 control-label" for="checkindate">Check in Due Date1</label>
									<div class="col-md-6">
										<div class="input-group" id="checkindate1">
											<input type="text" id="checkindates1" name="checkInDate[]" class="dates form-control" placeholder="Partial Payment Due Date">
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>
									</div>
								</div>


							    <div class="form-group">
									<label class="col-md-2 control-label" for="checkoutdate">Check Out Date1</label>
									<div class="col-md-6">
										<div class="input-group" id="checkoutdate1">
											<input type="text" id="checkoutdates1" name="checkOutDate[]" class="dates form-control" placeholder="Partial Payment Due Date">
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>
									</div>
								</div>


								<div class="form-group">
									<label class="col-md-2 control-label" for="extraroom">No Of Extra Room1</label>		
									<div class="col-md-6">
										<input type="text" id="extraroom1" name="noOfRoom[]" class="form-control" placeholder="No Of Extra Room">
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label" for="extrbed">No Of Extra Bed1</label>
										<div class="col-md-6">
											<input type="text" id="extrbed1" name="extraBed[]" class="form-control" placeholder="No Of Extra Bed">
										</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label" for="roomType">Room Type1</label>
										<div class="col-md-6">
											<input type="text" id="roomtype1" name="roomType[]" class="form-control" placeholder="Room Type">
										</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label" for="mealPlan">Meal Plan1</label>
										<div class="col-md-6">
											<input type="text" id="mealplan1" name="mealPlan[]" class="form-control" placeholder="Meal Plan">
										</div>
								</div>
					
					
								<div class="form-group">
									<label class="col-md-2 control-label" for="extrainclusion">Extra Inclusion1</label>
										<div class="col-md-6">
											<textarea type="text" id="extrainclusion1" name="extraInclusion[]" class="form-control smart_editor"placeholder="Extra Inclusion"></textarea>
										</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label" for="netpayable">Net Payable To Hotel1</label>
									<div class="col-md-6">
										<input type="text" id="netpayable1" name="netPayToHotel[]" class="form-control" placeholder="Net Payable To this Hotel Amount">
									</div>
								</div>
					

								<div class="form-group">
									<label class="col-md-2 control-label" for="reamraks">Remarks1</label>
									<div class="col-md-6">
										<textarea type="text" id="reamraks1" name="remarks[]" class="form-control smart_editor" placeholder="Remarks"></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label" for="paymentduedate">Payment Due Date1</label>
									<div class="col-md-6">
										<div class="input-group" id="paymentduedate1">
											<input type="text" id="paymentduedates1" name="paymentDueDate[]" class="dates form-control" placeholder="Payment Due Date">
											<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>
									</div>
								</div>


								<div class="form-group">
									<label class="col-md-2 control-label" for="partialpaymentamount">Partial Payment Amount1</label>
										<div class="col-md-6">
											<input type="text" id="partialpaymentamount1" name="partialPayAmount[]" class="form-control" placeholder="Partial Payment Amount">
										</div>
								</div>
					
								<div class="form-group">
									<label class="col-md-2 control-label" for="partialpaymentduedate">Partial Payment Due Date1</label>
									<div class="col-md-6">
										<div class="input-group" id="partialpaymentduedate1">
											<input type="text" id="partialpaymentduedates1" name="partialPayDueDate[]" class="dates form-control" placeholder="Partial Payment Due Date">
											<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>
									</div>
								</div>
							</div>
						<?php	} ?>
					</div>
					
					
				<div class="form-group m-b-20">
					<div class="col-md-8">
						<div class="btn btn-primary add-new-btn pull-right" onclick="addrow();" > Add New Hotel</div>
						
					</div>
				</div>
					<hr />
						<div class="form-group p-20">
							<label class="col-md-1 control-label"></label>
							
							<div class="col-md-11">
							 <button type="submit" class="btn w-sm btn-default waves-effect waves-light">Save</button>
							 <button type="button" class="btn w-sm btn-white waves-effect"><a href="<?php echo base_url(); ?>bookingListing">Cancel</a></button>
							</div> 
						</div>
                </div>
				</div>	
		</div>	
	</form>
		
	</div>


<?php $this->load->view('includes/footer', ''); ?>
 <script src="<?php echo base_url(); ?>assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script type="text/javascript">

jQuery(document).ready(function($) {
		$('.counter').counterUp({
			delay: 100,
			time: 1200
		});
		$(".knob").knob();
		
		$('form').parsley();
		//$('#FollowupDate').datepicker();
		//$('#travelingdate').datepicker();
		$('#datetimepicker1').datepicker();
		$('#datetimepicker2').datepicker();
		$('.dates').datepicker();
        $(".selectmultiple").select2();

        $(".slider_data").on('click','.remCF',function(){
				//alert($(this).parent().parent().attr('class'));
				$(this).parent().parent().remove();
			});


       $(document).on('change','.country',function(e,item){
        var state=$(this).parent().parent().next().find('.state').attr("id");
        
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('booking/getStates'); ?>',
                data:'country_id='+countryID,
                success:function(data){
                    $('#'+state).html('<option value="">Select State</option>'); 
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.id).text(this.name);           
                            $('#'+state).append(option);
                        });
                    }else{
                        $('#'+state).html('<option value="">State not available</option>');
                    }
                }
            }); 
        }else{
            $('#'+state).html('<option value="">Select country first</option>');
            
        }
    });
    
    /* Populate data to city dropdown */
    $(document).on('change','.state',function(e,item){

    	var city=$(this).parent().parent().next().find('.city').attr("id");
    	
    	var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('booking/getCities'); ?>',
                data:'state_id='+stateID,
                success:function(data){
                    $('#'+city).html('<option value="">Select City</option>'); 
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.id).text(this.name);           
                            $('#'+city).append(option);
                        });
                    }else{
                        $('#'+city).html('<option value="">City not available</option>');
                    }
                }
            }); 
        }else{
            $('#'+city).html('<option value="">Select state first</option>'); 
        }
    });

     $(document).on('change','.city',function(e,item){
        var city_id = $(this).val();
		var state=$(this).parent().parent().prev().find('.state').attr("id");
		var state_id = $('#'+state).val();
        var hotel=$(this).parent().parent().next().find('.hotel').attr("id");
        
        if(city_id){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('booking/getHotel'); ?>',
                data:'city_id='+city_id+'&state_id='+state_id,
                success:function(data){
                    $('#hotel').html('<option value="">Select hotel</option>'); 
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.id).text(this.hotelName);           
                            $('#'+hotel).append(option);
                        });
                    }else{
                        $('#'+hotel).html('<option value="">State not available</option>');
                    }
                }
            }); 
        }else{
            $('#'+hotel).html('<option value="">Select city first</option>');
            }
    });
    
  $("#city").select2();
    });

	function addrow()
			{
				var num1 = eval($('.hotel').length)+1;
				
				var html = '';
					
					html = html + '<div class="inner_slider_data">';
					html = html + '<div class="form-group m-b-20">';
				    html = html + '<label class="col-md-2 control-label">Countries'+num1+' :<span class="text-danger">*</span></label>';
					html = html + '<div class="col-md-6">'; 
					html = html + '<select class="country form-control" name="countryId[]" id="country'+num1+'">';
					html = html + '<option value="">Select Country</option>';
					html = html + '<?php foreach($countries as $count): ?>';
					html = html + '<option value=<?php echo $count->id; ?>><?php echo $count->name; ?></option>';
					html = html + '<?php endforeach; ?>'; 
					html = html + '</select>';
					html = html + '</div>';
					html = html + '	</div>';

					html = html + '<div class="form-group m-b-20">';
					html = html + '<label class="col-md-2 control-label">State'+num1+' :<span class="text-danger">*</span></label>';
					html = html + '<div class="col-md-6">';
					html = html + '<select id="state'+num1+'" class="state form-control" name="stateId[]" >';
					html = html + '<option value="">Select country first</option>';
					html = html + '</select>';
					html = html + '</div>';
				    html = html + '</div>';
					html = html + '	<div class="form-group m-b-20">';
html = html + '	<label class="col-md-2 control-label">Cities'+num1+':<span class="text-danger">*</span></label>';
					html = html + '	<div class="col-md-6">';
					html = html + '<select id="city'+num1+'" class="city form-control" name="cityId[]">';
					html = html + '<option value="">Select state first</option>';
					html = html + '</select>';
					html = html + '</div>';
					html = html + '</div>';
					html = html + '<div class="form-group m-b-20">';
					html = html + '<label class="col-md-2 control-label">Hotel'+num1+'';
					html = html + ':<span class="text-danger">*</span></label>';
					html = html + '<div class="col-md-6">';
					html = html + '<select id="hotel'+num1+'" class="hotel form-control" name="hotelId[]">';
					html = html + '<option value="">Select hotel</option>';
					html = html + '</select>';
					html = html + '</div>';
				    html = html + '</div>';
									
					html = html + '	<div class="form-group">';
					html = html + '	<label class="col-md-2 control-label" for="checkindate">Check in Due Date'+num1+'</label>';
					html = html + '	<div class="col-md-6">';
					html = html + '	<div class="input-group" id="checkindate'+num1+'">';
					html = html + '	<input type="text" id="checkindates'+num1+'" name="checkInDate[]" class="dates form-control" placeholder="Partial Payment Due Date">';
					html = html + '<span class="input-group-addon">';
					html = html + '<span class="glyphicon glyphicon-calendar"></span>';
					html = html + '</span>';
					html = html + '</div>';
					html = html + '	</div>';
					html = html + '	</div>';


					html = html + '	<div class="form-group">';
					html = html + '	<label class="col-md-2 control-label" for="checkoutdate">Check Out Date'+num1+'</label>';
					html = html + '	<div class="col-md-6">';
					html = html + '	<div class="input-group" id="checkoutdate'+num1+'">';
					html = html + '	<input type="text" id="checkoutdates'+num1+'" name="checkOutDate[]" class="dates form-control" placeholder="Partial Payment Due Date">';
					html = html + '<span class="input-group-addon">';
					html = html + '<span class="glyphicon glyphicon-calendar"></span>';
					html = html + '</span>';
					html = html + '</div>';
					html = html + '	</div>';
					html = html + '	</div>';


					html = html + '	<div class="form-group">';
					html = html + '	<label class="col-md-2 control-label" for="extraroom">No Of Extra Room'+num1+'</label>';
					html = html + '	<div class="col-md-6">';
					html = html + '	<input type="text" id="extraroom'+num1+'" name="noOfRoom[]" class="form-control" placeholder="Extra Room">';
					html = html + '	</div>';
					html = html + '	</div>';
						
					html = html + '	<div class="form-group">';
					html = html + '	<label class="col-md-2 control-label" for="extrbed">No Of Extra Bed'+num1+'</label>';
					html = html + '	<div class="col-md-6">';
					html = html + '	<input type="text" id="extrbed'+num1+'" name="extraBed[]" class="form-control" placeholder="Extra Bed">';
					html = html + '	</div>';
					html = html + '	</div>';

					html = html + '	<div class="form-group">';
					html = html + '	<label class="col-md-2 control-label" for="roomType">Room Type'+num1+'</label>';
					html = html + '	<div class="col-md-6">';
					html = html + '	<input type="text" id="roomtype'+num1+'" name="roomType[]" class="form-control" placeholder="Room Type">';
					html = html + '	</div>';
					html = html + '	</div>';

					html = html + '	<div class="form-group">';
					html = html + '	<label class="col-md-2 control-label" for="mealPlan">Meal Plan'+num1+'</label>';
					html = html + '	<div class="col-md-6">';
					html = html + '	<input type="text" id="mealplan'+num1+'" name="mealPlan[]" class="form-control" placeholder="Meal Plan">';
					html = html + '	</div>';
					html = html + '	</div>';
					
					
					html = html + '	<div class="form-group">';
					html = html + '	<label class="col-md-2 control-label" for="extrainclusion">Extra Inclusion'+num1+'</label>';
					html = html + '	<div class="col-md-6">';
					html = html + '	<textarea type="text" id="extrainclusion'+num1+'" name="extraInclusion[]" class="form-control smart_editor" 	placeholder="Ex. Extra Inclusion"></textarea>';
					html = html + '	</div>';
					html = html + '	</div>';

					html = html + '	<div class="form-group">';
					html = html + '	<label class="col-md-2 control-label" for="netpayable">Net Payable To Hotel'+num1+'</label>';
					html = html + '	<div class="col-md-6">';
					html = html + '	<input type="text" id="netpayable'+num1+'" name="netPayToHotel[]" class="form-control" placeholder="Net Payable To this Hotel Amount">';
					html = html + '	</div>';
					html = html + '	</div>';
					

					html = html + '	<div class="form-group">';
					html = html + '	<label class="col-md-2 control-label" for="reamraks">Remarks'+num1+'</label>';
					html = html + '	<div class="col-md-6">';
					html = html + '	<textarea type="text" id="reamraks'+num1+'" name="remarks[]" class="form-control smart_editor" placeholder="Remarks"></textarea>';
					html = html + '	</div>';
					html = html + '	</div>';

					html = html + '	<div class="form-group">';
					html = html + '	<label class="col-md-2 control-label" for="paymentduedate">Payment Due Date'+num1+'</label>';
					html = html + '	<div class="col-md-6">';
					html = html + '	<div class="input-group" id="paymentduedate'+num1+'">';
					html = html + '	<input type="text" id="paymentduedates'+num1+'" name="paymentDueDate[]" class="dates form-control" placeholder="Payment Due Date">';
					html = html + '<span class="input-group-addon">';
					html = html + '<span class="glyphicon glyphicon-calendar"></span>';
					html = html + '</span>';
					html = html + '</div>';
					html = html + '	</div>';
					html = html + '	</div>';
					
					/*html = html + '	<div class="form-group">';
					html = html + '	<label class="col-md-2 control-label" for="paymentduedate">Payment Due Date'+num1+'</label>';
					html = html + '	<div class="col-md-6">';
					html = html + '	<div class="dates input-group" id="paymentduedate">';
					html = html + '	<input type="text" id="paymentduedates'+num1+'" name="paymentDueDate[]" class="form-control" placeholder="Payment Due Date" />';
					html = html + '<span class="input-group-addon">';
					html = html + '<span class="glyphicon glyphicon-calendar"></span>';
					html = html + '</span>';
					html = html + '</div>';
					html = html + '	</div>';
					html = html + '	</div>';*/


					html = html + '	<div class="form-group">';
					html = html + '	<label class="col-md-2 control-label" for="partialpaymentamount">Partial Payment Amount'+num1+'</label>';
					html = html + '	<div class="col-md-6">';
					html = html + '	<input type="text" id="partialpaymentamount'+num1+'" name="partialPayAmount[]" class="form-control" placeholder="Partial Payment Amount">';
					html = html + '	</div>';
					html = html + '	</div>';
					
					html = html + '	<div class="form-group">';
					html = html + '	<label class="col-md-2 control-label" for="partialpaymentduedate">Payment Due Date'+num1+'</label>';
					html = html + '	<div class="col-md-6">';
					html = html + '	<div class="input-group" id="partialpaymentduedate">';
					html = html + '	<input type="text" id="partialpaymentduedates'+num1+'" name="partialPayDueDate[]" class="dates form-control" placeholder="Partial Payment Due Date">';
					html = html + '<span class="input-group-addon">';
					html = html + '<span class="glyphicon glyphicon-calendar"></span>';
					html = html + '</span>';
					html = html + '</div>';
					html = html + '	</div>';
					html = html + '<a class="remCF remove" title="Remove" ><i class="fa fa-trash" style="font-size:36px;"></i></a>';
					html = html + '	</div>';

					html = html + '	</div>';

					
					
				$(".slider_data").append(html);	
				$('.dates').removeClass('hasDatepicker');
				$('.dates').datepicker();
			}
 </script>

