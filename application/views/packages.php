
<!-- DataTables -->
<link href="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">

<!-- Page-Title -->
<div class="row">
	<div class="col-sm-12">
		<ol class="breadcrumb">
			<li>
				<a href="<?php echo base_url(); ?>"><h4 class="page-title">Dashboard</h4></a>
			</li>
			<li>
				<a href="<?php echo base_url(); ?>packageListing">Package</a>
			</li>
			<li class="active">
				List
			</li>
		</ol>
	</div>
</div>
<div class="row">
<div class="col-sm-12">
	<div class="card-box table-responsive">
		<div class="btn-group pull-right m-t-15 m-b-15">
		<?php if($chID != 'null' && $chID >0) { ?>
			<a class="btn btn-default dropdown-toggle waves-effect waves-light" href="<?php echo base_url(); ?>addPackage/<?php echo $packageRecords[0]->inquiryId; ?>"><span class="m-l-5"><i class="md md-add-circle"></i></span> Add New </a>
		<?php } ?>	
		</div>
		<h4 class="m-t-0 m-b-15 header-title"><b>Package List</b></h4>
		
		<div class="col-md-12">	
		<?php
			$error = $this->session->flashdata('error');
			if($error){ ?>
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<?php echo $this->session->flashdata('error'); ?>
			</div>
			<?php } ?>
			<?php  
			$success = $this->session->flashdata('success');
			if($success)
			{ ?>
			<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<?php echo $this->session->flashdata('success'); ?>
			</div>
		<?php } ?>
		</div>
			 
			  
    		
		<table id="datatable-responsive"
			   class="table table-striped table-bordered dt-responsive nowrap package_table" cellspacing="0"
			   width="100%">
			<thead>
				<tr>
					<th>InquiryId</th>
					<th>packageName</th>
					<th>depatureCity</th>
					<th>packagePrice</th>
					<th>nightStayPlan</th>
					<th>action</th>
				</tr>
			</thead>
			<tbody>
				<?php if(!empty($packageRecords)){
					foreach($packageRecords as  $value) { ?>		
						<tr>
							<td>
								<?php echo $value->inquiryId ?>
							</td>
						
							<td>
								<?php echo $value->packageName ?>
							</td>
							<td>
								<?php echo $value->depatureCity ?>
							</td>
							<td>
								<?php echo $value->packagePrice ?>
							</td>
							<td>
								<?php echo $value->nightStayPlan ?>
							</td>
							<td class="text-center">
								<a class="btn btn-sm btn-success" href="<?php echo base_url().'editPackage/'.$value->id; ?>" title="Edit">
									<i class="fa fa-pencil"></i>
								</a>
								<a class="btn btn-sm btn-success" href="<?php echo base_url().'createPdf/'.$value->id; ?>" title="PDF">
									<i class="fa fa-file-pdf-o"></i>
								</a>
							</td>
						</tr>
                    <?php }
                } ?>
			</tbody>
		</table>
	</div>
</div>
</div>
<?php $this->load->view('includes/footer', ''); ?>
<script type="text/javascript">

jQuery(document).ready(function($) {
		$('.counter').counterUp({
			delay: 100,
			time: 1200
		});
		$(".knob").knob();
		
		$('form').parsley();
		$('.package_table').DataTable({
			"lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]]
		});
});
		
</script>