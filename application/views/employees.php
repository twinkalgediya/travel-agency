
<!-- DataTables -->
<link href="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>

<!-- Page-Title -->
<div class="row">
	<div class="col-sm-12">
		<ol class="breadcrumb">
			<li>
				<a href="<?php echo base_url(); ?>"><h4 class="page-title">Dashboard</h4></a>
			</li>
			<li>
				<a href="<?php echo base_url(); ?>employeeListing">Employee</a>
			</li>
			<li class="active">
				List
			</li>
		</ol>
	</div>
</div>
<div class="row">
<div class="col-sm-12">
	<div class="card-box table-responsive">
		<div class="btn-group pull-right m-t-0">
			<a class="btn btn-default dropdown-toggle waves-effect waves-light" href="<?php echo base_url(); ?>addEmployee"><span class="m-l-5"><i class="md md-add-circle"></i></span> Add New </a>

		</div>
		<h4 class="m-t-0 m-b-30 header-title"><b>Employee List</b></h4>
		<?php
			$error = $this->session->flashdata('error');
			if($error){ ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                    <?php } ?>
                    <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    { ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                    <?php } ?>
		<table id="datatable-responsive"
			   class="table table-striped table-bordered dt-responsive emp_table nowrap" cellspacing="0"
			   width="100%">
			<thead>
				<tr>
					<th>Email Id</th>
					<th>Name</th>
					<th>phoneNo</th>
					<th>Role</th>
					<th>action</th>
				</tr>
			</thead>
			<tbody>
				<?php if(!empty($employeeRecords)){
					foreach($employeeRecords as  $value) { ?>		
						<tr>
							<td>
								<?php echo $value->email; ?>
							</td>
							<td>
								<?php echo $value->name; ?>
							</td>
							
							<td>
								<?php echo $value->mobile; ?>
							</td>
							
							<td>
								<?php echo $value->role; ?>
							</td>
							
							<td class="text-center">
								 <a class="btn btn-sm btn-success" href="<?php echo base_url().'editEmployee/'.$value->userId; ?>" title="Edit">
									<i class="fa fa-pencil"></i>
								</a>
								<a class="btn btn-sm btn-danger" href="<?php echo base_url().'deleteEmployee/'.$value->userId; ?>" title="Delete" data-userid="<?php echo $value->userId; ?>"
								title="Sil">
								<i class="fa fa-trash"></i>
								</a>
							</td>
						</tr>
                    <?php }
                } ?>
			</tbody>
		</table>
	</div>
</div>
</div>

<?php $this->load->view('includes/footer', ''); ?>
<script type="text/javascript">

jQuery(document).ready(function($) {
		$('.counter').counterUp({
			delay: 100,
			time: 1200
		});
		$(".knob").knob();
		
		$('form').parsley();
		$('.emp_table').DataTable({
			"lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]]
		});
});
		
</script>