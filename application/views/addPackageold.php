<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet"> 
<link href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.css" rel="stylesheet"> 

 <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li>
                                <h4 class="page-title">Dashboard</h4>
                            </li>
							<li>
                                <a href="<?php echo base_url(); ?>packageListing">Package</a>
                            </li>
                            
                            <li class="active">
                               <?php echo isset($Inquiry[0]->id) ? 'Edit' : 'Add' ?>
                            </li>
                        </ol>
                    </div>
                </div>

				
                <div class="row">
                    <div class="col-sm-12">
					<label class="col-md-2 control-label"></div>
									
					<form action="<?php echo base_url() ?><?php echo isset($Itn[0]->id) ? 'Package/updatePackage' : 'Package/insertPackage' ?>" name="insertPackage" id="insertPackage" method="post" class="form-horizontal" enctype="multipart/form-data" data-parsley-validate novalidate autocomplete="off">
					
                        <div class="row">
                            <div class="col-sm-12">
							
                                <div class="card-box">
                                    
										<h5 class="text-muted text-uppercase m-t-0 m-b-20"><b>General</b></h5>
										
											<div class="row">
												<div class="col-md-12">
													<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
												</div>
											</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">First Name :<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="firstName" name="firstName" class="form-control" 
											value="<?php echo $Package[0]->firstName; ?>" required>
											<?php if(isset($Package[0]->id)) { ?>
												<input type="hidden" name="inquiry_id" id="inquiry_id" value="<?php echo $Package[0]->id; ?>" >
											<?php } ?>
											<?php if(isset($Itn[0]->id)) { ?>
												<input type="hidden" name="package_id" id="package_id" value="<?php echo $Itn[0]->id; ?>" >
											<?php } ?>
											<?php if(isset($Itn[0]->inquiryId)) { ?>
												<input type="hidden" name="inqId" id="inqId" value="<?php echo $Itn[0]->inquiryId; ?>" >
											<?php } ?>
										</div>
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Last Name :<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="lastName" name="lastName" class="form-control" 
											value="<?php echo $Package[0]->lastName; ?>" required>
										</div>
									</div>
																		
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">No Off Adults :<span class="text-danger">*</span></label>
										<div class="col-md-6">
											<input type="numer" id="noOffAdults" name="noOffAdults" maxlength="3" class="form-control" 
											value="<?php echo isset($Package[0]->noOffAdults) ? set_value("noOffAdults", $Package[0]->noOffAdults) : set_value("noOffAdults"); ?>"  placeholder="Enter No Off Adults" parsley-trigger="change" required>
										</div>	
									</div>
									
								
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Child With Extra Bed :</label>
										<div class="col-md-6">
											<input type="numer" id="childWithExtraBed" name="childWithExtraBed" maxlength="3" class="form-control" 
											value="<?php echo isset($Package[0]->childWithExtraBed) ? set_value("childWithExtraBed", $Package[0]->childWithExtraBed) : set_value("childWithExtraBed"); ?>"  placeholder="Enter No Off Child With Extra bed" parsley-trigger="change" >
										</div>	
									</div>
									
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Child Without Extra Bed :</label>
										<div class="col-md-6">
											<input type="numer" id="childWithoutExtraBed" name="childWithoutExtraBed" maxlength="3" class="form-control" 
											value="<?php echo isset($Package[0]->childWithoutExtraBed) ? set_value("childWithoutExtraBed", $Package[0]->childWithoutExtraBed) : set_value("childWithoutExtraBed"); ?>"  placeholder="Enter No Off Child Without Extra bed" parsley-trigger="change">
										</div>	
									</div>
									
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Travel Date : <span class="text-danger">*</span></label>
										<div class="col-md-6">
											<div class='input-group date' id='datetimepicker1'>
												<input type='text' name="travelDate" id="travelDate" class="form-control" value="<?php if(isset($Package[0]->travelDate)){
													echo $Package[0]->travelDate;
												} ?>" placeholder="Select Travel Date" required/>
													<span class="input-group-addon">
														<span class="glyphicon glyphicon-calendar"></span>
													</span>
											</div>
										</div>	
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Package Name :<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="packageName" name="packageName" class="form-control" 
											value="<?php echo isset($Itn[0]->packageName) ? set_value("packageName", $Itn[0]->packageName) : set_value("packageName"); ?>" required>
										</div>
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Night Stay Plan:<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="nightStayPlan" name="nightStayPlan" class="form-control" 
											value="<?php echo isset($Itn[0]->nightStayPlan) ? set_value("nightStayPlan", $Itn[0]->nightStayPlan) : set_value("nightStayPlan"); ?>" required>
										</div>
									</div>
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label" for="itinanrary_data">Package Itinarary:<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<textarea name="itinanrary_data" id="itinanrary_data" class="form-control" required><?php echo isset($Itn[0]->itinanrary_data) ? set_value("itinanrary_data", $Itn[0]->itinanrary_data) : set_value("itinanrary_data"); ?></textarea>
										</div>
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label" for="hotelDetail">Hotel Detail:<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<textarea name="hotelDetail" id="hotelDetail" class="form-control" required><?php echo isset($Itn[0]->hotelDetail) ? set_value("hotelDetail", $Itn[0]->hotelDetail) : set_value("hotelDetail"); ?></textarea>
										</div>
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label" for="fliteDetail">Flite Detail:<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<textarea name="fliteDetail" id="fliteDetail" class="form-control" required><?php echo isset($Itn[0]->fliteDetail) ? set_value("fliteDetail", $Itn[0]->fliteDetail) : set_value("fliteDetail"); ?></textarea>
										</div>
									</div>
									
								<!--<div class="slider_data">
									<div class="inner_slider_data">
										<div class="form-group">
											<label class="col-md-2 control-label" for="itnDay">Day1:<span class="text-danger">*</span></label>
											<div class="col-md-6">
												<textarea name="itinanrary_data1" id="itinanrary_data1" class="form-control"></textarea>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-2 control-label" for="itnTitle">Day1 Title:<span class="text-danger">*</span></label>
											<div class="col-md-6">
												<input type="text" id="itnTitle" name="itnTitle[]" class="form-control" placeholder="Enter Title, Ex. Manali To Simla" required>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-2 control-label" for="itnDesc">Day1 Description:<span class="text-danger">*</span></label>
											<div class="col-md-6">
												<textarea name="itnDesc[]" id="itnDesc" class="form-control smart_editor" placeholder="Enter Description, Ex. Description" required></textarea>
											</div>
										</div>
									</div>
								</div>
								<div class="btn btn-primary add-new-btn pull-right col-lg-1" onclick="addrow();" > Add New </div>-->
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Dipature City:<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="depatureCity" name="depatureCity" class="form-control" 
											value="<?php echo isset($Itn[0]->depatureCity) ? set_value("depatureCity", $Itn[0]->depatureCity) : set_value("depatureCity"); ?>" required>
										</div>
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Package Price:<span class="text-danger">*</span></label>
										<div class="col-md-6"> 
											<input type="text" id="packagePrice" name="packagePrice" class="form-control" 
											value="<?php echo isset($Itn[0]->packagePrice) ? set_value("packagePrice", $Itn[0]->packagePrice) : set_value("packagePrice"); ?>" required>
										</div>
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Amount Per Person:</label>
										<div class="col-md-6"> 
											<input type="text" id="amountPerPerson" name="amountPerPerson" class="form-control" 
											value="<?php echo isset($Itn[0]->amountPerPerson) ? set_value("amountPerPerson", $Itn[0]->amountPerPerson) : set_value("amountPerPerson"); ?>">
										</div>
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Amount Per Child:</label>
										<div class="col-md-6"> 
											<input type="text" id="amountPerChild" name="amountPerChild" class="form-control" 
											value="<?php echo isset($Itn[0]->amountPerChild) ? set_value("amountPerChild", $Itn[0]->amountPerChild) : set_value("amountPerChild"); ?>">
										</div>
									</div>
									
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Package Inclusions :<span class="text-danger">*</span></label>
										<div class="col-md-6">
											<textarea name="packageInclusion" id="packageInclusion" required parsley-trigger="change" cols="88" rows="5"><?php echo isset($Itn[0]->packageInclusion) ? set_value("packageInclusion", $Itn[0]->packageInclusion) : set_value("packageInclusion"); ?>
											</textarea>
											<?php if( form_error('packageInclusion') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('packageInclusion'); ?></li></ul>
											<?php } ?>
										</div>	
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Photo Galary:</label>
										<div class="col-md-6">
											<input type="file" name="multiplepackagePhoto[]" multiple class="filestyle <?php if( form_error('multiplepackagePhoto') != '') { ?> parsley-error" <?php } ?>  data-iconname="fa fa-cloud-upload" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);" >
											<?php if( form_error('multiplepackagePhoto') != '') { ?>
												<ul class="parsley-errors-list filled" id="parsley-id-5"><li class="parsley-required"><?php echo form_error('multiplehotelPhoto'); ?></li></ul>
											<?php } ?>
										</div>
									</div>
									<?php if(isset($GalaryPhotodata) && !empty($GalaryPhotodata)){ ?>
											<div class="col-md-12" id="image_list" style="margin-bottom:20px">
											<label class="col-md-2 control-label"></label>
											<div class="col-md-6">
											<?php	
											 for($i = 0; $i < count($GalaryPhotodata); $i++){ ?>
												<div class="col-md-4 m-b-20" id="img_div_<?php echo $GalaryPhotodata[$i]['id']?>"><img src="<?php echo base_url() ?>uploads/images/<?php echo  $GalaryPhotodata[$i]['image']; ?>" height="120px" width="180px"/><span onclick="javasctipt: removethis(<?php echo  $GalaryPhotodata[$i]['packageId']; ?>,<?php echo $GalaryPhotodata[$i]['id']; ?>)"><i class="fa fa-trash trash-icon pull-right" title="Remove" ></i></span></div>
											 <?php } ?>
											</div> 
											</div>
										<?php } ?>
									
										<hr />
									<div class="form-group p-20">
										<label class="col-md-1 control-label"></label>
										
										<div class="col-md-11">
										 <button type="submit" class="btn w-sm btn-default waves-effect waves-light">Save</button>
										 <button type="button" class="btn w-sm btn-white waves-effect"><a href="<?php echo base_url(); ?>inquiryListing">Cancel</a></button>
										</div> 
									</div>
                                </div>
							</div>
						</div>
					</form>
                </div>
            </div>

<?php $this->load->view('includes/footer', ''); ?>
<script type="text/javascript">

jQuery(document).ready(function($) {
		$('.counter').counterUp({
			delay: 100,
			time: 1200
		});
		$(".knob").knob();
		
		$('form').parsley();
		//$('#FollowupDate').datepicker();
		//$('#travelingdate').datepicker();
		$('#datetimepicker1').datepicker();
		$('#datetimepicker2').datepicker();
});

/* remove image */
	function removethis(packageId, id)
	{
		if( confirm('Are you sure to delete this file') == true )
		{
			if ( packageId != '' )
			{
				$.ajax({
					url:'<?php echo base_url();?>deleteGalaryPhoto/'+packageId+'/'+id,
					method: 'post',
					data: {packageId: packageId, id:id},
					dataType: 'json',
					success: function(response){
						
						if(response.Del == 'TRUE'){
							$('#img_div_'+id).html('');
						}
					}
				});
			}
		}
	}	
	CKEDITOR.replace( 'itinanrary_data', {
		height: 260
	} );
	CKEDITOR.replace( 'hotelDetail', {
		height: 260
	} );
	CKEDITOR.replace( 'fliteDetail', {
		height: 260
	} );	
</script>           