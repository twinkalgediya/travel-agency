<?php

$userId = '';
$name = '';
$email = '';
$mobile = '';

if(!empty($userInfo))
{
    foreach ($userInfo as $uf)
    {
        $userId = $uf->userId;
        $name = $uf->name;
        $email = $uf->email;
        $mobile = $uf->mobile;
    }
}
?>

			<!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li>
                                <h4 class="page-title">Profile</h4>
                            </li>
							<li>
                                <a href="#">Account Setting</a>
                            </li>
                            
                            <li class="active">
                                Edit
                            </li>
                        </ol>
                </div>
               
                <div class="row">
                    <div class="col-sm-12">
					<label class="col-md-2 control-label"></div>
									
					<form action="<?php echo base_url() ?>updateUser" name="userUpdate" id="userUpdate" method="post" class="form-horizontal" data-parsley-validate novalidate>
                        <div class="row">
                            <div class="col-sm-12">
							
                                <div class="card-box">
                                    
										<h5 class="text-muted text-uppercase m-t-0 m-b-20"><b>General</b></h5>
										
									
										<?php
											$this->load->helper('form');
											$error = $this->session->flashdata('error');
											if($error)
											{
										?>
											<div class="alert alert-danger alert-dismissable">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
												<?php echo $this->session->flashdata('error'); ?>
											</div>
											<?php } ?>
											<?php  
											$success = $this->session->flashdata('success');
											if($success)
											{
										?>
											<div class="alert alert-success alert-dismissable">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
												<?php echo $this->session->flashdata('success'); ?>
											</div>
											<?php } ?>

											<?php  
											$noMatch = $this->session->flashdata('nomatch');
											if($noMatch)
											{
										?>
											<div class="alert alert-warning alert-dismissable">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
												<?php echo $this->session->flashdata('nomatch'); ?>
											</div>
											<?php } ?>

											<div class="row">
												<div class="col-md-12">
													<?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
												</div>
											</div>
									
									
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Name <span class="text-danger">*</span></label>
										<div class="col-md-10"> 
											<input type="text" id="fname" name="fname" maxlength="128" class="form-control" value="<?php echo $name; ?>"  placeholder="Enter Your Name" parsley-trigger="change" required>
											<input type="hidden" value="<?php echo $userId; ?>" name="userId" id="userId" />
										</div>
									</div>
										
									<div class="form-group m-b-20">
										 <label class="col-md-2 control-label">Email Address <span class="text-danger">*</span></label>
										<div class="col-md-10">
											<input type="text" id="email" name="email" maxlength="128" class="form-control" value="<?php echo $email; ?>"  placeholder="Enter Your Email" parsley-trigger="change" required>
										</div>	
									</div>
										
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Old Password <span class="text-danger">*</span></label>
										<div class="col-md-10"> 
											<input type="password" id="oldpassword" name="oldpassword" maxlength="20" class="form-control" placeholder="Enter Your Old Password" required>
										</div>
									</div>
										
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">New Password <span class="text-danger">*</span></label>
										<div class="col-md-10">
											<input type="password" id="cpassword" name="cpassword" maxlength="20" class="form-control" placeholder="Enter Your Old Password" required>
										</div>
									</div>
									
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Verify New Password<span class="text-danger">*</span></label>
										<div class="col-md-10">
											<input data-parsley-equalto="#cpassword" type="password" id="cpassword2" name="cpassword2" maxlength="20" class="form-control" placeholder="Verify New Password" required>
										</div>
									</div>
										
									<div class="form-group m-b-20">
										<label class="col-md-2 control-label">Phone Number<span class="text-danger">*</span></label>
										<div class="col-md-10"> 
											<input type="mobile" id="mobile" name="mobile" data-mask="+91-9999999999" maxlength="20" class="form-control" placeholder="Enter Your Phone Number" value="<?php echo $mobile; ?>" parsley-trigger="change" required>
										</div>
									</div>
										<hr />
									<div class="form-group p-20">
										<label class="col-md-1 control-label"></label>
										
										<div class="col-md-11">
										 <button type="submit" class="btn w-sm btn-default waves-effect waves-light">Save</button>
										 <button type="reset" class="btn w-sm btn-white waves-effect">Reset</button>
										</div> 
									</div>
                                </div>
							</div>
						</div>
					</form>
                </div>
            </div>

<?php $this->load->view('includes/footer', ''); ?>                