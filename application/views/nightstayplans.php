
<!-- DataTables -->
<link href="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>

<!-- Page-Title -->
<div class="row">
	<div class="col-sm-12">
		<ol class="breadcrumb">
			<li>
				<a href="<?php echo base_url(); ?>"><h4 class="page-title">Dashboard</h4></a>
			</li>
			<li>
				<a href="<?php echo base_url(); ?>hotelListing">Night Stay Plan</a>
			</li>
			<li class="active">
				List
			</li>
		</ol>
	</div>
</div>
<div class="row">
<div class="col-sm-12">
	<div class="card-box table-responsive">
		<div class="btn-group pull-right m-t-0">
			<a class="btn btn-default dropdown-toggle waves-effect waves-light" href="<?php echo base_url(); ?>addNightStayPlan"><span class="m-l-5"><i class="md md-add-circle"></i></span> Add New </a>

		</div>
		<h4 class="m-t-0 m-b-30 header-title"><b>Night Stay Plan List</b></h4>
		<?php
			$error = $this->session->flashdata('error');
			if($error){ ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                    <?php } ?>
                    <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    { ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                    <?php } ?>
		<table id="datatable-responsive"
			   class="table table-striped table-bordered dt-responsive hotel_list nowrap" cellspacing="0"
			   width="100%">
			<thead>
				<tr>
					<th>Type Of Place</th>
					<th>Country</th>
					<th>state</th>
					<th>No Of Nights</th>
					<th>Night Stay</th>
					<th>Action</th>
					
				</tr>
			</thead>
			<tbody>
				<?php if(!empty($nightRecords)){
					foreach($nightRecords as  $value) { ?>		
						<tr>
							<td> <?php echo $value->typeof_place;  ?> </td>
							<td> <?php echo $value->country; ?> </td>
							<td> <?php echo $value->state; ?> </td>
							<td> <?php echo $value->no_of_nights; ?> </td>
							<td> <?php echo $value->night_stay; ?> </td>
							

							<td class="text-center">
								 <a class="btn btn-sm btn-success" href="<?php echo base_url().'editNightStayPlan/'.$value->id; ?>" title="Edit">
									<i class="fa fa-pencil"></i>
								</a>
								<a class="btn btn-sm btn-danger" href="<?php echo base_url().'deleteNightStayPlan/'.$value->id; ?>" title="Delete" data-userid="<?php echo $value->id; ?>"
								title="Sil">
								<i class="fa fa-trash"></i>
								</a>
							</td>
						</tr>
                    <?php }
                } ?>
			</tbody>
		</table>
	</div>
</div>
</div>
<?php $this->load->view('includes/footer', ''); ?>
<script type="text/javascript">

jQuery(document).ready(function($) {
		$('.counter').counterUp({
			delay: 100,
			time: 1200
		});
		$(".knob").knob();
		
		$('form').parsley();
		$('.hotel_list').DataTable({
			"lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]]
		});
});
		
</script>