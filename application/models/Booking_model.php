<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


class Booking_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function bookingListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.bookingId,BaseTbl.inquiryId,BaseTbl.id, BaseTbl.packageId, BaseTbl.totalAmtReceve, BaseTbl.addedBy');
        $this->db->from('tbl_booking as BaseTbl');
		
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.inquiryId  LIKE '%".$searchText."%'
                            OR  BaseTbl.totalAmtReceve  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        if($this->session->userdata['roleText'] != 'Admin'){
			$this->db->where('BaseTbl.addedBy', $this->session->userdata['userId']);
		}
		//$this->db->where('BaseTbl.isDelete', 0);
		
        $query = $this->db->get();
        
        return $query->num_rows();
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function bookingListing($searchText = '', $page, $segment, $id='')
    {
		
       $this->db->select('BaseTbl.bookingId,BaseTbl.inquiryId,BaseTbl.id, BaseTbl.packageId, BaseTbl.totalAmtReceve, BaseTbl.addedBy,tbl_inquiry.inquiryID,tbl_packages.packageName,tbl_packages.packagePrice');
        $this->db->from('tbl_booking as BaseTbl');
		$this->db->join('tbl_inquiry','tbl_inquiry.id=BaseTbl.inquiryId');	
	    $this->db->join('tbl_packages','tbl_packages.id=BaseTbl.packageId');	
        if(!empty($searchText)) {
            $likeCriteria = "(tbl_inquiry.inquiryID  LIKE '%".$searchText."%'
                            OR  BaseTbl.totalAmtReceve  LIKE '%".$searchText."%'
							OR  tbl_packages.packageName  LIKE '%".$searchText."%'
							OR  tbl_packages.packagePrice  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
		if($this->session->userdata['roleText'] != 'Admin'){
			$this->db->where('BaseTbl.addedBy', $this->session->userdata['userId']);
		}
		
		$this->db->limit($page, $segment);
		
	   $this->db->order_by('BaseTbl.id', 'DESC');
        
        $query = $this->db->get();
       
        $result = $query->result(); 
	
        return $result;
    }
    
    /**
     * This function is used to get the inquiry information
     * @return array $result : This is result of the query
     */
    function getBooking($bookingId)
    {
        $this->db->select('*,tbl_booking.id as bookingAutoId,tbl_inquiry.id as inqAutoId,tbl_packages.id as pacAutoId');
        $this->db->from('tbl_booking');
        $this->db->join('tbl_inquiry','tbl_inquiry.id=tbl_booking.inquiryId');
        $this->db->join('tbl_packages','tbl_packages.id=tbl_booking.packageId');
        $this->db->where('tbl_booking.id',$bookingId);
		if($this->session->userdata['roleText'] != 'Admin'){
			$this->db->where('addedBy', $this->session->userdata['userId']);
		}
        $query = $this->db->get();
		
        return $query->result();
    }

     function getHotelDetail($bookingId)
    {
        $this->db->select('*');
        $this->db->from('tbl_hotel_booking');
        $this->db->where('bookingId',$bookingId);
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function insertBooking($bookingInfo)
    {
        $this->db->insert('tbl_booking', $bookingInfo);
        $insert_id = $this->db->insert_id();
		
        return $insert_id;
    }
	
	function getCount()
    {
        $this->db->select('*');
        $this->db->from('tbl_booking');
        $query = $this->db->get();
		
        return $query->num_rows();
    }
	
	/**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function insertHotelDetail($hotelInfo)
    {
        $this->db->insert('tbl_hotel_booking', $hotelInfo);
        $insert_id = $this->db->insert_id();
		return $insert_id;
    }
   
     public function deleteBookingDetail($bookingId)
    {
        $this->db->where('bookingId', $bookingId);
        $this->db->delete('tbl_hotel_booking');
        return $this->db->affected_rows();
    }
    
    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function updateBooking($bookingInfo, $bookingId)
    {
        $this->db->where('id', $bookingId);
        $this->db->update('tbl_booking', $bookingInfo);
        
        return TRUE;
    }

     /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function updateHotelBooking($hotelInfo, $hotelId)
    {
        $this->db->where('id', $hotelId);
        $this->db->update('tbl_hotel_booking', $hotelInfo);
        
        return TRUE;
    }
	
	function getHotelRows($state_id,$city_id){
		$this->db->select('*');
        $this->db->from('tbl_hotel');
        $this->db->where('city_id',$city_id);
        $this->db->where('state_id',$state_id);
        $query = $this->db->get();
        return $query->result_array();
    }
   
}

