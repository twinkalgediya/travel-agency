<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Inquiry_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function inquiryListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.firstName,BaseTbl.lastName, BaseTbl.inquiryID, BaseTbl.email, BaseTbl.phoneNo, BaseTbl.destination, BaseTbl.travelDate,BaseTbl.nextFollowupDate, BaseTbl.noOffAdults, BaseTbl.childWithExtraBed, BaseTbl.childWithoutExtraBed, BaseTbl.prospect, BaseTbl.source, BaseTbl.notes, BaseTbl.status');
        $this->db->from('tbl_inquiry as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.firstName  LIKE '%".$searchText."%'
                            OR  BaseTbl.lastName  LIKE '%".$searchText."%'
							OR  BaseTbl.inquiryID  LIKE '%".$searchText."%'
                            OR  BaseTbl.customerName  LIKE '%".$searchText."%'
                            OR  BaseTbl.phoneNo  LIKE '%".$searchText."%'
                            OR  BaseTbl.destination  LIKE '%".$searchText."%'
                            OR  BaseTbl.travelDate  LIKE '%".$searchText."%'
                            OR  BaseTbl.noOffAdults  LIKE '%".$searchText."%'
                            OR  BaseTbl.childWithExtraBed  LIKE '%".$searchText."%'
                            OR  BaseTbl.childWithoutExtraBed  LIKE '%".$searchText."%'
                            OR  BaseTbl.prospect  LIKE '%".$searchText."%'
                            OR  BaseTbl.prospect  LIKE '%".$searchText."%'
                            OR  BaseTbl.status  LIKE '%".$searchText."%'
                            OR  BaseTbl.notes  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        if($this->session->userdata['roleText'] != 'Admin'){
			$this->db->where('BaseTbl.addedBy', $this->session->userdata['userId']);
		}
		$this->db->where('BaseTbl.isDelete', 0);
		
        $query = $this->db->get();
        
        return $query->num_rows();
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function inquiryListing($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.firstName,BaseTbl.lastName, BaseTbl.inquiryID, BaseTbl.id, BaseTbl.altEmail, BaseTbl.altPhoneNo, BaseTbl.email, BaseTbl.phoneNo, BaseTbl.nextFollowupDate,BaseTbl.destination, BaseTbl.travelDate, BaseTbl.noOffAdults, BaseTbl.childWithExtraBed, BaseTbl.childWithoutExtraBed, BaseTbl.prospect, BaseTbl.source, BaseTbl.notes, BaseTbl.status');
        $this->db->from('tbl_inquiry as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.firstName  LIKE '%".$searchText."%'
                            OR  BaseTbl.lastName  LIKE '%".$searchText."%'
                            OR  BaseTbl.inquiryID  LIKE '%".$searchText."%'
                            OR  BaseTbl.customerName  LIKE '%".$searchText."%'
                            OR  BaseTbl.phoneNo  LIKE '%".$searchText."%'
                            OR  BaseTbl.destination  LIKE '%".$searchText."%'
                            OR  BaseTbl.travelDate  LIKE '%".$searchText."%'
                            OR  BaseTbl.nextFollowupDate  LIKE '%".$searchText."%'
                            OR  BaseTbl.noOffAdults  LIKE '%".$searchText."%'
                            OR  BaseTbl.childWithExtraBed  LIKE '%".$searchText."%'
                            OR  BaseTbl.childWithoutExtraBed  LIKE '%".$searchText."%'
                            OR  BaseTbl.prospect  LIKE '%".$searchText."%'
                            OR  BaseTbl.prospect  LIKE '%".$searchText."%'
                            OR  BaseTbl.status  LIKE '%".$searchText."%'
                            OR  BaseTbl.notes  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
		if($this->session->userdata['roleText'] != 'Admin'){
			$this->db->where('BaseTbl.addedBy', $this->session->userdata['userId']);
		}
        $this->db->where('BaseTbl.status <>', 'Closed');
        $this->db->where('BaseTbl.isDelete', 0);
        $this->db->limit($page, $segment);
		$this->db->order_by('BaseTbl.id', 'DESC');
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
    
    /**
     * This function is used to get the inquiry information
     * @return array $result : This is result of the query
     */
    function getInquiry($inquiryId)
    {
        $this->db->select('*');
        $this->db->from('tbl_inquiry');
        $this->db->where('id',$inquiryId);
		if($this->session->userdata['roleText'] != 'Admin'){
			$this->db->where('addedBy', $this->session->userdata['userId']);
		}
        $query = $this->db->get();
		
        return $query->result();
    }

	
	function getOditlog($inquiryId)
    {
        $this->db->select('*');
        $this->db->from('tbl_log');
        $this->db->where('pro_inq_id',$inquiryId);
		$this->db->order_by('id','DESC');
        $query = $this->db->get();
        return $query->result();
    }
	
    /**
     * This function is used to check whether email id is already exist or not
     * @param {string} $email : This is email id
     * @param {number} $userId : This is user id
     * @return {mixed} $result : This is searched result
     */
    function checkEmailExists($email, $userId = 0)
    {
        $this->db->select("email");
        $this->db->from("tbl_users");
        $this->db->where("email", $email);   
        $this->db->where("isDeleted", 0);
        if($userId != 0){
            $this->db->where("userId !=", $userId);
        }
        $query = $this->db->get();

        return $query->result();
    }
    
    
    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function insertInquiry($inqueryInfo)
    {
        $this->db->insert('tbl_inquiry', $inqueryInfo);
        $insert_id = $this->db->insert_id();
		
		
		//get last insert id
		$Inquiry_id = 'INQ'.str_pad($insert_id,6,'0',STR_PAD_LEFT);
		
		$data=array('inquiryID' =>$Inquiry_id);
		$this->db->where('id', $insert_id);
        $this->db->update('tbl_inquiry',$data);
			
        return $insert_id;
    }
	
	function insertNote($inqueryNote)
    {
        $this->db->insert('tbl_inquiry_notes', $inqueryNote);
        $insert_id = $this->db->insert_id();
		return $insert_id;
    }
    
    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getUserInfo($userId)
    {
        $this->db->select('userId, name, email, mobile, roleId');
        $this->db->from('tbl_users');
        $this->db->where('isDeleted', 0);
        $this->db->where('userId', $userId);
        $query = $this->db->get();
        
        return $query->result();
    }
	function getInquiryNote($inquiryId)
    {
        $this->db->select('tbl_users.name, tbl_inquiry_notes.*');
        $this->db->from('tbl_inquiry_notes');
		$this->db->join('tbl_users','tbl_users.userId=tbl_inquiry_notes.addedBy');
        $this->db->where('inquiryID', $inquiryId);
		$this->db->order_by('tbl_inquiry_notes.id', 'DESC');
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function updateInquiry($inquiryInfo, $inquiryId)
    {
        $this->db->where('id', $inquiryId);
        $this->db->update('tbl_inquiry', $inquiryInfo);
        
        return TRUE;
    }
    
    
    /**
     * This function is used to get the user roles information
     * @return array $result : This is result of the query
     */
    function getnbr()
    {
		$this->db->select('id, not_buy_resion');
        $this->db->from('tbl_not_buy_resion');
		$query = $this->db->get();
        
        return $query->result();
    }
	
    /**
     * This function is used to delete the user information
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    
	 public function deleteInquiry($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_inquiry');
		return $this->db->affected_rows();
    }


    /**
     * This function is used to match users password for change password
     * @param number $userId : This is user id
     */
    function matchOldPassword($userId, $oldPassword)
    {
        $this->db->select('userId, password');
        $this->db->where('userId', $userId);        
        $this->db->where('isDeleted', 0);
        $query = $this->db->get('tbl_users');
        
        $user = $query->result();

        if(!empty($user)){
            if(verifyHashedPassword($oldPassword, $user[0]->password)){
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }
    
    /**
     * This function is used to change users password
     * @param number $userId : This is user id
     * @param array $userInfo : This is user updation info
     */
    function changePassword($userId, $userInfo)
    {
        $this->db->where('userId', $userId);
        $this->db->where('isDeleted', 0);
        $this->db->update('tbl_users', $userInfo);
        
        return $this->db->affected_rows();
    }


    /**
     * This function is used to get user log history count
     * @param number $userId : This is user id
     */
    	
    function logHistoryCount($userId)
    {
        $this->db->select('*');
        $this->db->from('tbl_log as BaseTbl');

        if ($userId == NULL)
        {
            $query = $this->db->get();
            return $query->num_rows();
        }
        else
        {
            $this->db->where('BaseTbl.userId', $userId);
            $query = $this->db->get();
            return $query->num_rows();
        }
    }

    /**
     * This function is used to get user log history
     * @param number $userId : This is user id
     * @return array $result : This is result
     */
    function logHistory($userId)
    {
        $this->db->select('*');        
        $this->db->from('tbl_log as BaseTbl');

        if ($userId == NULL)
        {
            $this->db->order_by('BaseTbl.createdDtm', 'DESC');
            $query = $this->db->get();
            $result = $query->result();        
            return $result;
        }
        else
        {
            $this->db->where('BaseTbl.userId', $userId);
            $this->db->order_by('BaseTbl.createdDtm', 'DESC');
            $query = $this->db->get();
            $result = $query->result();
            return $result;
        }
    }

    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getUserInfoById($userId)
    {
        $this->db->select('userId, name, email, mobile, roleId');
        $this->db->from('tbl_users');
        $this->db->where('isDeleted', 0);
        $this->db->where('userId', $userId);
        $query = $this->db->get();
        
        return $query->row();
    }

    

    /**
     * This function is used to get user log history
     * @return array $result : This is result
     */
    function logHistoryBackup()
    {
        $this->db->select('*');        
        $this->db->from('tbl_log_backup as BaseTbl');
        $this->db->order_by('BaseTbl.createdDtm', 'DESC');
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }

    

    /**
     * This function is used to get the logs count
     * @return array $result : This is result
     */
    function logsCount()
    {
        $this->db->select('*');
        $this->db->from('tbl_log as BaseTbl');
        $query = $this->db->get();
        return $query->num_rows();
    }

    /**
     * This function is used to get the users count
     * @return array $result : This is result
     */
    function usersCount()
    {
        $this->db->select('*');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->where('isDeleted', 0);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function getUserStatus($userId)
    {
        $this->db->select('BaseTbl.status');
        $this->db->where('BaseTbl.userId', $userId);
        $this->db->limit(1);
        $query = $this->db->get('tbl_users as BaseTbl');

        return $query->row();
    }
}

  