<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Noofnigth_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function noofnightListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.id,countries.name as country,states.name as state, BaseTbl.no_of_nights,BaseTbl.typeof_place');
        $this->db->from('no_of_night as BaseTbl');
        $this->db->join('countries','countries.id=BaseTbl.country_id');
         $this->db->join('states','states.id=BaseTbl.state_id');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.typeof_place  LIKE '%".$searchText."%'
                            OR  BaseTbl.no_of_nights  LIKE '%".$searchText."%'
                            OR  BaseTbl.typeof_place LIKE '%".$searchText."%'
                             OR  countries.name LIKE '%".$searchText."%'
                              OR  states.name LIKE '%".$searchText."%'
                           )";
            $this->db->where($likeCriteria);
        }
		$query = $this->db->get();
       
        return $query->num_rows();
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function noofnightListing($searchText = '', $page, $segment)
    {
       $this->db->select('BaseTbl.id,countries.name as country,states.name as state, BaseTbl.no_of_nights,BaseTbl.typeof_place,BaseTbl.country_id,BaseTbl.state_id');
        $this->db->from('no_of_night as BaseTbl');
        $this->db->join('countries','countries.id=BaseTbl.country_id','left');
        $this->db->join('states','states.id=BaseTbl.state_id','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.typeof_place  LIKE '%".$searchText."%'
                            OR  BaseTbl.no_of_nights  LIKE '%".$searchText."%'
                            OR  BaseTbl.typeof_place LIKE '%".$searchText."%'
                             OR  countries.name LIKE '%".$searchText."%'
                              OR  states.name LIKE '%".$searchText."%'
                           )";
            $this->db->where($likeCriteria);
        }
        $query = $this->db->get();  
        $result = $query->result(); 
        return $result;
    }
    

    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function insertNoofnight($night_stay_plan)
    {
        $this->db->insert('no_of_night', $night_stay_plan);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    } 
	
    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function updateNightStayPlan($night_stay_plan, $nightId)
    {
        $this->db->where('id', $nightId);
        $this->db->update('no_of_night', $night_stay_plan);
        
        return TRUE;
    }
    
    
    /**
     * This function is used to delete the user information
     * @param n$this->db->join('states','states.id=night_stay_plan.state_id');umber $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    
	public function deletenight($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('no_of_night');
		return $this->db->affected_rows();
    }

    function getInternationalrows($plan){
        if($plan =='domestic')
        {   $this->db->select('states.name,night_stay_plan.state_id');
            $this->db->from('night_stay_plan');
            $this->db->join('states','states.id=night_stay_plan.state_id');
        }
        if($plan =='international')
        {   $this->db->select('countries.name,night_stay_plan.country_id');
            $this->db->from('night_stay_plan');
            $this->db->join('countries','countries.id=night_stay_plan.country_id');
        }
        $this->db->where('typeof_place',$plan);
        $query = $this->db->get();
        return $query->result_array();
    }

    function getnightbycountryrows($country_id)
    {   
        $this->db->select('night_stay_plan.id,night_stay_plan.no_of_nights');
        $this->db->from('night_stay_plan');
        $this->db->where('country_id',$country_id);
        $query = $this->db->get();
        
        return $query->result_array();
    }
    function getnightbystaterows($state_id)
    {   
        $this->db->select('night_stay_plan.id,night_stay_plan.no_of_nights');
        $this->db->from('night_stay_plan');
        $this->db->where('state_id',$state_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    function getNoofnight($nightid)
    {   
        $this->db->select('*');
        $this->db->from('no_of_night');
        $this->db->where('id',$nightid);
        $query = $this->db->get();
        return $query->result();
    }
    function getIternaryrows($plan_id)
    {   
        $this->db->select('night_stay_plan.itinerary');
        $this->db->from('night_stay_plan');
        $this->db->where('id',$plan_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    

}

  