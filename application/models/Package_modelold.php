<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Package_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function packageListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.packageName,BaseTbl.packageName,BaseTbl.id, BaseTbl.packagePrice, BaseTbl.depatureCity, BaseTbl.amountPerPerson, BaseTbl.amountPerChild, BaseTbl.nightStayPlan, BaseTbl.packageInclusion');
        $this->db->from('tbl_packages as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.packageName  LIKE '%".$searchText."%'
                            OR  BaseTbl.packagePrice  LIKE '%".$searchText."%'
                            OR  BaseTbl.depatureCity  LIKE '%".$searchText."%'
							OR  BaseTbl.amountPerPerson  LIKE '%".$searchText."%'
                            OR  BaseTbl.nightStayPlan  LIKE '%".$searchText."%'
                            OR  BaseTbl.amountPerChild  LIKE '%".$searchText."%'
                            OR  BaseTbl.packageInclusion  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        if($this->session->userdata['roleText'] != 'Admin'){
			$this->db->where('BaseTbl.addedBy', $this->session->userdata['userId']);
		}
		//$this->db->where('BaseTbl.isDelete', 0);
		
        $query = $this->db->get();
        
        return $query->num_rows();
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function packageListing($searchText = '', $page, $segment, $id='')
    {
		
       $this->db->select('BaseTbl.id,BaseTbl.inquiryId,BaseTbl.packageName,BaseTbl.id,BaseTbl.packagePrice, BaseTbl.depatureCity, BaseTbl.amountPerPerson, BaseTbl.amountPerChild, BaseTbl.nightStayPlan, BaseTbl.packageInclusion,BaseTbl.itinanrary_data');
        $this->db->from('tbl_packages as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.packageName  LIKE '%".$searchText."%'
                            OR  BaseTbl.packagePrice  LIKE '%".$searchText."%'
                            OR  BaseTbl.depatureCity  LIKE '%".$searchText."%'
							OR  BaseTbl.amountPerPerson  LIKE '%".$searchText."%'
                            OR  BaseTbl.nightStayPlan  LIKE '%".$searchText."%'
                            OR  BaseTbl.amountPerChild  LIKE '%".$searchText."%'
                            OR  BaseTbl.packageInclusion  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
		if($this->session->userdata['roleText'] != 'Admin'){
			$this->db->where('BaseTbl.addedBy', $this->session->userdata['userId']);
		}
		
		if(isset($id) && $id > 0){
			$this->db->where('BaseTbl.inquiryId', $id);
		}else{
			$this->db->limit($page, $segment);
		}
       // $this->db->where('BaseTbl.isDelete', 0);
	   $this->db->order_by('BaseTbl.id', 'DESC');
        
        $query = $this->db->get();
       
        $result = $query->result(); 
	
        return $result;
    }
    
    /**
     * This function is used to get the inquiry information
     * @return array $result : This is result of the query
     */
    function getPackage($packageId)
    {
        $this->db->select('*');
        $this->db->from('tbl_packages');
        $this->db->where('id',$packageId);
		if($this->session->userdata['roleText'] != 'Admin'){
			$this->db->where('addedBy', $this->session->userdata['userId']);
		}
        $query = $this->db->get();
		
        return $query->result();
    }

	function getPackageWithGalary($packageId)
    {
        $this->db->select('*');
        $this->db->from('tbl_packages');
		$this->db->join('tbl_galary_photo', 'tbl_galary_photo.packageId = tbl_packages.id');
		$this->db->join('tbl_inquiry', 'tbl_inquiry.id = tbl_packages.inquiryId');
        $this->db->where('tbl_packages.id',$packageId);
		
        $query = $this->db->get();
		
        return $query->result();
    }
    
    
    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function insertPackage($packageInfo)
    {
        $this->db->insert('tbl_packages', $packageInfo);
        $insert_id = $this->db->insert_id();
		
        return $insert_id;
    }
	
	/**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function insertItinarary($itnInfo)
    {
        $this->db->insert('tbl_itinarary', $itnInfo);
        $insert_id = $this->db->insert_id();
		return $insert_id;
    }
	
	function getExtraPhoto($packageId)
    {
        $this->db->select('*');
        $this->db->from('tbl_galary_photo');
        $this->db->where('packageId',$packageId);
        $query = $this->db->get();
        return $query->result();
    }
   
   function getExtraPhotoByID($Id)
    {
        $this->db->select('*');
        $this->db->from('tbl_galary_photo');
        $this->db->where('id',$Id);
        $query = $this->db->get();
        return $query->result();
    }
   
    /**
     * This function is used to add hotel extra photos to system
     * @return number $insert_id : This is last inserted id
     */
    function insertGalaryPhoto($galaryPhotoInfo)
    {
        $this->db->insert('tbl_galary_photo', $galaryPhotoInfo);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    
	public function deletePhoto($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_galary_photo');
		return $this->db->affected_rows();
    }
	
	public function deleteExtraPhoto($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_galary_photo');
		return $this->db->affected_rows();
    } 
	
    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function updatePackage($packageInfo, $packageId)
    {
        $this->db->where('id', $packageId);
        $this->db->update('tbl_packages', $packageInfo);
        
        return TRUE;
    }
    
}

  