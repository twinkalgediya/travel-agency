<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Hotel_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function hotelListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.hotelName,BaseTbl.id, BaseTbl.stareRatting, BaseTbl.phoneNumber, BaseTbl.email_Id, BaseTbl.address');
        $this->db->from('tbl_hotel as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email_Id  LIKE '%".$searchText."%'
                            OR  BaseTbl.hotelName  LIKE '%".$searchText."%'
                           
                            OR  BaseTbl.stareRatting  LIKE '%".$searchText."%'
                            OR  BaseTbl.phoneNumber  LIKE '%".$searchText."%'
                            OR  BaseTbl.address  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
		/* if($this->session->userdata['roleText'] != 'Admin'){
			$this->db->where('BaseTbl.addedBy', $this->session->userdata['userId']);
		}*/
        $this->db->where('BaseTbl.isDelete', 0);
        $query = $this->db->get();
       
        return $query->num_rows();
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function hotelListing($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.hotelName, BaseTbl.id, BaseTbl.email_Id,BaseTbl.stareRatting, BaseTbl.phoneNumber, BaseTbl.address');
        $this->db->from('tbl_hotel as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email_Id  LIKE '%".$searchText."%'
                            OR  BaseTbl.hotelName  LIKE '%".$searchText."%'
                         
                            OR  BaseTbl.stareRatting  LIKE '%".$searchText."%'
                            OR  BaseTbl.phoneNumber  LIKE '%".$searchText."%'
                            OR  BaseTbl.address  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
		 /*if($this->session->userdata['roleText'] != 'Admin'){
			$this->db->where('BaseTbl.addedBy', $this->session->userdata['userId']);
		}*/
        $this->db->where('BaseTbl.isDelete', 0);
        $this->db->limit($page, $segment);
		$this->db->order_by('BaseTbl.id', 'DESC');
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
    
    /**
     * This function is used to get the inquiry information
     * @return array $result : This is result of the query
     */
    function getHotel($hotelId)
    {
        $this->db->select('*');
        $this->db->from('tbl_hotel');
        $this->db->where('id',$hotelId);
        $query = $this->db->get();
        return $query->result();
    }
	
	/**
     * This function is used to get the inquiry information
     * @return array $result : This is result of the query
	 * get hotel extra photos edit mode
     */
    function getExtraPhoto($hotelId)
    {
        $this->db->select('*');
        $this->db->from('tbl_hotel_photos');
        $this->db->where('hotelID',$hotelId);
        $query = $this->db->get();
        return $query->result();
    }
	
	function getExtraPhotoByID($Id)
    {
        $this->db->select('*');
        $this->db->from('tbl_hotel_photos');
        $this->db->where('id',$Id);
        $query = $this->db->get();
        return $query->result();
    }
	
	/**
     * This function is used to get the inquiry information
     * @return array $result : This is result of the query
     */
    function getPhoto($Id)
    {
        $this->db->select('*');
        $this->db->from('tbl_hotel');
        $this->db->where('id',$Id);
        $query = $this->db->get();
        return $query->result();
    }

    
    
    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function insertHotel($hotelInfo)
    {
        $this->db->insert('tbl_hotel', $hotelInfo);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    } 
	
	/**
     * This function is used to add hotel extra photos to system
     * @return number $insert_id : This is last inserted id
     */
    function insertHotelPhoto($hotelPhotoInfo)
    {
        $this->db->insert('tbl_hotel_photos', $hotelPhotoInfo);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    
    
    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function updateHotel($hotelInfo, $hotelId)
    {
        $this->db->where('id', $hotelId);
        $this->db->update('tbl_hotel', $hotelInfo);
        
        return TRUE;
    }
    
    
    
    /**
     * This function is used to delete the user information
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    
	 public function deleteHotel($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_hotel');
		return $this->db->affected_rows();
    }
	
	 public function deleteExtraPhoto($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_hotel_photos');
		return $this->db->affected_rows();
    } 
	
	public function deletePhoto($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_hotel');
		return $this->db->affected_rows();
    }

    
   
}

  