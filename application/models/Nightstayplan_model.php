<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Nightstayplan_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function nightstayplanListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.typeof_place,BaseTbl.id, BaseTbl.no_of_nights, BaseTbl.night_stay');
        $this->db->from('night_stay_plan as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.typeof_place  LIKE '%".$searchText."%'
                            OR  BaseTbl.no_of_nights  LIKE '%".$searchText."%'
                            OR  BaseTbl.night_stay  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
		$query = $this->db->get();
       
        return $query->num_rows();
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function nightstayplanListing($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.typeof_place,BaseTbl.id, no_of_night.no_of_nights, BaseTbl.night_stay,BaseTbl.country_id,BaseTbl.state_id,countries.name as country,states.name as state');
        $this->db->from('night_stay_plan as BaseTbl');
        $this->db->join('countries','countries.id=BaseTbl.country_id','left');
        $this->db->join('states','states.id=BaseTbl.state_id','left');
        $this->db->join('no_of_night','BaseTbl.no_of_nights=no_of_night.id');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.typeof_place  LIKE '%".$searchText."%'
                            OR  no_of_night.no_of_nights  LIKE '%".$searchText."%'
                            OR  BaseTbl.night_stay  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
    

    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function insertNightStayPlan($night_stay_plan)
    {
        $this->db->insert('night_stay_plan', $night_stay_plan);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    } 
	
     function getNightStayPlan($nightId)
    {
        $this->db->select('*');
        $this->db->from('night_stay_plan');
        $this->db->where('id',$nightId);
        
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function updateNightStayPlan($night_stay_plan, $nightId)
    {
        $this->db->where('id', $nightId);
        $this->db->update('night_stay_plan', $night_stay_plan);
        
        return TRUE;
    }
    
    
    /**
     * This function is used to delete the user information
     * @param n$this->db->join('states','states.id=night_stay_plan.state_id');umber $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    
	public function deleteNightStayPlan($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('night_stay_plan');
		return $this->db->affected_rows();
    }

    function getInternationalrows($plan){
        if($plan =='domestic')
        {   $this->db->select('states.name,night_stay_plan.state_id');
            $this->db->from('night_stay_plan');
            $this->db->join('states','states.id=night_stay_plan.state_id');
            $this->db->where('typeof_place',$plan);
            $this->db->group_by('night_stay_plan.state_id');
        }
        if($plan =='international')
        {   $this->db->select('countries.name,night_stay_plan.country_id');
            $this->db->from('night_stay_plan');
            $this->db->join('countries','countries.id=night_stay_plan.country_id');
            $this->db->where('typeof_place',$plan);
            $this->db->group_by('night_stay_plan.country_id');
        }
       
        $query = $this->db->get();
      
        return $query->result_array();
    }

    function getInternationalrows1($plan){
        if($plan =='domestic')
        {   $this->db->select('states.name,states.id AS state_id');
            $this->db->from('states');
        }
        if($plan =='international')
        {   $this->db->select('countries.name,countries.id AS country_id');
            $this->db->from('countries');
        }
       
        $query = $this->db->get();
      
        return $query->result_array();
    }

    function getnightbycountryrows($country_id)
    {   
        $this->db->select('night_stay_plan.id,no_of_night.no_of_nights');
        $this->db->from('night_stay_plan');
        $this->db->join('no_of_night','night_stay_plan.no_of_nights=no_of_night.id');
        $this->db->where('night_stay_plan.country_id',$country_id);
        $query = $this->db->get();
        //print_r($this->db->last_query());die;
        return $query->result_array();
    }
    function getnightbystaterows($state_id)
    {   
        $this->db->select('night_stay_plan.id,no_of_night.no_of_nights');
        $this->db->from('night_stay_plan');
        $this->db->join('no_of_night','night_stay_plan.no_of_nights=no_of_night.id');
        $this->db->where('night_stay_plan.state_id',$state_id);
        $query = $this->db->get();

        return $query->result_array();
    }
    function getnightStayPlanrows($nightid)
    {   
        $this->db->select('night_stay_plan.id,night_stay_plan.night_stay');
        $this->db->from('night_stay_plan');
        $this->db->where('id',$nightid);
        $query = $this->db->get();
        return $query->result_array();
    }
    function getIternaryrows($plan_id)
    {   
        $this->db->select('night_stay_plan.itinerary, night_stay_plan.id');
        $this->db->from('night_stay_plan');
        $this->db->where('id',$plan_id);
        $query = $this->db->get();
        return $query->result_array();
    }
	function getnightByCountry($country_id)
    {   $this->db->select('no_of_night.no_of_nights, no_of_night.id');
        $this->db->from('no_of_night');
        //$this->db->join('night_stay_plan','night_stay_plan.no_of_nights=no_of_night.id AND night_stay_plan.country_id="'.$country_id.'" ','left');
        $this->db->where('no_of_night.country_id',$country_id);
        //$this->db->where('night_stay_plan.no_of_nights',null);
        $query = $this->db->get();
		//print_r($this->db->last_query());
        return $query->result();
    }
    function getnightByCountryedit($country_id)
    {  
        $this->db->select('no_of_night.no_of_nights, no_of_night.id');
        $this->db->from('no_of_night');
        $this->db->where('no_of_night.country_id',$country_id);
        $query = $this->db->get();

        return $query->result();
    }
    function getnightByState($state_id)
    {   
        $this->db->select('no_of_night.no_of_nights, no_of_night.id');
        $this->db->from('no_of_night');
        //$this->db->join('night_stay_plan','night_stay_plan.no_of_nights=no_of_night.id AND night_stay_plan.state_id="'.$state_id.'" ','left');
        $this->db->where('no_of_night.state_id',$state_id);
        //$this->db->where('night_stay_plan.no_of_nights',null);
        $query = $this->db->get();
        return $query->result();
    }
    function getnightByStateedit($state_id)
    {   
        $this->db->select('no_of_night.no_of_nights, no_of_night.id');
        $this->db->from('no_of_night');
        $this->db->where('no_of_night.state_id',$state_id);
       $query = $this->db->get();
        return $query->result();
    }
    
}

  