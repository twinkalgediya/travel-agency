
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
 include_once APPPATH.'/third_party/mpdf/mpdf.php';
 
class M_pdf {
 
    public $param;
    public $pdf;
 
    public function __construct($param =  'Letter','0in','0in','0in','0in','UTF-8',None)
    {
        $this->param =$param;
        $this->pdf = new mPDF($this->param);
    }
	
}