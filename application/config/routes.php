<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = "login";
$route['404_override'] = 'login/error';

/*********** USER DEFINED ROUTES *******************/

$route['loginMe'] = 'login/loginMe';
$route['dashboard'] = 'user';
$route['logout'] = 'user/logout';

/*********** ADMIN CONTROLLER ROUTES *******************/
$route['noaccess'] = 'login/noaccess';
$route['userListing'] = 'admin/userListing';
$route['userListing/(:num)'] = "admin/userListing/$1";
$route['addNew'] = "admin/addNew";
$route['addNewUser'] = "admin/addNewUser";
$route['editOld'] = "admin/editOld";
$route['editOld/(:num)'] = "admin/editOld/$1";
$route['editUser'] = "admin/editUser";
$route['deleteUser'] = "admin/deleteUser";
$route['log-history'] = "admin/logHistory";
$route['log-history-backup'] = "admin/logHistoryBackup";
$route['log-history/(:num)'] = "admin/logHistorysingle/$1";
$route['log-history/(:num)/(:num)'] = "admin/logHistorysingle/$1/$2";
$route['backupLogTable'] = "admin/backupLogTable";
$route['backupLogTableDelete'] = "admin/backupLogTableDelete";
$route['log-history-upload'] = "admin/logHistoryUpload";
$route['logHistoryUploadFile'] = "admin/logHistoryUploadFile";


/*********** Employee CONTROLLER ROUTES *******************/
$route['employeeListing'] = 'employee/employeeListing';
$route['employeeListing/(:num)'] = "employee/employeeListing/$1";
$route['addEmployee'] = 'employee/addEmployee';
$route['editEmployee/(:num)'] = "employee/editEmployee/$1";
$route['updateEmployee/(:num)'] = "employee/updateEmployee/$1";
$route['deleteEmployee/(:num)'] = "employee/deleteEmployee/$1";

/*********** INQUIRY CONTROLLER ROUTES *******************/
$route['inquiryListing'] = 'inquiry/inquiryListing';
$route['inquiryListing/(:num)'] = "inquiry/inquiryListing/$1";
$route['addInquiry'] = 'inquiry/addInquiry';
$route['editInquiry/(:num)'] = "inquiry/editInquiry/$1";
$route['updateInquiry/(:num)'] = "inquiry/updateInquiry/$1";
$route['deleteInquiry/(:num)'] = "inquiry/deleteInquiry/$1";

/*********** Hotel CONTROLLER ROUTES *******************/
$route['hotelListing'] 			= 'hotel/hotelListing';
$route['hotelListing/(:num)'] 	= "hotel/hotelListing/$1";
$route['addHotel'] 				= 'hotel/addHotel';
$route['editHotel/(:num)'] 		= "hotel/editHotel/$1";
$route['updateHotel/(:num)'] 	= "hotel/updateHotel/$1";
$route['deleteHotel/(:num)'] 	= "hotel/deleteHotel/$1";
$route['deleteHotelPhoto/(:num)/(:num)'] 	    = "hotel/deleteExtraPhoto/$1/$2";

$route['hotel/getStates'] 			= 'hotel/getstate';
$route['hotel/getCities'] 			= 'hotel/getcity';


/*********** Vehicle CONTROLLER ROUTES *******************/
$route['vehicleListing'] 			= 'vehicle/vehicleListing';
$route['vehicleListing/(:num)'] 	= "vehicle/vehicleListing/$1";
$route['addVehicle'] 				= 'vehicle/addVehicle';
$route['editVehicle/(:num)'] 		= "vehicle/editVehicle/$1";
$route['updateVehicle/(:num)'] 	= "vehicle/updateVehicle/$1";
$route['deleteVehicle/(:num)'] 	= "vehicle/deleteVehicle/$1";

/*********** Night Stay Plan CONTROLLER ROUTES *******************/
$route['nightstayplanListing'] 			= 'nightstayplan/nightstayplanListing';
$route['nightstyaplanListing/(:num)'] 	= "nightstayplan/nightstyaplanListing/$1";
$route['addNightStayPlan'] 				= 'nightstayplan/addNightStayPlan';
$route['editNightStayPlan/(:num)'] 		= "nightstayplan/editNightStayPlan/$1";
$route['updateNightStayPlan/(:num)'] 	= "nightstayplan/updateNightStayPlan/$1";
$route['deleteNightStayPlan/(:num)'] 	= "nightstayplan/deleteNightStayPlan/$1";

/*********** No of night CONTROLLER ROUTES *******************/
$route['noofnightListing'] 			= 'noofnight/noofnightListing';
$route['noofnightListing/(:num)'] 	= "noofnight/noofnightListing/$1";
$route['addnoofnight'] 				= 'noofnight/addnoofnight';
$route['editNoofnight/(:num)'] 		= "noofnight/editNoofnight/$1";
$route['updateNoofnight/(:num)'] 	= "noofnight/updateNoofnight/$1";
$route['deleteNoofnight/(:num)'] 	= "noofnight/deleteNoofnight/$1";

/*********** Package CONTROLLER ROUTES *******************/
$route['packageListing'] 			= 'package/packageListing';
$route['Package/insertPackage'] 	= 'package/insertPackage';
$route['packageListing/(:num)'] 	= "package/packageListing/$1";
$route['addPackage/(:num)'] 		= 'package/addPackage/$1';
$route['editPackage/(:num)'] 		= "package/editPackage/$1";
$route['updatePackage/(:num)'] 	= "package/updatePackage/$1";
$route['deletePackage/(:num)'] 	= "package/deletePackage/$1";
$route['deleteGalaryPhoto/(:num)/(:num)'] 	    = "package/deleteGalaryPhoto/$1/$2";
$route['deleteflight/(:num)/(:num)'] 	    = "package/deleteflight/$1/$2";

$route['createPdf/(:num)'] 		= 'createpdf/ganratePdf/$1';

/*********** Bookinng CONTROLLER ROUTES *******************/
$route['bookingListing'] 			= 'booking/bookingListing';
$route['booking/insertBooking'] 	= 'booking/insertBooking';
$route['bookingListing/(:num)'] 	= "booking/bookingListing/$1";
$route['addBooking/(:num)/(:num)'] 	= 'booking/addBooking/$1/$1';
$route['addBooking/(:num)'] 		= 'booking/addBooking/$1';
$route['editBooking/(:num)'] 		= "booking/editBooking/$1";
$route['updateBooking/(:num)'] 		= "booking/updateBooking/$1";
$route['deleteBooking/(:num)'] 		= "booking/deleteBooking/$1";
$route['booking/getHotel'] 			= "booking/getHotel";
/*********** MANAGER CONTROLLER ROUTES *******************/
$route['tasks'] = "manager/tasks";
$route['addNewTask'] = "manager/addNewTask";
$route['addNewTasks'] = "manager/addNewTasks";
$route['editOldTask/(:num)'] = "manager/editOldTask/$1";
$route['editTask'] = "manager/editTask";
$route['deleteTask/(:num)'] = "manager/deleteTask/$1";

/*********** USER CONTROLLER ROUTES *******************/
$route['loadChangePass'] = "user/loadChangePass";
$route['changePassword'] = "user/changePassword";
$route['pageNotFound'] = "user/pageNotFound";
$route['checkEmailExists'] = "user/checkEmailExists";
$route['endTask/(:num)'] = "user/endTask/$1";
$route['etasks'] = "user/etasks";
$route['userEdit'] = "user/loadUserEdit";
$route['updateUser'] = "user/updateUser";


/*********** LOGIN CONTROLLER ROUTES *******************/
$route['forgotPassword'] = "login/forgotPassword";
$route['resetPasswordUser'] = "login/resetPasswordUser";
$route['resetPasswordConfirmUser'] = "login/resetPasswordConfirmUser";
$route['resetPasswordConfirmUser/(:any)'] = "login/resetPasswordConfirmUser/$1";
$route['resetPasswordConfirmUser/(:any)/(:any)'] = "login/resetPasswordConfirmUser/$1/$2";
$route['createPasswordUser'] = "login/createPasswordUser";

/*********** get dropdown ROUTES *******************/
$route['getStates'] 	= "booking/getStates";
$route['getCityRows'] 	= "booking/getCityRows";
$route['getHotelRows'] 	= "booking/getHotel";
$route['getNoofNightbycountry']="Nightstayplan/getNoofNightbycountry";
$route['getnightbystaterows']="Nightstayplan/getnightbystaterows";
$route['getnightStayPlanrows']="Nightstayplan/getnightStayPlanrows";
$route['getIternary']="Nightstayplan/getIternary";
